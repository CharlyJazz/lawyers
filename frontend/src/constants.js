export const URL_BASE =
  process.env.NODE_ENV === "development"
    ? "http://localhost:5000/api"
    : "http://165.227.29.132:5000/api";
