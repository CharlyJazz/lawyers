import React from "react";
import { Link } from "@reach/router";
import "../FormI129.css";

const Supplements = () => {
  return (
    <div>
      <ul>
        <li>
          <Link to="/e1-e2">
            E-1/E-2 Classification Supplement to Form I-129
          </Link>
        </li>
        <li>
          <Link to="/trade-agreement">
            Trade Agreement Supplement to Form I-129
          </Link>
        </li>
        <li>
          <Link to="/h-classification">
            H Classification Supplement to Form I-129
          </Link>
        </li>
        <li>
          <Link to="/h1b-h1b1">
            H-1B and H-1B1 Data Collection and Filing Fee Exemption Supplement
          </Link>
        </li>
        <li>
          <Link to="/l-classification">
            L Classification Supplement to Form I-129
          </Link>
        </li>
        <li>
          <Link to="/o-p-classification">
            O and P Classifications Supplement to Form I-129
          </Link>
        </li>
        <li>
          <Link to="/q-1-classification">Q-1 Classification Supplement to Form I-129</Link>
        </li>
        <li>
          <Link to="/r-1-classification">R-1 Classification Supplement to Form I-129</Link>
        </li>
        <li>
          <Link to="/attachment-1">Attachment-1 Supplement to Form I-129</Link>
        </li>
      </ul>
    </div>
  );
};

export default Supplements;
