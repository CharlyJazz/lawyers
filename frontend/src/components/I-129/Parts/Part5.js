import React from "react";
import Input from "../../Input";
import StateSelect from "../../StateSelect";
import WithValues from "../../../WithValues";

const Part5 = ({ doc }) => {
  return (
    <WithValues doc={doc}>
      <div>
        <h2>
          Part 5. Basic Information About the Proposed Employment and Employer
        </h2>
        <h3>
          Attach the Form I-129 supplement relevant to the classification of the
          worker(s) you are requesting.
        </h3>
        <fieldset>
          <Input
            label="Job Title"
            name="Job Title"
            annot_field_key="<feff00500061007200740035005f00510031005f004a006f0062005400690074006c0065005b0030005d>"
            doc="i-129"
          />
          <Input
            label="LCA or ETA Case Number"
            name="LCA or ETA Case Number"
            annot_field_key="<feff00500061007200740035005f00510032005f004c00430041006f0072004500540041005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>
          Address where the beneficiary(ies) will work if different from address
          in Part 1.
        </h3>
        <fieldset>
          <Input
            label="Street Number and Name"
            name="Street Number and Name"
            annot_field_key="<feff004c0069006e006500380061005f005300740072006500650074004e0075006d006200650072004e0061006d0065005b0031005d>"
            doc="i-129"
          />
          <Input
            inputType="checkbox"
            label="Suite"
            name="Suite"
            annot_field_key="<feff004c0069006e006500380062005f0055006e00690074005b0032005d>"
            doc="i-129"
          />
          <Input
            inputType="checkbox"
            label="Apartment"
            name="Apartment"
            annot_field_key="<feff004c0069006e006500380062005f0055006e00690074005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="checkbox"
            label="Floor"
            name="Floor"
            annot_field_key="<feff004c0069006e006500380062005f0055006e00690074005b0031005d>"
            doc="i-129"
          />
          <Input
            label="Number"
            name="Number"
            annot_field_key="<feff004c0069006e006500380063005f0041007000740053007400650046006c0072004e0075006d006200650072005b0030005d>"
            doc="i-129"
          />
          <Input
            label="City or Town"
            name="City or Town"
            annot_field_key="<feff004c0069006e006500380064005f00430069007400790054006f0077006e005b0031005d>"
            doc="i-129"
          />
          <StateSelect
            annot_field_key="<feff004c0069006e006500380065005f00530074006100740065005b0031005d>"
            doc="i-129"
            id="State"
          />
          <Input
            label="ZIP Code"
            name="ZIP Code"
            annot_field_key="<feff004c0069006e006500380066005f005a006900700043006f00640065005b0031005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>Did you include an itinerary with the petition?</h3>
        <fieldset>
          <Input
            inputType="radio"
            label="Yes"
            name="Itinerary"
            annot_field_key="<feff00500035004c0069006e00650034005f005900650073005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="No"
            name="Itinerary"
            annot_field_key="<feff00500035004c0069006e00650034005f004e006f005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>
          Will the beneficiary(ies) work for you off-site at another company or
          organization's location?
        </h3>
        <fieldset>
          <Input
            inputType="radio"
            label="Yes"
            name="Work"
            annot_field_key="<feff00500035004c0069006e00650035005f005900650073005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="No"
            name="Work"
            annot_field_key="<feff00500035004c0069006e00650035005f004e006f005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>
          Will the beneficiary(ies) work exclusively in the Commonwealth of the
          Northern Mariana Islands (CNMI)?
        </h3>
        <fieldset>
          <Input
            inputType="radio"
            label="Yes"
            name="COmmonwealth"
            annot_field_key="<feff00500035004c0069006e00650036005f005900650073005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="No"
            name="COmmonwealth"
            annot_field_key="<feff00500035004c0069006e00650036005f004e006f005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>Is this a full-time position?</h3>
        <fieldset>
          <Input
            inputType="radio"
            label="Yes"
            name="Position"
            annot_field_key="<feff00500035004c0069006e00650037005f005900650073005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="No"
            name="Position"
            annot_field_key="<feff00500035004c0069006e00650037005f004e006f005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>
          If the answer to Item Number 7. is no, how many hours per week for the
          position?
        </h3>
        <fieldset>
          <Input
            label="Hours"
            name="Hours"
            annot_field_key="<feff00500035004c0069006e00650039005f0048006f007500720073005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>Wages</h3>
        <fieldset>
          <Input
            label="Quantity"
            name="Quantity"
            annot_field_key="<feff004c0069006e00650038005f00570061006700650073005b0030005d>"
            doc="i-129"
          />
          <Input
            label="Per (Specify hour, week, month, or year)"
            name="Per (Specify hour, week, month, or year)"
            annot_field_key="<feff004c0069006e00650038005f005000650072005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>Other Compensation (Explain)</h3>
        <fieldset>
          <Input
            inputType="textarea"
            label="Compensation"
            name="Compensation"
            annot_field_key="<feff004c0069006e006500310030005f004500780070006c0061006e006100740069006f006e005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>Dates of intended employment</h3>
        <fieldset>
          <Input
            label="From"
            name="From"
            annot_field_key="<feff00500061007200740035005f005100310030005f004400610074006500460072006f006d005b0030005d>"
            doc="i-129"
            inputType="date"
          />
          <Input
            label="To"
            name="To"
            annot_field_key="<feff00500061007200740035005f005100310030005f00440061007400650054006f005b0030005d>"
            doc="i-129"
            inputType="date"
          />
        </fieldset>
        <h3>Type of Business</h3>
        <fieldset>
          <Input
            label="Business"
            name="Business"
            annot_field_key="<feff00500061007200740035004c0069006e006500310032005f0054007900700065006f00660042007500730069006e006500730073005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>Year Established</h3>
        <fieldset>
          <Input
            label="Year"
            name="Year"
            annot_field_key="<feff00500035004c0069006e006500310033005f005900650061007200450073007400610062006c00690073006800650064005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>Current Number of Employees in the United States</h3>
        <fieldset>
          <Input
            label="Number of Employees"
            name="Number of Employees"
            annot_field_key="<feff00500035004c0069006e006500310034005f004e0075006d006200650072006f00660045006d0070006c006f0079006500650073005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>Gross Annual Income</h3>
        <fieldset>
          <Input
            label="Income"
            name="Income"
            annot_field_key="<feff004c0069006e006500310035005f00470072006f007300730041006e006e00750061006c0049006e0063006f006d0065005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>Net Annual Income</h3>
        <fieldset>
          <Input
            label="Net Income"
            name="Net Income"
            annot_field_key="<feff004c0069006e006500310036005f004e006500740041006e006e00750061006c0049006e0063006f006d0065005b0030005d>"
            doc="i-129"
          />
        </fieldset>
      </div>
    </WithValues>
  );
};

export default Part5;
