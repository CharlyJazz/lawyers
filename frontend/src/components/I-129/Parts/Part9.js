import React from "react";
import Input from "../../Input";
import WithValues from "../../../WithValues";

const Part9 = ({ doc }) => {
  return (
    <WithValues doc={doc}>
      <div>
        <h2>
          Part 9. Additional Information About Your Petition For Nonimmigrant
          Worker
        </h2>
        <h4>
          If you require more space to provide any additional information within
          this petition, use the space below. If you require more space than
          what is provided to complete this petition, you may make a copy of
          Part 9. to complete and file with this petition. In order to assist us
          in reviewing your response, you must identify the Page Number, Part
          Number and Item Number corresponding to the additional information.
        </h4>
        <h3>A-Number</h3>
        <fieldset>
          <Input
            label="Number"
            name="Number"
            annot_field_key="<feff00500061007200740037004c0069006e00650031005f0041006c00690065006e004e0075006d006200650072005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>1.</h3>
        <fieldset>
          <Input
            label="Page Number"
            name="Page Number"
            annot_field_key="<feff004c0069006e006500320061005f0050006100670065004e0075006d006200650072005b0030005d>"
            doc="i-129"
          />
          <Input
            label="Part Number"
            name="Part Number"
            annot_field_key="<feff004c0069006e006500320062005f0050006100720074004e0075006d006200650072005b0030005d>"
            doc="i-129"
          />
          <Input
            label="Item Number"
            name="Item Number"
            annot_field_key="<feff004c0069006e006500340061005f0050006100670065004e0075006d006200650072005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="textarea"
            label="Additional Information"
            name="Additional Information"
            annot_field_key="<feff004c0069006e00650034005f004100640064006900740069006f006e0061006c0049006e0066006f005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>2.</h3>
        <fieldset>
          <Input
            label="Page Number"
            name="Page Number2"
            annot_field_key="<feff004c0069006e006500340061005f0050006100670065004e0075006d006200650072005b0030005d>"
            doc="i-129"
          />
          <Input
            label="Part Number"
            name="Part Number2"
            annot_field_key="<feff004c0069006e006500340062005f0050006100720074004e0075006d006200650072005b0030005d>"
            doc="i-129"
          />
          <Input
            label="Item Number"
            name="Item Number2"
            annot_field_key="<feff004c0069006e006500340063005f004900740065006d004e0075006d006200650072005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="textarea"
            label="Additional Information"
            name="Additional Information2"
            annot_field_key="<feff004c0069006e00650033005f004100640064006900740069006f006e0061006c0049006e0066006f005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>3.</h3>
        <fieldset>
          <Input
            label="Page Number"
            name="Page Number3"
            annot_field_key="<feff004c0069006e006500340061005f0050006100670065004e0075006d006200650072005b0031005d>"
            doc="i-129"
          />
          <Input
            label="Part Number"
            name="Part Number3"
            annot_field_key="<feff004c0069006e006500340062005f0050006100720074004e0075006d006200650072005b0031005d>"
            doc="i-129"
          />
          <Input
            label="Item Number"
            name="Item Number3"
            annot_field_key="<feff004c0069006e006500340063005f004900740065006d004e0075006d006200650072005b0031005d>"
            doc="i-129"
          />
          <Input
            inputType="textarea"
            label="Additional Information"
            name="Additional Information3"
            annot_field_key="<feff004c0069006e00650032005f004100640064006900740069006f006e0061006c0049006e0066006f005b0030005d>"
            doc="i-129"
          />
        </fieldset>
      </div>
    </WithValues>
  );
};

export default Part9;
