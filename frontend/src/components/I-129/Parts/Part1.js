import React from "react";
import Input from "../../Input";
import StateSelect from "../../StateSelect";
import WithValues from "../../../WithValues";

const Part1 = ({ doc }) => {
  return (
    <WithValues doc={doc}>
      <div>
        <h2>Part 1. Petitioner Information</h2>
        <h3>Legal Name of Individual Petitioner</h3>
        <fieldset>
          <Input
            label="Family Name (Last Name)"
            name="Family Name (Last Name)"
            annot_field_key="<feff004c0069006e00650031005f00460061006d0069006c0079004e0061006d0065005b0030005d>"
            doc="i-129"
          />
          <Input
            label="Given Name (First Name)"
            name="Given Name (First Name)"
            annot_field_key="<feff004c0069006e00650031005f0047006900760065006e004e0061006d0065005b0030005d>"
            doc="i-129"
          />
          <Input
            label="Middle Name"
            name="Middle Name"
            annot_field_key="<feff004c0069006e00650031005f004d006900640064006c0065004e0061006d0065005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>Company or Organization Name</h3>
        <fieldset>
          <Input
            label="Organization Name"
            name="Organization Name"
            annot_field_key="<feff004c0069006e00650033005f0043006f006d00700061006e0079006f0072004f00720067004e0061006d0065005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>Mailing Address of Individual, Company or Organization</h3>
        <fieldset>
          <Input
            label="In Care Of Name"
            name="In Care Of Name"
            annot_field_key="<feff004c0069006e006500370061005f0049006e0043006100720065006f0066004e0061006d0065005b0030005d>"
            doc="i-129"
          />
          <Input
            label="Street Number and Name"
            name="Street Number and Name"
            annot_field_key="<feff004c0069006e006500370062005f005300740072006500650074004e0075006d006200650072004e0061006d0065005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="checkbox"
            label="Suite"
            name="Suite"
            annot_field_key="<feff004c0069006e00650033005f0055006e00690074005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="checkbox"
            label="Apartment"
            name="Apartment"
            annot_field_key="<feff004c0069006e00650033005f0055006e00690074005b0031005d>"
            doc="i-129"
          />
          <Input
            inputType="checkbox"
            label="Floor"
            name="Floor"
            annot_field_key="<feff004c0069006e00650033005f0055006e00690074005b0032005d>"
            doc="i-129"
          />
          <Input
            label="Number"
            name="Number"
            annot_field_key="<feff004c0069006e00650033005f0041007000740053007400650046006c0072004e0075006d006200650072005b0030005d>"
            doc="i-129"
          />
          <Input
            label="City or Town"
            name="City or Town"
            annot_field_key="<feff004c0069006e0065005f00430069007400790054006f0077006e005b0030005d>"
            doc="i-129"
          />
          <StateSelect
            annot_field_key="<feff00500031005f004c0069006e00650033005f00530074006100740065005b0030005d>"
            doc="i-129"
            id="State"
          />
          <Input
            label="ZIP Code"
            name="ZIP Code"
            annot_field_key="<feff00500031005f004c0069006e00650033005f005a006900700043006f00640065005b0030005d>"
            doc="i-129"
          />
          <Input
            label="Province"
            name="Province"
            annot_field_key="<feff00500031005f004c0069006e00650033005f00500072006f00760069006e00630065005b0030005d>"
            doc="i-129"
          />
          <Input
            label="Postal Code"
            name="Postal Code"
            annot_field_key="<feff00500031005f004c0069006e00650033005f0050006f007300740061006c0043006f00640065005b0030005d>"
            doc="i-129"
          />
          <Input
            label="County"
            name="County"
            annot_field_key="<feff00500031005f004c0069006e00650033005f0043006f0075006e007400720079005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>Contact Information</h3>
        <fieldset>
          <Input
            label="Daytime Telephone Number"
            name="Daytime Telephone Number"
            annot_field_key="<feff004c0069006e00650032005f00440061007900740069006d006500500068006f006e0065004e0075006d0062006500720031005f00500061007200740038005b0030005d>"
            doc="i-129"
          />
          <Input
            label="Mobile Telephone Number"
            name="Mobile Telephone Number"
            annot_field_key="<feff004c0069006e00650033005f004d006f00620069006c006500500068006f006e0065004e0075006d0062006500720031005f00500061007200740038005b0030005d>"
            doc="i-129"
          />{" "}
          <Input
            label="Email Address (if any)"
            name="Email Address (if any)"
            annot_field_key="<feff004c0069006e00650039005f0045006d00610069006c0041006400640072006500730073005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>Other Information</h3>
        <fieldset>
          <Input
            label="Federal Employer Identification Number (FEIN)"
            name="Federal Employer Identification Number (FEIN)"
            annot_field_key="<feff0054006500780074004600690065006c00640031005b0030005d>"
            doc="i-129"
          />
          <Input
            label="Individual IRS Tax Number"
            name="Individual IRS Tax Number"
            annot_field_key="<feff004c0069006e00650033005f005400610078004e0075006d006200650072005b0030005d>"
            doc="i-129"
          />{" "}
          <Input
            label="U.S. Social Security Number (if any)"
            name="U.S. Social Security Number (if any)"
            annot_field_key="<feff004c0069006e00650034005f00530053004e005b0030005d>"
            doc="i-129"
          />
        </fieldset>
      </div>
    </WithValues>
  );
};

export default Part1;
