import React from "react";
import Input from "../../Input";
import WithValues from "../../../WithValues";

const Part4 = ({ doc }) => {
  return (
    <WithValues doc={doc}>
      <div>
        <h2>Part 4. Processing Information</h2>
        <h3>
          If a beneficiary or beneficiaries named in Part 3. is/are outside the
          United States, or a requested extension of stay or change of status
          cannot be granted, state the U.S. Consulate or inspection facility you
          want notified if this petition is approved.
        </h3>
        <fieldset>
          <h4>Type of Office</h4>
          <Input
            inputType="radio"
            label="Consulate"
            name="Office"
            annot_field_key="<feff0054007900700065006f0066004f00660066006900630065005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="Pre-flight inspection"
            name="Office"
            annot_field_key="<feff0054007900700065006f0066004f00660066006900630065005b0031005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="Port of Entry"
            name="Office"
            annot_field_key="<feff0054007900700065006f0066004f00660066006900630065005b0032005d>"
            doc="i-129"
          />
          <Input
            label="Office Address (City)"
            name="Office Address (City)"
            annot_field_key="<feff004f0066006600690063006500410064006400720065007300730043006900740079005b0030005d>"
            doc="i-129"
          />
          <Input
            label="U.S. State or Foreign Country"
            name="U.S. State or Foreign Country"
            annot_field_key="<feff00500061007200740034005f00310063005f00530074006100740065005f006f0072005f0043006f0075006e007400720079005b0030005d>"
            doc="i-129"
          />
          <h4>Beneficiary's Foreign Address</h4>
          <Input
            label="U.S. State or Foreign Country"
            name="U.S. State or Foreign Country"
            annot_field_key="<feff004c0069006e006500320063005f00430069007400790054006f0077006e005b0030005d>"
            doc="i-129"
          />
          <Input
            label="Street Number and Name"
            name="Street Number and Name"
            annot_field_key="<feff004c0069006e006500320062005f005300740072006500650074004e0075006d006200650072004e0061006d0065005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="checkbox"
            label="Suite"
            name="Suite"
            annot_field_key="<feff004c0069006e0065003200620032005f0055006e00690074005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="checkbox"
            label="Apartment"
            name="Apartment"
            annot_field_key="<feff004c0069006e0065003200620032005f0055006e00690074005b0032005d>"
            doc="i-129"
          />
          <Input
            inputType="checkbox"
            label="Floor"
            name="Floor"
            annot_field_key="<feff004c0069006e0065003200620032005f0055006e00690074005b0031005d>"
            doc="i-129"
          />
          <Input
            label="Number"
            name="Number"
            annot_field_key="<feff004c0069006e0065003200620032005f0041007000740053007400650046006c0072004e0075006d006200650072005b0030005d>"
            doc="i-129"
          />
          <Input
            label="City or Town"
            name="City or Town"
            annot_field_key="<feff004c0069006e006500320063005f00430069007400790054006f0077006e005b0030005d>"
            doc="i-129"
          />
          <Input
            label="State"
            name="State"
            annot_field_key="<feff004c0069006e0065003200670032005f00500072006f00760069006e00630065005b0030005d>"
            doc="i-129"
            id="City or Town"
          />
          <Input
            label="Province"
            name="Province"
            annot_field_key="<feff004c0069006e0065003200670032005f00500072006f00760069006e00630065005b0031005d>"
            doc="i-129"
          />
          <Input
            label="Postal Code"
            name="Postal Code"
            annot_field_key="<feff004c0069006e006500330066005f0050006f007300740061006c0043006f00640065005b0030005d>"
            doc="i-129"
          />
          <Input
            label="County"
            name="County"
            annot_field_key="<feff004c0069006e0065005f0043006f0075006e007400720079005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>Does each person in this petition have a valid passport?</h3>
        <fieldset>
          <Input
            inputType="radio"
            label="Yes"
            name="Passport"
            annot_field_key="<feff00500034004c0069006e00650032005f005900650073005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="No. If no, go to Part 9. and type or print your
          explanation."
            name="Passport"
            annot_field_key="<feff00500034004c0069006e00650032005f004e006f005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>Are you filing any other petitions with this one?</h3>
        <fieldset>
          <Input
            inputType="radio"
            label="Yes"
            name="Petitions"
            annot_field_key="<feff00500034004c0069006e00650033005f005900650073005b0030005d>"
            doc="i-129"
          />
          <Input
            label="If yes, how many?"
            name="If yes, how many?"
            annot_field_key="<feff00500034004c0069006e00650033005f0048006f0077004d0061006e0079005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="No"
            name="Petitions"
            annot_field_key="<feff00500034004c0069006e00650033005f004e006f005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>
          Are you filing any applications for replacement/initial I-94,
          Arrival-Departure Records with this petition? Note that if the
          beneficiary was issued an electronic Form I-94 by CBP when he/she was
          admitted to the United States at an air or sea port, he/ she may be
          able to obtain the Form I-94 from the CBP Website at www.cbp.gov/i94
          instead of filing an application for a replacement/initial I-94.
        </h3>
        <fieldset>
          <Input
            inputType="radio"
            label="Yes"
            name="Application"
            annot_field_key="<feff00500034004c0069006e00650034005f0048006f0077004d0061006e0079005b0030005d>"
            doc="i-129"
          />
          <Input
            label="If yes, how many?"
            name="If yes, how many?"
            annot_field_key="<feff00500034004c0069006e00650034005f005900650073005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="No"
            name="Application"
            annot_field_key="<feff00500034004c0069006e00650034005f004e006f005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>
          Are you filing any applications for dependents with this petition?
        </h3>
        <fieldset>
          <Input
            inputType="radio"
            label="Yes"
            name="Dependents"
            annot_field_key="<feff00500034004c0069006e00650035005f005900650073005b0030005d>"
            doc="i-129"
          />
          <Input
            label="If yes, how many?"
            name="If yes, how many?"
            annot_field_key="<feff00500034004c0069006e00650035005f0048006f0077004d0061006e0079005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="No"
            name="Dependents"
            annot_field_key="<feff00500034004c0069006e00650035005f004e006f005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>Is any beneficiary in this petition in removal proceedings?</h3>
        <fieldset>
          <Input
            inputType="radio"
            label="Yes"
            name="Proceedings"
            annot_field_key="<feff00500034004c0069006e00650036005f005900650073005b0030005d>"
            doc="i-129"
          />
          <h4>
            If yes, proceed to Part 9. and list the beneficiary's(ies) name(s).
          </h4>
          <Input
            inputType="radio"
            label="No"
            name="Proceedings"
            annot_field_key="<feff00500034004c0069006e00650036005f004e006f005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>
          Have you ever filed an immigrant petition for any beneficiary in this
          petition?
        </h3>
        <fieldset>
          <Input
            inputType="radio"
            label="Yes"
            name="Inmigrant"
            annot_field_key="<feff00500034004c0069006e00650035005f0048006f0077004d0061006e0079005b0031005d>"
            doc="i-129"
          />
          <Input
            label="If yes, how many?"
            name="If yes, how many?"
            annot_field_key="<feff00500034004c0069006e00650037005b0031005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="No"
            name="Inmigrant"
            annot_field_key="<feff00500034004c0069006e00650037005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>Did you indicate you were filing a new petition in Part 2.?</h3>
        <fieldset>
          <Input
            inputType="radio"
            label="Yes. If yes, answer the questions below."
            name="Filing"
            annot_field_key="<feff00500034004c0069006e00650038005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="No. If no, proceed to Item Number 9."
            name="Filing"
            annot_field_key="<feff00500034004c0069006e00650038005b0031005d>"
            doc="i-129"
          />
          <h4>
            Has any beneficiary in this petition ever been given the
            classification you are now requesting within the last seven years?
          </h4>
          <Input
            inputType="radio"
            label="Yes. If yes, proceed to Part 9. and type or print your explanation."
            name="Given"
            annot_field_key="<feff00500034004c0069006e006500380061005f005900650073005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="No"
            name="Given"
            annot_field_key="<feff00500034004c0069006e006500380061005f004e006f005b0030005d>"
            doc="i-129"
          />
          <h4>
            Has any beneficiary in this petition ever been denied the
            classification you are now requesting within the last seven years?
          </h4>
          <Input
            inputType="radio"
            label="Yes. If yes, proceed to Part 9. and type or print your explanation."
            name="Given2"
            annot_field_key="<feff00500034004c0069006e006500380062005f005900650073005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="No"
            name="Given2"
            annot_field_key="<feff00500034004c0069006e006500380062005f004e006f005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>
          Have you ever previously filed a nonimmigrant petition for this
          beneficiary?
        </h3>
        <fieldset>
          <Input
            inputType="radio"
            label="Yes. If yes, proceed to Part 9. and type or print your explanation."
            name="Noimigrant"
            annot_field_key="<feff00500034004c0069006e00650039005f005900650073005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="No"
            name="Noimigrant"
            annot_field_key="<feff00500034004c0069006e00650039005f004e006f005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>
          If you are filing for an entertainment group, has any beneficiary in
          this petition not been with the group for at least one year?
        </h3>
        <fieldset>
          <Input
            inputType="radio"
            label="Yes. If yes, proceed to Part 9. and type or print your explanation."
            name="Group"
            annot_field_key="<feff00500034004c0069006e006500310030005f005900650073005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="No"
            name="Group"
            annot_field_key="<feff00500034004c0069006e006500310030005f004e006f005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>
          Has any beneficiary in this petition ever been a J-1 exchange visitor
          or J-2 dependent of a J-1 exchange visitor?
        </h3>
        <fieldset>
          <Input
            inputType="radio"
            label="Yes. If yes, proceed to Item Number 11.b."
            name="Visitor"
            annot_field_key="<feff00500034004c0069006e0065003100310061005f005900650073005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="No"
            name="Visitor"
            annot_field_key="<feff00500034004c0069006e0065003100310061005f004e006f005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>
          If you checked yes in Item Number 11.a., provide the dates the
          beneficiary maintained status as a J-1 exchange visitor or J-2
          dependent. Also, provide evidence of this status by attaching a copy
          of either a DS-2019, Certificate of Eligibility for Exchange Visitor
          (J-1) Status, a Form IAP-66, or a copy of the passport that includes
          the J visa stamp.
        </h3>
        <fieldset>
          <Input
            label="Status"
            name="Status"
            annot_field_key="<feff00500034005f004c0069006e0065003100310062005b0030005d>"
            doc="i-129"
          />
        </fieldset>
      </div>
    </WithValues>
  );
};

export default Part4;
