import React from "react";
import Input from "../../Input";
import StateSelect from "../../StateSelect";
import WithValues from "../../../WithValues";

const Part6 = ({ doc }) => {
  return (
    <WithValues doc={doc}>
      <div>
        <h2>
          Part 6. Certification Regarding the Release of Controlled Technology
          or Technical Data to Foreign Persons in the United States
        </h2>
        <h4>
          (This section of the form is required only for H-1B, H-1B1
          Chile/Singapore, L-1, and O-1A petitions. It is not required for any
          other classifications. Please review the Form I-129 General Filing
          Instructions before completing this section.)
        </h4>
        <h3>
          With respect to the technology or technical data the petitioner will
          release or otherwise provide access to the beneficiary, the petitioner
          certifies that it has reviewed the Export Administration Regulations
          (EAR) and the International Traffic in Arms Regulations (ITAR) and has
          determined that
        </h3>
        <fieldset>
          <Input
            inputType="radio"
            label="A license is not required from either the U.S. Department of Commerce or the U.S. Department of State to release such
          technology or technical data to the foreign person; or"
            name="Technical"
            annot_field_key="<feff004e006f004400650065006d00650064005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="A license is required from the U.S. Department of Commerce and/or the U.S. Department of State to release such technology
          or technical data to the beneficiary and the petitioner will prevent access to the controlled technology or technical data by the
          beneficiary until and unless the petitioner has received the required license or other authorization to release it to the
          beneficiary"
            name="Technical"
            annot_field_key="<feff004400650065006d00650064005b0030005d>"
            doc="i-129"
          />
        </fieldset>
      </div>
    </WithValues>
  );
};

export default Part6;
