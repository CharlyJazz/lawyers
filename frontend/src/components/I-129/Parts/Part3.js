import React from "react";
import Input from "../../Input";
import StateSelect from "../../StateSelect";
import WithValues from "../../../WithValues";

const Part3 = ({ doc }) => {
  return (
    <WithValues doc={doc}>
      <div>
        <h2>
          Part 3. Beneficiary Information (Information about the
          beneficiary/beneficiaries you are filing for. Complete the blocks
          below. Use the Attachment-1 sheet to name each beneficiary included in
          this petition.)
        </h2>
        <h3>If an Entertainment Group, Provide the Group Name</h3>
        <fieldset>
          <Input
            label="Group Name"
            name="Group Name"
            annot_field_key="<feff00500061007200740037004c0069006e00650044005f0045006d00700031004e0061006d0065005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>Provide Name of Beneficiary</h3>
        <fieldset>
          <Input
            label="Family Name (Last Name)"
            name="Family Name (Last Name)"
            annot_field_key="<feff004c0069006e00650031005f00460061006d0069006c0079004e0061006d0065005b0031005d>"
            doc="i-129"
          />
          <Input
            label="Given Name (First Name)"
            name="Given Name (First Name)"
            annot_field_key="<feff004c0069006e00650031005f0047006900760065006e004e0061006d0065005b0031005d>"
            doc="i-129"
          />
          <Input
            label="Middle Name"
            name="Middle Name"
            annot_field_key="<feff004c0069006e00650031005f004d006900640064006c0065004e0061006d0065005b0031005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>
          Provide all other names the beneficiary has used. Include nicknames,
          aliases, maiden name, and names from all previous marriages.
        </h3>
        <fieldset>
          <Input
            label="Family Name (Last Name)"
            name="Family Name (Last Name)"
            annot_field_key="<feff004c0069006e00650033005f00460061006d0069006c0079004e0061006d00650032005b0030005d>"
            doc="i-129"
          />
          <Input
            label="Given Name (First Name)"
            name="Given Name (First Name)"
            annot_field_key="<feff004c0069006e00650033005f0047006900760065006e004e0061006d00650032005b0030005d>"
            doc="i-129"
          />
          <Input
            label="Middle Name"
            name="Middle Name"
            annot_field_key="<feff004c0069006e00650033005f004d006900640064006c0065004e0061006d00650032005b0030005d>"
            doc="i-129"
          />
          <Input
            label="Family Name (Last Name) 2"
            name="Family Name (Last Name) 2"
            annot_field_key="<feff004c0069006e00650033005f0047006900760065006e004e0061006d00650032005b0031005d>"
            doc="i-129"
          />
          <Input
            label="Given Name (First Name) 2"
            name="Given Name (First Name) 2"
            annot_field_key="<feff004c0069006e00650033005f00460061006d0069006c0079004e0061006d00650032005b0031005d>"
            doc="i-129"
          />
          <Input
            label="Middle Name 2"
            name="Middle Name 2"
            annot_field_key="<feff004c0069006e00650033005f004d006900640064006c0065004e0061006d00650032005b0031005d>"
            doc="i-129"
          />
          <Input
            label="Family Name (Last Name) 3"
            name="Family Name (Last Name) 3"
            annot_field_key="<feff004c0069006e00650033005f00460061006d0069006c0079004e0061006d00650031005b0030005d>"
            doc="i-129"
          />
          <Input
            label="Given Name (First Name) 3"
            name="Given Name (First Name) 3"
            annot_field_key="<feff004c0069006e00650033005f0047006900760065006e004e0061006d00650031005b0030005d>"
            doc="i-129"
          />
          <Input
            label="Middle Name 3"
            name="Middle Name 3"
            annot_field_key="<feff004c0069006e00650033005f004d006900640064006c0065004e0061006d00650031005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>Other Information</h3>
        <fieldset>
          <Input
            label="Date of Birth"
            name="Date of Birth"
            annot_field_key="<feff004c0069006e00650036005f0044006100740065004f006600420069007200740068005b0030005d>"
            doc="i-129"
            inputType="date"
          />
          <h4>Gender</h4>
          <Input
            inputType="radio"
            label="Male"
            name="Gender Male"
            annot_field_key="<feff004c0069006e00650031005f00470065006e006400650072005f00500033005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="Female"
            name="Gender Female"
            annot_field_key="<feff004c0069006e00650031005f00470065006e006400650072005f00500033005b0031005d>"
            doc="i-129"
          />
          <Input
            label="U.S. Social Security Number (if any)"
            name="U.S. Social Security Number (if any)"
            annot_field_key="<feff004c0069006e00650035005f00530053004e005b0030005d>"
            doc="i-129"
          />
          <Input
            label="Alien Registration Number (A-Number)"
            name="Alien Registration Number (A-Number)"
            annot_field_key="<feff004c0069006e00650031005f0041006c00690065006e004e0075006d006200650072005b0030005d>"
            doc="i-129"
          />
          <Input
            label="Country of Birth"
            name="Country of Birth"
            annot_field_key="<feff004c0069006e0065005f0043006f0075006e007400720079004f006600420069007200740068005b0030005d>"
            doc="i-129"
          />
          <Input
            label="Province of Birth"
            name="Province of Birth"
            annot_field_key="<feff00500061007200740034004c0069006e00650033005f004400500072006f00760069006e00630065005b0030005d>"
            doc="i-129"
          />
          <Input
            label="Country of Citizenship or Nationality"
            name="Country of Citizenship or Nationality"
            annot_field_key="<feff004c0069006e0065005f0043006f0075006e007400720079004f006600420069007200740068005b0031005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>
          If the beneficiary is in the United States, complete the following
        </h3>
        <fieldset>
          <Input
            label="Date of Last Arrival (mm/dd/yyyy)"
            name="Date of Last Arrival (mm/dd/yyyy)"
            annot_field_key="<feff004c0069006e0065003100310061005f0044006100740065006f0066004100720072006900760061006c005b0030005d>"
            doc="i-129"
            inputType="date"
          />
          <Input
            label="I-94 Arrival-Departure Record Number"
            name="I-94 Arrival-Departure Record Number"
            annot_field_key="<feff004c0069006e0065003100310061005f004100720072006900760061006c004400650070006100720074007500720065005b0030005d>"
            doc="i-129"
          />
          <Input
            label="Passport or Travel Document Number"
            name="Passport or Travel Document Number"
            annot_field_key="<feff004c0069006e0065003100310063005f00500061007300730070006f00720074006f007200540072006100760044006f0063005b0030005d>"
            doc="i-129"
          />
          <Input
            label="Date Passport or Travel Document issued (mm/dd/yyyy)"
            name="Date Passport or Travel Document issued (mm/dd/yyyy)"
            annot_field_key="<feff004c0069006e0065003100310065005f0045007800700044006100740065005b0031005d>"
            doc="i-129"
            inputType="date"
          />
          <Input
            label="Date Passport or Travel Document Expires (mm/dd/yyyy)"
            name="Date Passport or Travel Document Expires (mm/dd/yyyy)"
            annot_field_key="<feff004c0069006e0065003100310065005f0045007800700044006100740065005b0030005d>"
            doc="i-129"
            inputType="date"
          />
          <Input
            label="Passport or Travel Document Country of Issuance"
            name="Passport or Travel Document Country of Issuance"
            annot_field_key="<feff004c0069006e0065005f0043006f0075006e007400720079004f006600490073007300750061006e00630065005b0030005d>"
            doc="i-129"
          />
          <Input
            label="Current Nonimmigrant Status"
            name="Current Nonimmigrant Status"
            annot_field_key="<feff004c0069006e0065003100310067005f00430075007200720065006e0074004e006f006e005b0030005d>"
            doc="i-129"
          />
          <Input
            label="Date Status Expires or D/S (mm/dd/yyyy)"
            name="Date Status Expires or D/S (mm/dd/yyyy)"
            annot_field_key="<feff004c0069006e0065003100310068005f00440061007400650053007400610074007500730045007800700069007200650073005b0030005d>"
            doc="i-129"
            inputType="date"
          />
          <Input
            label="Student and Exchange Visitor Information System (SEVIS) Number (if any)"
            name="Student and Exchange Visitor Information System (SEVIS) Number (if any)"
            annot_field_key="<feff004c0069006e0065003100310069005f00500061007300730070006f00720074005b0030005d>"
            doc="i-129"
          />
          <Input
            label="Employment Authorization Document (EAD) Number (if any)"
            name="Employment Authorization Document (EAD) Number (if any)"
            annot_field_key="<feff004c0069006e006500310031006a005f00500061007300730070006f00720074005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>
          Current Residential U.S. Address (if applicable) (do not list a P.O.
          Box)
        </h3>
        <fieldset>
          <Input
            label="Street Number and Name"
            name="Street Number and Name"
            annot_field_key="<feff004c0069006e006500380061005f005300740072006500650074004e0075006d006200650072004e0061006d0065005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="checkbox"
            label="Suite"
            name="Suite"
            annot_field_key="<feff004c0069006e00650036005f0055006e00690074005b0032005d>"
            doc="i-129"
          />
          <Input
            inputType="checkbox"
            label="Apartment"
            name="Apartment"
            annot_field_key="<feff004c0069006e00650036005f0055006e00690074005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="checkbox"
            label="Floor"
            name="Floor"
            annot_field_key="<feff004c0069006e00650036005f0055006e00690074005b0031005d>"
            doc="i-129"
          />
          <Input
            label="Number"
            name="Number"
            annot_field_key="<feff004c0069006e00650036005f0041007000740053007400650046006c0072004e0075006d006200650072005b0030005d>"
            doc="i-129"
          />
          <Input
            label="City or Town"
            name="City or Town"
            annot_field_key="<feff004c0069006e006500380064005f00430069007400790054006f0077006e005b0030005d>"
            doc="i-129"
          />
          <StateSelect
            annot_field_key="<feff004c0069006e006500380065005f00530074006100740065005b0030005d>"
            doc="i-129"
            id="City or Town"
          />
          <Input
            label="ZIP Code"
            name="ZIP Code"
            annot_field_key="<feff004c0069006e006500380066005f005a006900700043006f00640065005b0030005d>"
            doc="i-129"
          />
        </fieldset>
      </div>
    </WithValues>
  );
};

export default Part3;
