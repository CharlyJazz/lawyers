import React from "react";
import Input from "../../Input";
import WithValues from "../../../WithValues";

const Part2 = ({ doc }) => {
  return (
    <WithValues doc={doc}>
      <div>
        <h2>
          Part 2. Information About This Petition (See instructions for fee
          information)
        </h2>
        <h3>Requested Nonimmigrant Classification</h3>
        <fieldset>
          <Input
            label="Write classification symbol"
            name="Write classification symbol"
            annot_field_key="<feff00500061007200740032005f0043006c0061007300730069006600690063006100740069006f006e00530079006d0062006f006c005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>Basis for Classification (select only one box):</h3>
        <fieldset>
          <Input
            inputType="radio"
            label="New employment"
            name="Clasification"
            annot_field_key="<feff006e00650077005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="Continuation of previously approved employment without change with the same employer"
            name="Clasification"
            annot_field_key="<feff0063006f006e00740069006e0075006100740069006f006e005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="New concurrent employment"
            name="Clasification"
            annot_field_key="<feff0063006f006e00630075007200720065006e0074005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="Change in previously approved employment"
            name="Clasification"
            annot_field_key="<feff00700072006500760069006f00750073006300680061006e00670065005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="Change of employer"
            name="Clasification"
            annot_field_key="<feff006300680061006e00670065005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="Amended petition"
            name="Clasification"
            annot_field_key="<feff0061006d0065006e006400650064005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>
          Provide the most recent petition/application receipt number for the
          beneficiary. If none exists, indicate "None"
        </h3>
        <fieldset>
          <Input
            label="Most recent petition/application receipt number"
            name="Most recent petition/application receipt number"
            annot_field_key="<feff004c0069006e00650031005f0052006500630065006900700074004e0075006d006200650072005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>Requested Action (select only one box)</h3>
        <fieldset>
          <Input
            inputType="radio"
            label="Notify the office in Part 4. so each beneficiary can obtain a visa or be admitted. (NOTE: A petition is not required for
            E-1, E-2, E-3, H-1B1 Chile/Singapore, or TN visa beneficiaries.)"
            name="Action"
            annot_field_key="<feff005000320043006800650063006b0062006f00780034005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label={`Change the status and extend the stay of each beneficiary because the beneficiary(ies) is/are now in the United States in
          another status (see instructions for limitations). This is available only when you check "New Employment" in Item
          Number 2., above.`}
            name="Action"
            annot_field_key="<feff005000320043006800650063006b0062006f00780034005b0031005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="Extend the stay of each beneficiary because the beneficiary(ies) now hold(s) this status."
            name="Action"
            annot_field_key="<feff005000320043006800650063006b0062006f00780034005b0032005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="Amend the stay of each beneficiary because the beneficiary(ies) now hold(s) this status."
            name="Action"
            annot_field_key="<feff005000320043006800650063006b0062006f00780034005b0033005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="Extend the status of a nonimmigrant classification based on a free trade agreement. (See Trade Agreement Supplement
            to Form I-129 for TN and H-1B1.)"
            name="Action"
            annot_field_key="<feff005000320043006800650063006b0062006f00780034005b0034005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="Change status to a nonimmigrant classification based on a free trade agreement. (See Trade Agreement Supplement to
            Form I-129 for TN and H-1B1.)"
            name="Action"
            annot_field_key="<feff005000320043006800650063006b0062006f00780034005b0035005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>
          Total number of workers included in this petition. (See instructions
          relating to when more than one worker can be included.)
        </h3>
        <fieldset>
          <Input
            label="Workers"
            name="Workers"
            annot_field_key="<feff00540074006c004e0075006d0062006500720073006f00660057006f0072006b00650072005b0030005d>"
            doc="i-129"
          />
        </fieldset>
      </div>
    </WithValues>
  );
};

export default Part2;
