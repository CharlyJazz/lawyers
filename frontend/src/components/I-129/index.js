import React from "react";
import "./FormI129.css";
import Part1 from "./Parts/Part1";
import { Link, Router } from "@reach/router";
import Part2 from "./Parts/Part2";
import Part3 from "./Parts/Part3";
import Part4 from "./Parts/Part4";
import Part5 from "./Parts/Part5";
import Part6 from "./Parts/Part6";
import Part7 from "./Parts/Part7";
import Part8 from "./Parts/Part8";
import Part9 from "./Parts/Part9";
import Supplements from "./Supplements";

const FormI129 = () => {
  return (
    <div>
      <h1>I-129</h1>
      <div className="Layout-Form">
        <aside className="Layout-Form-Aside">
          <ul>
            {[0, 1, 2, 3, 4, 5, 6, 7, 8, 9].map(p => (
              <li key={p}>
                <Link to={`${p === 0 ? `/i-129` : `part-${p}`}`}>
                  {p === 0 ? "Supplements" : `Part ${p}`}
                </Link>
              </li>
            ))}
          </ul>
        </aside>
        <section className="Layout-Section">
          <Router>
            <Supplements path="/" />
            <Part1 path="part-1" />
            <Part2 path="part-2" />
            <Part3 path="part-3" />
            <Part4 path="part-4" />
            <Part5 path="part-5" />
            <Part6 path="part-6" />
            <Part7 path="part-7" />
            <Part8 path="part-8" />
            <Part9 path="part-9" />
          </Router>
        </section>
      </div>
    </div>
  );
};

export default FormI129;
