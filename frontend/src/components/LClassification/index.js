import React from "react";
import "./LClassification.css";
import Section0 from "./Sections/Section0";
import Section1 from "./Sections/Section1";
import Section2 from "./Sections/Section2";
import Section3 from "./Sections/Section3";
import { Link, Router } from "@reach/router";

const LClassification = () => {

  return (
    <div>
      <h1>L Classification Supplement to Form I-129</h1>
      <div className="Layout-Form">
        <aside className="Layout-Form-Aside">
          <ul>
            <li>
              <Link to="/l-classification">L Classification Supplement</Link>
            </li>
            {[1, 2, 3, 4].map(p => (
              <li key={p}>
                <Link to={`section-${p}`}>Section {p}</Link>
              </li>
            ))}
          </ul>
        </aside>
        <section className="Layout-Section">
          <Router>
            <Section0 path="/" />
            <Section1 path="section-1" />
            <Section2 path="section-2" />
            <Section3 path="section-3" />
          </Router>
        </section>
      </div>
    </div>
  );
};

export default LClassification;
