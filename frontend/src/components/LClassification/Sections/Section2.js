import React from "react";
import InputTable from "../../InputTable";
import WithValues from '../../../WithValues'

const Section2 = () => {

  return (
    <WithValues doc={doc}>
      <div>
        <h3>Section 2. Complete This Section If Filing A Blanket Petition</h3>
        <h3>
          List all U.S. and foreign parent, branches, subsidiaries, and affiliates
          included in this petition. (Attach separate sheets of paper if
          additional space is needed.)
      </h3>
        <fieldset>
          <InputTable
            annot_field_key=""
            doc="i-129"
            columns={["Name and Address", "Relationship"]}
            rows={[
              [
                {
                  id: "<feff00430065006c006c0031005b0030005d>",
                  label: "Name and Adress",
                  name: "Name and Adress"
                },
                {
                  id: "<feff00430065006c006c0032005b0030005d>",
                  label: "Relationship",
                  name: "Relationship"
                }
              ],
              [
                {
                  id: "<feff00430065006c006c0031005b0030005d>",
                  label: "Name and Adress",
                  name: "Name and Adress2"
                },
                {
                  id: "<feff00430065006c006c0032005b0030005d>",
                  label: "Relationship",
                  name: "Relationship2"
                }
              ],
              [
                {
                  id: "<feff00430065006c006c0031005b0030005d>",
                  label: "Name and Adress",
                  name: "Name and Adress3"
                },
                {
                  id: "<feff00430065006c006c0032005b0030005d>",
                  label: "Relationship",
                  name: "Relationship3"
                }
              ],
              [
                {
                  id: "<feff00430065006c006c0031005b0030005d>",
                  label: "Name and Adress",
                  name: "Name and Adress4"
                },
                {
                  id: "<feff00430065006c006c0032005b0030005d>",
                  label: "Relationship",
                  name: "Relationship4"
                }
              ],
              [
                {
                  id: "<feff00430065006c006c0031005b0030005d>",
                  label: "Name and Adress",
                  name: "Name and Adress5"
                },
                {
                  id: "<feff00430065006c006c0032005b0030005d>",
                  label: "Relationship",
                  name: "Relationship5"
                }
              ],
              [
                {
                  id: "<feff00430065006c006c0031005b0030005d>",
                  label: "Name and Adress",
                  name: "Name and Adress6"
                },
                {
                  id: "<feff00430065006c006c0032005b0030005d>",
                  label: "Relationship",
                  name: "Relationship6"
                }
              ],
              [
                {
                  id: "<feff00430065006c006c0031005b0030005d>",
                  label: "Name and Adress",
                  name: "Name and Adress7"
                },
                {
                  id: "<feff00430065006c006c0032005b0030005d>",
                  label: "Relationship",
                  name: "Relationship7"
                }
              ],
              [
                {
                  id: "<feff00430065006c006c0031005b0030005d>",
                  label: "Name and Adress",
                  name: "Name and Adress8"
                },
                {
                  id: "<feff00430065006c006c0032005b0030005d>",
                  label: "Relationship",
                  name: "Relationship8"
                }
              ]
            ]}
          />
        </fieldset>
      </div>
    </WithValues>
  );
};

export default Section2;
