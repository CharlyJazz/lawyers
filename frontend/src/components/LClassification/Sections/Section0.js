import React from "react";
import Input from "../../Input";
import WithValues from '../../../WithValues'

const Section0 = () => {

  return (
    <WithValues doc={doc}>
      <div>
        <h3>1 Name of Petitioner</h3>
        <fieldset>
          <Input
            label="Petitioner"
            name="Petitioner"
            annot_field_key="<feff004c0069006e00650031005f00460061006d0069006c0079004e0061006d0065005b0036005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>2 Name of the beneficiary or if this petition</h3>
        <fieldset>
          <Input
            label="Beneficiary"
            name="Beneficiary"
            annot_field_key="<feff0048005300750070004c0069006e00650032005f00460061006d0069006c0079004e0061006d0065005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>3. this petition is (select only one box):</h3>
        <fieldset>
          <Input
            label="a. An individual petition"
            name="a. An individual petition"
            type="checkbox"
            annot_field_key="<feff0061005f0069006e0064006900760069006400750061006c005b0030005d>"
            doc="i-129"
          />
          <Input
            label="b. A blanket petition"
            name="b. A blanket petition"
            type="checkbox"
            annot_field_key="<feff0062005f0062006c0061006e006b00650074005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>
          4.a. Does the petitioner employ 50 or more individuals in the U.S.?
      </h3>
        <fieldset>
          <Input
            label="4.a Yes"
            name="4.a Does the petitioner employ 50 or more individuals in the U.S."
            type="checkbox"
            doc="i-129"
            annot_field_key="<feff004c0053007500700070004c0069006e006500340061005b0031005d>"
          />
          <Input
            label="4.a No"
            name=" 4.a Does the petitioner employ 50 or more individuals in the U.S."
            type="checkbox"
            annot_field_key="<feff004c0053007500700070004c0069006e006500340061005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>
          4.b. If yes, are more than 50 percent of those employee in H-1B, L-1A,
          or L-1B nonimmigrant status?
      </h3>
        <fieldset>
          <Input
            label="4.b Yes"
            name="4.b. If yes, are more than 50 percent of those employee in H-1B, L-1A, or L-1B nonimmigrant status?"
            type="checkbox"
            doc="i-129"
            annot_field_key="<feff004c0053007500700070004c0069006e006500340062005f005900650073005b0030005d>"
          />
          <Input
            label="4.b No"
            name="4.b. If yes, are more than 50 percent of those employee in H-1B, L-1A, or L-1B nonimmigrant status?"
            type="checkbox"
            doc="i-129"
            annot_field_key="<feff004c0053007500700070004c0069006e006500340062005f004e006f005b0030005d>"
          />
        </fieldset>
      </div>
    </WithValues>
  );
};

export default Section0;
