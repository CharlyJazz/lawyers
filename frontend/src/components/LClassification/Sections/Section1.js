import React from "react";
import InputTable from "../../InputTable";
import Input from "../../Input";
import StateSelect from "../../StateSelect";
import WithValues from '../../../WithValues'

const Section1 = () => {

  return (
    <WithValues doc={doc}>
    <div>
      <h2>
        Section 1. Complete This Section If Filing For An Individual Petition
      </h2>
      <h3>1. Classification sought (select only one box):</h3>
      <fieldset>
        <Input
          label="a. L-1A manager or executive"
          name="a. L-1A manager or executive"
          type="checkbox"
          annot_field_key="<feff0061005f004c00310041005b0030005d>"
          doc="i-129"
        />
        <Input
          label="b. L-1B specialized knowledge"
          name="b. L-1B specialized knowledge"
          type="checkbox"
          doc="i-129"
          annot_field_key="<feff0062005f004c00310042005b0030005d>"
        />
      </fieldset>
      <h3>
        2. List the beneficiary's and any dependent family member's prior
        periods of stay in an H or L classification in the United States for the
        last seven years. Be sure to list only those periods in which the
        beneficiary and/or family members were physically present in the U.S. in
        an H or L classification. Do not include periods in which the
        beneficiary was in a dependent status, for example, H-4 or L-2 status.
        If more space is needed, go to Part 9. of Form I-129. NOTE: Submit
        photocopies of Forms I-94, I-797, and/or other USCIS issued documents
        noting these periods of stay in the H or L classification. (If more
        space is needed, attach an additional sheet.)
      </h3>
      <fieldset>
        <InputTable
          columns={[
            "Subject's Name",
            "Period of Stay (mm/dd/yyyy) From",
            "Period of Stay (mm/dd/yyyy) To"
          ]}
          rows={[
            [
              {
                id:
                  "<feff00530065006300740031005f004e0061006d0065005f004c0069006e00650031005b0030005d>",
                label: "Subject's Name",
                name: "Subject's Name"
              },
              {
                id:
                  "<feff004400610074006500460072006f006d005f006c0069006e00650031005b0030005d>",
                label: "From",
                name: "From"
              },
              {
                id:
                  "<feff00440061007400650054006f005f006c0069006e00650031005b0030005d>",
                label: "To",
                name: "To"
              }
            ],
            [
              {
                id:
                  "<feff00530065006300740031005f004e0061006d0065005f004c0069006e00650032005b0030005d>",
                label: "Subject's Name",
                name: "Subject's Name2"
              },
              {
                id:
                  "<feff004400610074006500460072006f006d005f006c0069006e00650032005b0030005d>",
                label: "From",
                name: "From2"
              },
              {
                id:
                  "<feff00440061007400650054006f005f006c0069006e00650032005b0030005d>",
                label: "To",
                name: "To2"
              }
            ],
            [
              {
                id:
                  "<feff004e0061006d0065005f004c0069006e00650033005b0030005d>",
                label: "Subject's Name",
                name: "Subject's Name3"
              },
              {
                id:
                  "<feff004400610074006500460072006f006d005f004c0069006e00650033005b0030005d>",
                label: "From",
                name: "From3"
              },
              {
                id:
                  "<feff00440061007400650054006f005f004c0069006e00650033005b0030005d>",
                label: "To",
                name: "To3"
              }
            ],
            [
              {
                id:
                  "<feff00530065006300740031005f004e0061006d0065005f004c0069006e00650033005b0030005d>",
                label: "Subject's Name",
                name: "Subject's Name4"
              },
              {
                id:
                  "<feff004400610074006500460072006f006d005f006c0069006e00650033005b0030005d>",
                label: "From",
                name: "From4"
              },
              {
                id:
                  "<feff00440061007400650054006f005f006c0069006e00650033005b0030005d>",
                label: "To",
                name: "To4"
              }
            ],
            [
              {
                id:
                  "<feff00530065006300740031005f004e0061006d0065005f004c0069006e00650033005b0030005d>",
                label: "Subject's Name",
                name: "Subject's Name5"
              },
              {
                id:
                  "<feff004400610074006500460072006f006d005f006c0069006e00650033005b0030005d>",
                label: "From",
                name: "From5"
              },
              {
                id:
                  "<feff00440061007400650054006f005f006c0069006e00650033005b0030005d>",
                label: "To",
                name: "To5"
              }
            ],
            [
              {
                id:
                  "<feff00530065006300740031005f004e0061006d0065005f004c0069006e00650033005b0030005d>",
                label: "Subject's Name",
                name: "Subject's Name6"
              },
              {
                id:
                  "<feff004400610074006500460072006f006d005f006c0069006e00650033005b0030005d>",
                label: "From",
                name: "From6"
              },
              {
                id:
                  "<feff00440061007400650054006f005f006c0069006e00650033005b0030005d>",
                label: "To",
                name: "To6"
              }
            ],
            [
              {
                id:
                  "<feff00530065006300740031005f004e0061006d0065005f004c0069006e00650033005b0030005d>",
                label: "Subject's Name",
                name: "Subject's Name7"
              },
              {
                id:
                  "<feff004400610074006500460072006f006d005f006c0069006e00650033005b0030005d>",
                label: "From",
                name: "From7"
              },
              {
                id:
                  "<feff00440061007400650054006f005f006c0069006e00650033005b0030005d>",
                label: "To",
                name: "To7"
              }
            ]
          ]}
          doc="i-129"
        />
      </fieldset>
      <h3>3. Name of Employer Abroad</h3>
      <fieldset>
        <Input
          name="Name of Employer Abroad"
          label="Name of Employer Abroad"
          doc="i-129"
          annot_field_key="<feff004c0053007500700070004c0069006e00650033005f004e0061006d0065006f00660045006d0070006c006f007900650072004100620072006f00610064005b0030005d>"
        />
      </fieldset>
      <h3>4. Address of Employer Abroad</h3>
      <fieldset>
        <Input
          name="4. Street Number and Name"
          label="4. Street Number and Name"
          doc="i-129"
          annot_field_key="<feff00500061007200740033004c0069006e00650032005f005300740072006500650074004e0061006d0065005b0030005d>"
        />
        <Input
          name="4. Apt."
          label="4. Apt."
          doc="i-129"
          type="checkbox"
          annot_field_key="<feff004c0043006c006100730073004c0069006e00650034005f0055006e00690074005b0032005d>"
        />
        <Input
          name="4. Ste."
          label="4. Ste."
          doc="i-129"
          type="checkbox"
          annot_field_key="<feff004c0043006c006100730073004c0069006e00650034005f0055006e00690074005b0031005d>"
        />
        <Input
          name="4. Flr."
          label="4. Flr."
          doc="i-129"
          type="checkbox"
          annot_field_key="<feff004c0043006c006100730073004c0069006e00650034005f0055006e00690074005b0030005d>"
        />
        <Input
          name="4. Number"
          label="4. Number"
          type="Number"
          doc="i-129"
          annot_field_key="<feff004c0069006e00650032005f0041007000740053007400650046006c0072004e0075006d006200650072005b0030005d>"
        />
        <Input
          name="4. City or Town"
          label="4. City or Town"
          doc="i-129"
          annot_field_key="<feff00500061007200740033004c0069006e00650032005f0043006900740079005b0030005d>"
        />
        <StateSelect
          name="4. State"
          label="4. State"
          doc="i-129"
          annot_field_key="<feff00500061007200740033004c0069006e00650032005f00530074006100740065005b0030005d>"
        />
        <Input
          name="4. ZIP Code"
          label="4. ZIP Code"
          doc="i-129"
          annot_field_key="<feff00500061007200740033004c0069006e00650032005f005a006900700043006f00640065005b0030005d>"
        />
        <Input
          name="4. Province"
          label="4. Province"
          doc="i-129"
          annot_field_key="<feff00500061007200740033004c0069006e00650032005f00500072006f00760069006e00630065005b0030005d>"
        />
        <Input
          name="4. Postal Code"
          label="4. Postal Code"
          doc="i-129"
          annot_field_key="<feff00500061007200740033004c0069006e00650032005f0050006f007300740061006c0043006f00640065005b0030005d>"
        />
        <Input
          name="4. Country"
          label="4. Country"
          doc="i-129"
          annot_field_key="<feff00500061007200740033004c0069006e00650032005f0043006f0075006e007400720079005b0030005d>"
        />
      </fieldset>
      <h3>
        5.Dates of beneficiary's employment with this employer. Explain any
        interruptions in employment.
      </h3>
      <fieldset>
        <InputTable
          columns={[
            "Dates of Employment (mm/dd/yyyy) From",
            "Dates of Employment (mm/dd/yyyy) To",
            "Explanation of Interruptions"
          ]}
          rows={[
            [
              {
                id:
                  "<feff00710035005f004400610074006500460072006f006d005f004c0069006e00650031005b0030005d>",
                label: "From",
                name: "From"
              },
              {
                id:
                  "<feff00710035005f00440061007400650054006f005f004c0069006e00650031005b0030005d>",
                label: "To",
                name: "To"
              },
              {
                id:
                  "<feff00710035005f004500780070006c0061006e006100740069006f006e005f004c0069006e00650031005b0030005d>",
                label: "Explanations",
                name: "Explanations"
              }
            ],
            [
              {
                id:
                  "<feff00710035005f004400610074006500460072006f006d005f004c0069006e00650032005b0030005d>",
                label: "From",
                name: "From2"
              },
              {
                id:
                  "<feff00710035005f00440061007400650054006f005f004c0069006e00650032005b0030005d>",
                label: "To",
                name: "To2"
              },
              {
                id:
                  "<feff00710035005f004500780070006c0061006e006100740069006f006e005f004c0069006e00650032005b0030005d>",
                label: "Explanations",
                name: "Explanations2"
              }
            ],
            [
              {
                id:
                  "<feff00710035005f004400610074006500460072006f006d005f004c0069006e00650033005b0030005d>",
                label: "From",
                name: "From3"
              },
              {
                id:
                  "<feff00710035005f00440061007400650054006f005f004c0069006e00650033005b0030005d>",
                label: "To",
                name: "To3"
              },
              {
                id:
                  "<feff00710035005f004500780070006c0061006e006100740069006f006e005f004c0069006e00650033005b0030005d>",
                label: "Explanations",
                name: "Explanations3"
              }
            ],
            [
              {
                id:
                  "<feff00710035005f004400610074006500460072006f006d005f004c0069006e00650034005b0030005d>",
                label: "From",
                name: "From4"
              },
              {
                id:
                  "<feff00710035005f00440061007400650054006f005f004c0069006e00650034005b0030005d>",
                label: "To",
                name: "To4"
              },
              {
                id:
                  "<feff00710035005f004500780070006c0061006e006100740069006f006e005f004c0069006e00650034005b0030005d>",
                label: "Explanations",
                name: "Explanations4"
              }
            ],
            [
              {
                id:
                  "<feff00710035005f004400610074006500460072006f006d005f004c0069006e00650035005b0030005d>",
                label: "From",
                name: "From5"
              },
              {
                id:
                  "<feff00710035005f00440061007400650054006f005f004c0069006e00650035005b0030005d>",
                label: "To",
                name: "To5"
              },
              {
                id:
                  "<feff00710035005f004500780070006c0061006e006100740069006f006e005f004c0069006e00650035005b0030005d>",
                label: "Explanations",
                name: "Explanations5"
              }
            ],
            [
              {
                id:
                  "<feff00710035005f004400610074006500460072006f006d005f004c0069006e00650036005b0030005d>",
                label: "From",
                name: "From6"
              },
              {
                id:
                  "<feff00710035005f00440061007400650054006f005f004c0069006e00650036005b0030005d>",
                label: "To",
                name: "To6"
              },
              {
                id:
                  "<feff00710035005f004500780070006c0061006e006100740069006f006e005f004c0069006e00650036005b0030005d>",
                label: "Explanations",
                name: "Explanations6"
              }
            ]
          ]}
        />
      </fieldset>
      <h3>
        6. Describe the beneficiary's duties abroad for the 3 years preceding
        the filing of the petition. (If the beneficiary is currently inside the
        United States, describe the beneficiary's duties abroad for the 3 years
        preceding the beneficiary's admission to the United States.)
      </h3>
      <fieldset>
        <Input
          name="6. Describe the beneficiary's duties abroad for the 3 years preceding the filing of the petition. (If the beneficiary is currently inside the
            United States, describe the beneficiary's duties abroad for the 3 years preceding the beneficiary's admission to the United States.)"
          label="6. Describe the beneficiary's duties abroad for the 3 years preceding the filing of the petition. (If the beneficiary is currently inside the
            United States, describe the beneficiary's duties abroad for the 3 years preceding the beneficiary's admission to the United States.)"
          doc="i-129"
          annot_field_key="<feff004c0069006e00650033005f004a006f0062004400650073006300720069007000740069006f006e005b0033005d>"
          inputType="textarea"
        />
      </fieldset>
      <h3>
        7. Describe the beneficiary's proposed duties in the United States.
      </h3>
      <fieldset>
        <Input
          name="7. Describe the beneficiary's proposed duties in the United States."
          label="7. Describe the beneficiary's proposed duties in the United States."
          doc="i-129"
          annot_field_key="<feff004c0069006e00650033005f004a006f0062004400650073006300720069007000740069006f006e005b0031005d>"
          inputType="textarea"
        />
      </fieldset>
      <h3>8. Summarize the beneficiary's education and work experience.</h3>
      <fieldset>
        <Input
          name="8. Summarize the beneficiary's education and work experience."
          label="8. Summarize the beneficiary's education and work experience."
          doc="i-129"
          annot_field_key="<feff004c0069006e00650033005f004a006f0062004400650073006300720069007000740069006f006e005b0032005d>"
          inputType="textarea"
        />
      </fieldset>
      <h3>
        9. How is the U.S. company related to the company abroad? (select only
        one box)
      </h3>
      <fieldset>
        <Input
          name="9.a. Parent"
          label="9.a. Parent"
          type="checkbox"
          doc="i-129"
          annot_field_key="<feff0061005f0050006100720065006e0074005b0030005d>"
        />
        <Input
          name="b. Branch"
          label="b. Branch"
          type="checkbox"
          doc="i-129"
          annot_field_key="<feff0062005f004200720061006e00630068005b0030005d>"
        />
        <Input
          name="c. Subsidiary"
          label="9.a. Parent"
          type="checkbox"
          doc="i-129"
          annot_field_key="<feff0063005f0053007500620073006900640069006100720079005b0030005d>"
        />
        <Input
          name="d. Affiliate"
          label="d. Affiliate"
          type="checkbox"
          doc="i-129"
          annot_field_key="<feff0064005f0041006600660069006c0069006100740065005b0030005d>"
        />
        <Input
          name="e. Joint Venture"
          label="e. Joint Venture"
          type="checkbox"
          doc="i-129"
          annot_field_key="<feff0065005f004a006f0069006e007400560065006e0074007500720065005b0030005d>"
        />
      </fieldset>
      <h3>
        10. Describe the percentage of stock ownership and managerial control of
        each company that has a qualifying relationship. Provide the Federal
        Employer Identification Number for each U.S. company that has a
        qualifying relationship.
      </h3>
      <fieldset>
        <InputTable
          columns={[
            "Percentage of company stock ownership and managerial control of each company that has a qualifying relationship.",
            "Federal Employer Identification Number for each U.S. company that has a qualifying relationship"
          ]}
          rows={[
            [
              {
                id: "<feff004c0069006e00650031005b0030005d>",
                label: "Percentage",
                name: "Percentage"
              },
              {
                id:
                  "<feff004600450049004e005f004c0069006e00650031005b0030005d>",
                label: "Identification Number",
                name: "Identification Number"
              }
            ],
            [
              {
                id: "<feff004c0069006e00650032005b0030005d>",
                label: "Percentage",
                name: "Percentage2"
              },
              {
                id:
                  "<feff004600450049004e005f004c0069006e00650032005b0030005d>",
                label: "Identification Number",
                name: "Identification Number2"
              }
            ],
            [
              {
                id: "<feff004c0069006e00650033005b0030005d>",
                label: "Percentage",
                name: "Percentage3"
              },
              {
                id:
                  "<feff004600450049004e005f004c0069006e00650033005b0030005d>",
                label: "Identification Number",
                name: "Identification Number3"
              }
            ],
            [
              {
                id: "<feff004c0069006e00650034005b0030005d>",
                label: "Percentage",
                name: "Percentage4"
              },
              {
                id:
                  "<feff004600450049004e005f004c0069006e00650034005b0030005d>",
                label: "Identification Number",
                name: "Identification Number4"
              }
            ],
            [
              {
                id: "<feff004c0069006e00650035005b0030005d>",
                label: "Percentage",
                name: "Percentage5"
              },
              {
                id:
                  "<feff004600450049004e005f004c0069006e00650035005b0030005d>",
                label: "Identification Number",
                name: "Identification Number5"
              }
            ]
          ]}
        />
      </fieldset>
      <h3>
        11. Do the companies currently have the same qualifying relationship as
        they did during the one-year period of the alien's employment with the
        company abroad?
      </h3>
      <fieldset>
        <Input
          label="11. Yes"
          name="11. Yes"
          doc="i-129"
          annot_field_key="<feff004c0053006500630031004c0069006e006500310031005f005900650073005b0030005d>"
          type="checkbox"
        />
        <Input
          label="11. No. If no, provide an explanation in Part 9. of Form I-129 that the U.S. company has and will have a qualifying
          relationship with another foreign entity during the full period of the requested period of stay."
          name="11. No. If no, provide an explanation in Part 9. of Form I-129 that the U.S. company has and will have a qualifying
          relationship with another foreign entity during the full period of the requested period of stay."
          doc="i-129"
          annot_field_key="<feff004c0053006500630031004c0069006e006500310031005f004e006f005b0030005d>"
          type="checkbox"
        />
      </fieldset>
      <h3>
        12. Is the beneficiary coming to the United States to open a new office?
      </h3>
      <fieldset>
        <Input
          label="12. Yes"
          name="12. Yes"
          doc="i-129"
          annot_field_key="<feff004c0053006500630031004c0069006e006500310032005f005900650073005b0030005d>"
          type="checkbox"
        />
        <Input
          label="11. No (attach explanation)"
          name="11. No (attach explanation)"
          doc="i-129"
          annot_field_key="<feff004c0053006500630031004c0069006e006500310032005f004e006f005b0030005d>"
          type="checkbox"
        />
      </fieldset>
      <h3>
        If you are seeking L-1B specialized knowledge status for an individual,
        answer the following question:
      </h3>
      <h3>
        13.a. Will the beneficiary be stationed primarily offsite (at the
        worksite of an employer other than the petitioner or its affiliate,
        subsidiary, or parent)?
      </h3>
      <fieldset>
        <Input
          name="13.a Yes"
          label="13.a Yes"
          doc="i-129"
          annot_field_key="<feff004c0053006500630031004c0069006e006500310032005b0031005d>"
          type="checkbox"
        />
        <Input
          name="13.a No"
          label="13.a No"
          doc="i-129"
          annot_field_key="<feff004c0053006500630031004c0069006e006500310032005b0030005d>"
          type="checkbox"
        />
      </fieldset>
      <h3>
        13.b. If you answered yes to the preceding question, describe how and by
        whom the beneficiary's work will be controlled and supervised. Include a
        description of the amount of time each supervisor is expected to control
        and supervise the work. If you need additional space to respond to this
        question, proceed to Part 9. of the Form I-129, and type or print your
        explanation.
      </h3>
      <fieldset>
        <Input
          name="  13.b. If you answered yes to the preceding question, describe how and by whom the beneficiary's work will be controlled and
        supervised. Include a description of the amount of time each supervisor is expected to control and supervise the work. If you
        need additional space to respond to this question, proceed to Part 9. of the Form I-129, and type or print your explanation."
          label="  13.b. If you answered yes to the preceding question, describe how and by whom the beneficiary's work will be controlled and
        supervised. Include a description of the amount of time each supervisor is expected to control and supervise the work. If you
        need additional space to respond to this question, proceed to Part 9. of the Form I-129, and type or print your explanation."
          doc="i-129"
          annot_field_key="<feff004c0069006e00650033005f004a006f0062004400650073006300720069007000740069006f006e005b0035005d>"
          inputType="textarea"
        />
      </fieldset>
      <h3>
        13.c. If you answered yes to the preceding question, describe the
        reasons why placement at another worksite outside the petitioner,
        subsidiary, affiliate, or parent is needed. Include a description of how
        the beneficiary's duties at another worksite relate to the need for the
        specialized knowledge he or she possesses. If you need additional space
        to respond to this question, proceed to Part 9. of the Form I-129, and
        type or print your explanation.
      </h3>
      <fieldset>
        <Input
          name="  13.b. If you answered yes to the preceding question, describe how and by whom the beneficiary's work will be controlled and
        supervised. Include a description of the amount of time each supervisor is expected to control and supervise the work. If you
        need additional space to respond to this question, proceed to Part 9. of the Form I-129, and type or print your explanation."
          label="  13.b. If you answered yes to the preceding question, describe how and by whom the beneficiary's work will be controlled and
        supervised. Include a description of the amount of time each supervisor is expected to control and supervise the work. If you
        need additional space to respond to this question, proceed to Part 9. of the Form I-129, and type or print your explanation."
          doc="i-129"
          annot_field_key="<feff004c0069006e00650033005f004a006f0062004400650073006300720069007000740069006f006e005b0034005d>"
          inputType="textarea"
        />
      </fieldset>
    </div>
    </WithValues>
  );
};

export default Section1;
