import React from "react";

const Section3 = () => {

  return (
    <div>
      <h3>Section 3. Additional Fees</h3>
      <h3>
        NOTE: A petitioner that seeks initial approval of L nonimmigrant status
        for a beneficiary, or seeks approval to employ an L nonimmigrant
        currently working for another employer, must submit an additional $500
        Fraud Prevention and Detection fee. For petitions filed on or after
        December 18, 2015, you must submit an additional fee of $4,500 if you
        responded yes to both questions in Item Numbers 4.a. and 4.b. on the
        first page of this L Classification Supplement. This $4,500 fee is
        mandated by the provisions of Public Law 114-113. These fees, when
        applicable, may not be waived. You must include payment of the fees with
        your submission of this form. Failure to submit the fees when required
        will result in rejection or denial of your submission. Each of these
        fees should be paid by separate checks or money orders.
      </h3>
    </div>
  );
};

export default Section3;
