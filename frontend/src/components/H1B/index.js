import React from "react";
import "./H1B.css";
import Section0 from "./Sections/Section0";
import Section1 from "./Sections/Section1";
import Section2 from "./Sections/Section2";
import Section3 from "./Sections/Section3";
import Section4 from "./Sections/Section4";
import { Link, Router } from "@reach/router";

const H1B = () => {

  return (
    <div>
      <h1>
        H-1B and H-1B1 Data Collection and Filing Fee Exemption Supplement
      </h1>
      <div className="Layout-Form">
        <aside className="Layout-Form-Aside">
          <ul>
            <li>
              <Link to="/trade-agreement">H-1B and H-1B1</Link>
            </li>
            {[1, 2, 3, 4].map(p => (
              <li key={p}>
                <Link to={`section-${p}`}>Section {p}</Link>
              </li>
            ))}
          </ul>
        </aside>
        <section className="Layout-Section">
          <Router>
            <Section0 path="/" />
            <Section1 path="section-1" />
            <Section2 path="section-2" />
            <Section3 path="section-3" />
            <Section4 path="section-4" />
          </Router>
        </section>
      </div>
    </div>
  );
};

export default H1B;
