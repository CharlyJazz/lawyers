import React from "react";
import WithValues from '../../../WithValues'
import Input from "../../Input";
import StateSelect from "../../StateSelect";

const Section3 = () => {

  return (
    <WithValues doc={doc}>
      <div>
        <h2>Section 3. Numerical Limitation Information</h2>
        <h3>Specify the type of H-1B petition you are filing.</h3>
        <fieldset>
          <Input
            inputType="radio"
            label="CAP H-1B Bachelor's Degree"
            name="Type"
            annot_field_key="<feff004300410050005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="CAP H-1B U.S. Master's Degree or Higher"
            name="Type"
            annot_field_key="<feff004300410050005b0031005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="CAP H-1B1 Chile/Singapore"
            name="Type"
            annot_field_key="<feff004300410050005b0032005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="CAP Exempt"
            name="Type"
            annot_field_key="<feff004300410050005b0033005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>
          If you answered Item Number 1.b. "CAP H-1B U.S. Master's Degree or
          Higher," provide the following information regarding the master's or
          higher degree the beneficiary has earned from a U.S. institution as
          defined in 20 U.S.C. 1001(a):
      </h3>
        <fieldset>
          <h3>Name of the United States Institution of Higher Education</h3>
          <Input
            label="Institution"
            name="Institution"
            annot_field_key="<feff0048003100620053006500630033004c0069006e006500320061005f004e0061006d0065005b0030005d>"
            doc="i-129"
          />
          <h3>Date Degree Awarded</h3>
          <Input
            label="Date"
            name="Date"
            annot_field_key="<feff0048003100620053006500630033004c0069006e006500320062005f00440061007400650044006500670072006500650041007700610072006400650064005b0030005d>"
            doc="i-129"
          />
          <h3>
            Type of United States Degree
        </h3>
          <Input
            label="Degree"
            name="Degree"
            annot_field_key="<feff0048003100620053006500630033004c0069006e006500320063005f0054007900700065006f0066004400650067007200650065005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>
          Address of the United States institution of higher education
      </h3>
        <fieldset>
          <Input
            label="Street Number and Name"
            name="Street Number and Name"
            annot_field_key="<feff0048003100620053006500630033004c0069006e006500320064005f005300740072006500650074004e0061006d0065005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="checkbox"
            label="Suite"
            name="Suite"
            annot_field_key="<feff004c0069006e00650062005f0055006e00690074005b0031005d>"
            doc="i-129"
          />
          <Input
            inputType="checkbox"
            label="Apartment"
            name="Apartment"
            annot_field_key="<feff004c0069006e00650062005f0055006e00690074005b0032005d>"
            doc="i-129"
          />
          <Input
            inputType="checkbox"
            label="Floor"
            name="Floor"
            annot_field_key="<feff004c0069006e00650062005f0055006e00690074005b0030005d>"
            doc="i-129"
          />
          <Input
            label="Number"
            name="Number"
            annot_field_key="<feff004c0069006e00650062005f0041007000740053007400650046006c0072004e0075006d006200650072005b0030005d>"
            doc="i-129"
          />
          <Input
            label="City or Town"
            name="City or Town"
            annot_field_key="<feff00500061007200740037004c0069006e00650042005f00420045006d007000310043006900740079005b0030005d>"
            doc="i-129"
          />
          <StateSelect
            annot_field_key="<feff00500061007200740037004c0069006e00650042005f00420045006d0070003100530074006100740065005b0030005d>"
            doc="i-129"
            id="City or Town"
          />
          <Input
            label="ZIP Code"
            name="ZIP Code"
            annot_field_key="<feff00500061007200740037004c0069006e00650042005f00420045006d00700031005a006900700043006f00640065005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>
          If you answered Item Number 1.d. "CAP Exempt," you must specify the
          reason(s) this petition is exempt from the numerical limitation for H-1B
          classification:
      </h3>
        <fieldset>
          <Input
            inputType="checkbox"
            label="The petitioner is an institution of higher education as defined in section 101(a) of the Higher Education Act, of 1965, 20 U.S.C. 1001(a)."
            name="Higher education"
            annot_field_key="<feff00500061007200740043005f003300610043006800650063006b0062006f0078005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="checkbox"
            label="The petitioner is a nonprofit entity related to or affiliated with an institution of higher education as defined in 8 CFR 214.2(h)(8)(ii)(F)(2)."
            name="Entity Related"
            annot_field_key="<feff00500061007200740043005f003300620043006800650063006b0062006f0078005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="checkbox"
            label="The petitioner is a nonprofit research organization or a governmental research organization as defined in 8 CFR 214.2(h)(8)(ii)(F)(3)."
            name="Research Org"
            annot_field_key="<feff00500061007200740043005f003300630043006800650063006b0062006f0078005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="checkbox"
            label="The beneficiary will be employed at a qualifying cap exempt institution, organization or entity pursuant to 8 CFR 214.2(h)(8)(ii)(F)(4)."
            name="Cap exempt"
            annot_field_key="<feff00500061007200740043005f003300640043006800650063006b0062006f0078005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="checkbox"
            label="The petitioner is requesting an amendment to or extension of stay for the beneficiary's current H-1B classification."
            name="Amendement"
            annot_field_key="<feff00500061007200740043005f003300650043006800650063006b0062006f0078005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="checkbox"
            label="The beneficiary of this petition is a J-1 nonimmigrant physician who has received a waiver based on section 214(l) of the Act."
            name="Physician"
            annot_field_key="<feff00500061007200740043005f003300660043006800650063006b0062006f0078005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="checkbox"
            label="The beneficiary of this petition has been counted against the cap and (1) is applying for the remaining portion of the 6 year period of admission, or (2) is seeking an extension beyond the 6-year limitation based upon sections 104(c) or 106(a) of the American Competitiveness in the Twenty-First Century Act (AC21)."
            name="Counted Against"
            annot_field_key="<feff00500061007200740043005f003300670043006800650063006b0062006f0078005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="checkbox"
            label="The petitioner is an employer subject to the Guam-CNMI cap exemption pursuant to Public Law 110-229."
            name="Employer subject"
            annot_field_key="<feff00500061007200740043005f003300680043006800650063006b0062006f0078005b0030005d>"
            doc="i-129"
          />
        </fieldset>
      </div>
    </WithValues>
  );
};

export default Section3;
