import React from "react";
import Input from "../../Input";
import WithValues from '../../../WithValues'


const Section4 = () => {

  return (
    <WithValues doc={doc}>
      <div>
        <h2>Section 4. Off-Site Assignment of H-1B Beneficiaries</h2>
        <h3>
          The beneficiary of this petition will be assigned to work at an off-site
          location for all or part of the period for which H-1B classification
          sought.
      </h3>
        <fieldset>
          <Input
            inputType="radio"
            label="Yes"
            name="work"
            annot_field_key="<feff0048003100420053006500630034004c0069006e006500310061005f005900650073005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="No"
            name="work"
            annot_field_key="<feff0048003100420053006500630034004c0069006e006500310061005f004e006f005b0030005d>"
            doc="i-129"
          />
          <h4>If no, do not complete Item Numbers 2. and 3.</h4>
        </fieldset>
        <h3>
          Placement of the beneficiary off-site during the period of employment
          will comply with the statutory and regulatory requirements of the H-1B
          nonimmigrant classification.
      </h3>
        <fieldset>
          <Input
            inputType="radio"
            label="Yes"
            name="Off-site"
            annot_field_key="<feff0048003100420053006500630034004c0069006e006500310062005f005900650073005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="No"
            name="Off-site"
            annot_field_key="<feff0048003100420053006500630034004c0069006e006500310062005f004e006f005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>
          The beneficiary will be paid the higher of the prevailing or actual wage
          at any and all off-site locations.
      </h3>
        <fieldset>
          <Input
            inputType="radio"
            label="Yes"
            name="Paid"
            annot_field_key="<feff0048003100420053006500630034004c0069006e006500310063005f005900650073005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="No"
            name="Paid"
            annot_field_key="<feff0048003100420053006500630034004c0069006e006500310063005f004e006f005b0030005d>"
            doc="i-129"
          />
        </fieldset>
      </div>
    </WithValues>
  );
};

export default Section4;
