import React from "react";
import Input from "../../Input";
import WithValues from '../../../WithValues'

const Section2 = () => {

  return (
    <WithValues doc={doc}>
      <div>
        <h2>Section 2. Fee Exemption and/or Determination</h2>
        <h4>
          In order for USCIS to determine if you must pay the additional $1,500 or
          $750 American Competitiveness and Workforce Improvement Act (ACWIA) fee,
          answer all of the following questions
      </h4>
        <h3>
          Are you an institution of higher education as defined in section 101(a)
          of the Higher Education Act of 1965, 20 U.S.C. 1001(a)?
      </h3>
        <fieldset>
          <Input
            inputType="radio"
            label="Yes"
            name="Education"
            annot_field_key="<feff0048003100420053006500630032004c0069006e00650031005f005900650073005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="No"
            name="Education"
            annot_field_key="<feff0048003100420053006500630032004c0069006e00650031005f004e006f005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>
          Are you a nonprofit organization or entity related to or affiliated with
          an institution of higher education, as defined in 8 CFR
          214.2(h)(19)(iii)(B)?
      </h3>
        <fieldset>
          <Input
            inputType="radio"
            label="Yes"
            name="Organization"
            annot_field_key="<feff0048003100420053006500630032004c0069006e00650032005f005900650073005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="No"
            name="Organization"
            annot_field_key="<feff0048003100420053006500630032004c0069006e00650032005f004e006f005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>
          Are you a nonprofit research organization or a governmental research
          organization, as defined in 8 CFR 214.2(h)(19)(iii)(C)?
      </h3>
        <fieldset>
          <Input
            inputType="radio"
            label="Yes"
            name="Research"
            annot_field_key="<feff0048003100420053006500630032004c0069006e00650033005f005900650073005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="No"
            name="Research"
            annot_field_key="<feff0048003100420053006500630032004c0069006e00650033005f004e006f005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>
          Is this the second or subsequent request for an extension of stay that
          this petitioner has filed for this alien?
      </h3>
        <fieldset>
          <Input
            inputType="radio"
            label="Yes"
            name="Request"
            annot_field_key="<feff0048003100420053006500630032004c0069006e00650034005f005900650073005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="No"
            name="Request"
            annot_field_key="<feff0048003100420053006500630032004c0069006e00650034005f004e006f005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>
          Is this an amended petition that does not contain any request for
          extensions of stay?
      </h3>
        <fieldset>
          <Input
            inputType="radio"
            label="Yes"
            name="Amended"
            annot_field_key="<feff0048003100420053006500630032004c0069006e00650035005f005900650073005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="No"
            name="Amended"
            annot_field_key="<feff0048003100420053006500630032004c0069006e00650035005f004e006f005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>Are you filing this petition to correct a USCIS error?</h3>
        <fieldset>
          <Input
            inputType="radio"
            label="Yes"
            name="Filing"
            annot_field_key="<feff0048003100420053006500630032004c0069006e00650036005f005900650073005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="No"
            name="Filing"
            annot_field_key="<feff0048003100420053006500630032004c0069006e00650036005f004e006f005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>
          7. Is the petitioner a primary or secondary education institution?
      </h3>
        <fieldset>
          <Input
            inputType="radio"
            label="Is the petitioner a primary or secondary education institution? Yes"
            name="Is the petitioner a primary or secondary education institution? YES"
            annot_field_key="<feff0048003100420053006500630032004c0069006e00650037005f005900650073005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label=" Is the petitioner a primary or secondary education institution? No"
            name="Is the petitioner a primary or secondary education institution? NO"
            annot_field_key="<feff0048003100420053006500630032004c0069006e00650037005f004e006f005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>
          Is the petitioner a nonprofit entity that engages in an established
          curriculum-related clinical training of students registered at such an
          institution?
      </h3>
        <fieldset>
          <Input
            inputType="radio"
            label="Yes"
            name="Registered"
            annot_field_key="<feff0048003100420053006500630032004c0069006e00650038005f005900650073005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="No"
            name="Registered"
            annot_field_key="<feff0048003100420053006500630032004c0069006e00650038005f004e006f005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h4>
          If you answered yes to any of the questions above, you are not required
          to submit the ACWIA fee for your H-1B Form I-129 petition. If you
          answered no to all questions, answer Item Number 9. below.
      </h4>
        <h3>
          Do you currently employ a total of 25 or fewer full-time equivalent
          employees in the United States, including all affiliates or subsidiaries
          of this company/organization?
      </h3>
        <fieldset>
          <Input
            inputType="radio"
            label="Yes"
            name="Subsidiaries"
            annot_field_key="<feff0048003100420053006500630032004c0069006e00650039005f005900650073005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="No"
            name="Subsidiaries"
            annot_field_key="<feff0048003100420053006500630032004c0069006e00650039005f004e006f005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h4>
          <p>
            If you answered yes, to Item Number 9. above, you are required to pay
            an additional ACWIA fee of $750. If you answered no, then you are
            required to pay an additional ACWIA fee of $1,500.
        </p>
          <p>
            <strong>NOTE:</strong> A petitioner seeking initial approval of H-1B
            nonimmigrant status for a beneficiary, or seeking approval to employ
            an H-1B nonimmigrant currently working for another employer, must
            submit an additional $500 Fraud Prevention and Detection fee. For
            petitions filed on or after December 18, 2015, an additional fee of
            $4,000 must be submitted if you responded yes to Item Numbers 1.d. and
            1.d.1. of Section 1. of this supplement. This $4,000 fee was mandated
            by the provisions of Public Law 114-113.
        </p>
          <p>
            The Fraud Prevention and Detection Fee and Public Law 114-113 fee do
            not apply to H-1B1 petitions. These fees, when applicable, may not be
            waived. You must include payment of the fees when you submit this
            form. Failure to submit the fees when required will result in
            rejection or denial of your submission. Each of these fees should be
            paid by separate checks or money orders.
        </p>
        </h4>
      </div>
    </WithValues>
  );
};

export default Section2;
