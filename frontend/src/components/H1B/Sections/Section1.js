import React from "react";
import Input from "../../Input";
import WithValues from '../../../WithValues'

const Section1 = () => {

  return (
    <WithValues doc={doc}>
    <div>
      <h2>Section 1. General Information</h2>
      <h3>Employer Information - (select all items that apply)</h3>
      <fieldset>
        <h3>Is the petitioner an H-1B dependent employer?</h3>
        <Input
          inputType="radio"
          label="Yes"
          name="Dependent"
          annot_field_key="<feff0048003100420053006500630041004c0069006e006500310061005f005900650073005b0030005d>"
          doc="i-129"
        />
        <Input
          inputType="radio"
          label="No"
          name="Dependent"
          annot_field_key="<feff0048003100420053006500630041004c0069006e006500310061005f004e006f005b0030005d>"
          doc="i-129"
        />
        <h3>
          Has the petitioner ever been found to be a willful violator?
        </h3>
        <Input
          inputType="radio"
          label="Yes"
          name="Violator"
          annot_field_key="<feff0048003100420053006500630041004c0069006e006500310062005f005900650073005b0030005d>"
          doc="i-129"
        />
        <Input
          inputType="radio"
          label="No"
          name="Violator"
          annot_field_key="<feff0048003100420053006500630041004c0069006e006500310062005f004e006f005b0030005d>"
          doc="i-129"
        />
        <h3>
          Is the beneficiary an H-1B nonimmigrant exempt from the Department of Labor attestation
          requirements?
        </h3>
        <fieldset>
          <Input
            name="Is the beneficiary an H-1B nonimmigrant exempt from the Department of Labor attestation
            requirements? YES"
            label="Is the beneficiary an H-1B nonimmigrant exempt from the Department of Labor attestation
            requirements? YES"
            doc="i-129"
            annot_field_key="<feff0048003100420053006500630041004c0069006e006500310063005f005900650073005b0030005d>"
            inputType="radio"
          />
          <Input
            name="Is the beneficiary an H-1B nonimmigrant exempt from the Department of Labor attestation
            requirements? NO"
            label="Is the beneficiary an H-1B nonimmigrant exempt from the Department of Labor attestation
            requirements? NO"
            doc="i-129"
            annot_field_key="<feff0048003100420053006500630041004c0069006e006500310063005f004e006f005b0030005d>"
            inputType="radio"
          />
        <h3>
          If yes, is it because the beneficiary's annual rate of pay is equal to
          at least $60,000?
        </h3>
        <Input
          inputType="radio"
          label="Yes"
          name="Pay"
          annot_field_key="<feff0048003100420053006500630041004c0069006e0065003100630031005f005900650073005b0030005d>"
          doc="i-129"
        />
        <Input
          inputType="radio"
          label="No"
          name="Pay"
          annot_field_key="<feff0048003100420053006500630041004c0069006e0065003100630031005f004e006f005b0030005d>"
          doc="i-129"
        />
        <h3>
          Or is it because the beneficiary has a master's degree or higher
          degree in a specialty related to the employment?
        </h3>
        <Input
          inputType="radio"
          label="Yes"
          name="Specialty"
          annot_field_key="<feff0048003100420053006500630041004c0069006e0065003100630032005f005900650073005b0030005d>"
          doc="i-129"
        />
        <Input
          inputType="radio"
          label="No"
          name="Specialty"
          annot_field_key="<feff0048003100420053006500630041004c0069006e0065003100630032005f004e006f005b0030005d>"
          doc="i-129"
        />
        </fieldset>
        <h3>
          Does the petitioner employ 50 or more individuals in the United
          States?
        </h3>
        <fieldset>
        <Input
          inputType="radio"
          label="Yes"
          name="Individuals"
          annot_field_key="<feff0048003100420053006500630041004c0069006e006500310064005f005900650073005b0030005d>"
          doc="i-129"
        />
        <Input
          inputType="radio"
          label="No"
          name="Individuals"
          annot_field_key="<feff0048003100420053006500630041004c0069006e006500310064005f004e006f005b0030005d>"
          doc="i-129"
        />
        <h3>
          If yes, are more than 50 percent of those employees in H-1B, L-1A, or
          L-1B nonimmigrant status?
        </h3>
        <Input
          inputType="radio"
          label="Yes"
          name="Status"
          annot_field_key="<feff0048003100420053006500630041004c0069006e0065003100640031005f005900650073005b0030005d>"
          doc="i-129"
        />
        <Input
          inputType="radio"
          label="No"
          name="Status"
          annot_field_key="<feff0048003100420053006500630041004c0069006e0065003100640031005f004e006f005b0030005d>"
          doc="i-129"
        />
      </fieldset>
      </fieldset>
      <h3>Beneficiary's Highest Level of Education</h3>
      <fieldset>
        <Input
          inputType="radio"
          label="NO DIPLOMA"
          name="Level"
          annot_field_key="<feff0061005f006e006f005f006400690070006c006f006d0061005b0030005d>"
          doc="i-129"
        />
        <Input
          inputType="radio"
          label="HIGH SCHOOL GRADUATE DIPLOMA or
          the equivalent (for example: GED)"
          name="Level"
          annot_field_key="<feff0062005f00480053004400690070006c006f006d0061005b0030005d>"
          doc="i-129"
        />
        <Input
          inputType="radio"
          label="Some college credit, but less than 1 year"
          name="Level"
          annot_field_key="<feff0063005f0073006f006d0065005f0063006f006c006c006500670065005b0030005d>"
          doc="i-129"
        />
        <Input
          inputType="radio"
          label="One or more years of college, no degree"
          name="Level"
          annot_field_key="<feff0064005f0063006f006c006c0065006700650070006c00750073005b0030005d>"
          doc="i-129"
        />
        <Input
          inputType="radio"
          label="Associate's degree (for example: AA, AS)"
          name="Level"
          annot_field_key="<feff0065005f004100730073006f00630069006100740065004400650067007200650065005b0030005d>"
          doc="i-129"
        />
        <Input
          inputType="radio"
          label="Bachelor's degree (for example: BA, AB, BS)"
          name="Level"
          annot_field_key="<feff0066005f00420061006300680065006c006f0072004400650067007200650065005b0030005d>"
          doc="i-129"
        />
        <Input
          inputType="radio"
          label="Master's degree (for example: MA, MS, MEng, MEd,
            MSW, MBA)"
          name="Level"
          annot_field_key="<feff0067005f004d00610073007400650072004400650067007200650065005b0030005d>"
          doc="i-129"
        />
        <Input
          inputType="radio"
          label="Professional degree (for example: MD, DDS, DVM, LLB, JD)"
          name="Level"
          annot_field_key="<feff0068005f00500072006f00660065007300730069006f006e0061006c004400650067007200650065005b0030005d>"
          doc="i-129"
        />
        <Input
          inputType="radio"
          label="Doctorate degree (for example: PhD, EdD)"
          name="Level"
          annot_field_key="<feff0069005f0044006f00630074006f0072006100740065004400650067007200650065005b0030005d>"
          doc="i-129"
        />
      </fieldset>
      <h3>Major/Primary Field of Study</h3>
      <fieldset>
        <Input
          label="Field of Study"
          name="Field of Study"
          annot_field_key="<feff00500061007200740041005f00710033005f004600690065006c0064005f006f0066005f00530074007500640079005b0030005d>"
          doc="i-129"
        />
      </fieldset>
      <h3>Rate of Pay Per Year</h3>
      <fieldset>
        <Input
          label="Rate of Pay Per Year"
          name="Rate of Pay Per Year"
          annot_field_key="<feff004c0069006e00650034005f0052006100740065006f00660050006100790050006500720059006500610072005b0030005d>"
          doc="i-129"
        />
      </fieldset>
      <h3>DOT Code</h3>
      <fieldset>
        <Input
          label="DOT Code"
          name="DOT Code"
          annot_field_key="<feff004c0069006e00650035005f0044004f00540043006f00640065005b0030005d>"
          doc="i-129"
        />
      </fieldset>
      <h3>NAICS Code</h3>
      <fieldset>
        <Input
          label="NAICS Code"
          name="NAICS Code"
          annot_field_key="<feff004c0069006e00650036005f004e00410049004300530043006f00640065005b0030005d>"
          doc="i-129"
        />
      </fieldset>
    </div>
    </WithValues>
  );
};

export default Section1;
