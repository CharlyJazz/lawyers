import React from "react";
import WithValues from '../../../WithValues'
import Input from "../../Input";

const Section0 = () => {

  return (
    <WithValues doc={doc}>
      <div>
        <h3>Name of Petitioner</h3>
        <fieldset>
          <Input
            label="Petitioner"
            name="Petitioner"
            annot_field_key="<feff004c0069006e00650031005f00460061006d0069006c0079004e0061006d0065005b0035005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>Name of the Beneficiary</h3>
        <fieldset>
          <Input
            label="Beneficiary"
            name="Beneficiary"
            annot_field_key="<feff004c0069006e00650031005f00460061006d0069006c0079004e0061006d0065005b0034005d>"
            doc="i-129"
          />
        </fieldset>
      </div>
    </WithValues>
  );
};

export default Section0;
