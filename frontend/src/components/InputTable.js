import React from "react";
import Input from "./Input";

const InputTable = ({ columns, rows, ...props }) => {
  return (
    <div>
      <table
        style={{
          width: "80%",
          textAlign: "center",
          marginTop: 15
        }}
      >
        <thead>
          <tr>
            {columns.map(column => (
              <th
                style={{
                  border: "1px solid",
                  borderRadius: "3.2px",
                  padding: "8px"
                }}
                key={column}
              >
                {column}
              </th>
            ))}
          </tr>
        </thead>
        <tbody>
          {rows.map((row, index) => (
            <tr key={index}>
              {row.map(column => (
                <td
                  style={{
                    border: "1px solid",
                    borderRadius: "3.2px",
                    padding: "8px"
                  }}
                  key={column.id}
                >
                  <Input
                    inputType="input-table"
                    label={column.label}
                    name={column.name}
                    annot_field_key={column.id}
                    doc="i-129"
                  />
                </td>
              ))}
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default InputTable;
