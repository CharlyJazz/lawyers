import React from "react";
import { AppContext } from "../../state";
import "./Login.css";
import { URL_BASE } from "../../constants";

const Login = () => {
  const [, dispatch] = React.useContext(AppContext);
  const isDev = process.env.NODE_ENV === "development";
  const [password, setPassword] = React.useState(isDev ? "123456" : "");
  const [email, setEmail] = React.useState(isDev ? "email@email.com" : "");

  const handleSubmit = event => {
    event.preventDefault();

    fetch(URL_BASE + "/login", {
      method: "POST",
      body: JSON.stringify({
        email,
        password
      }),
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(res => res.json())
      .then(res => {
        localStorage.setItem("jwt", res.access_token);
        localStorage.setItem("user", JSON.stringify(res.user));
        dispatch({
          type: "LOGIN_SUCCESS",
          user: res.user,
          token: res.access_token
        });
      });
  };

  return (
    <div className="Login">
      <form className="Login__form" onSubmit={handleSubmit}>
        <div className="Login__form-group">
          <label>Email</label>
          <input
            type="email"
            required
            placeholder="Email Address"
            value={email}
            onChange={event => setEmail(event.target.value)}
          />
        </div>
        <div className="Login__form-group">
          <label>Password</label>
          <input
            type="password"
            required
            placeholder="Password"
            value={password}
            onChange={event => setPassword(event.target.value)}
          />
        </div>
        <button className="Login__form-button">Login</button>
      </form>
    </div>
  );
};

export default Login;
