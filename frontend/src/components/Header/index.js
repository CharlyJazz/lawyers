import React from "react";
import notificationIcon from "../../images/notification.svg";
import { Link } from "@reach/router";
import "./Header.css";

const Header = () => {
  return (
    <header className="Header">
      <div>
        <h2>
          <Link to="/">Lawyers</Link>
        </h2>
      </div>
      <div className="Header__right">
        <button className="Header__notification">
          <img src={notificationIcon} alt="Notification" />
        </button>
        <div className="Header__user-info">
          <span className="Header__user-name">Carlos</span>
        </div>
      </div>
    </header>
  );
};

export default Header;
