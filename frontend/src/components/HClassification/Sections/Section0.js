import React from "react";
import Input from "../../Input";
import InputTable from "../../InputTable";
import WithValues from '../../../WithValues'

const Section0 = () => {

  return (
    <WithValues doc={doc}>
    <div>
      <h3>Name of Petitioner</h3>
      <fieldset>
        <Input
          label="Petitioner"
          name="Petitioner"
          annot_field_key="<feff004c0069006e00650031005f005000650074006900740069006f006e00650072004e0061006d0065005b0030005d>"
          doc="i-129"
        />
      </fieldset>
      <h3>
        Name of the beneficiary or if this petition includes multiple
        beneficiaries, the total number of beneficiaries
      </h3>
      <h3>Name of the Beneficiary</h3>
      <fieldset>
        <Input
          label="Beneficiary"
          name="Beneficiary"
          annot_field_key="<feff004c0069006e00650032005f00420065006e00650066006900630069006100720079004e0061006d0065005b0030005d>"
          doc="i-129"
        />
      </fieldset>
      <h4>OR</h4>
      <h3>Provide the total number of beneficiaries</h3>
      <fieldset>
        <Input
          label="Beneficiaries"
          name="Beneficiaries"
          annot_field_key="<feff004c0069006e00650032005f00540074006c004e0075006d006200650072006f006600420065006e0065006600690063006900610072006900650073005b0030005d>"
          doc="i-129"
        />
      </fieldset>
      <h3>
        <p>
          List each beneficiary's prior periods of stay in H or L classification
          in the United States for the last six years (beneficiaries requesting
          H-2A or H-2B classification need only list the last three years). Be
          sure to only list those periods in which each beneficiary was actually
          in the United States in an H or L classification. Do not include
          periods in which the beneficiary was in a dependent status, for
          example, H-4 or L-2 status.
        </p>
        <p>
          NOTE: Submit photocopies of Forms I-94, I-797, and/or other USCIS
          issued documents noting these periods of stay in the H or L
          classification. (If more space is needed, attach an additional sheet.)
        </p>
      </h3>
      <fieldset>
        <InputTable
          columns={[
            "Subject's Name",
            "Period of Stay From (mm/dd/yyyy)",
            "Period of Stay To (mm/dd/yyyy)"
          ]}
          rows={[
            [
              {
                id:
                  "<feff004e0061006d0065005f004c0069006e00650031005b0030005d>",
                label: "Subject's Name",
                name: "Subject's Name"
              },
              {
                id:
                  "<feff004400610074006500460072006f006d005f004c0069006e00650031005b0030005d>",
                label: "From",
                name: "From"
              },
              {
                id:
                  "<feff00440061007400650054006f005f004c0069006e00650031005b0030005d>",
                label: "To",
                name: "To"
              }
            ],
            [
              {
                id:
                  "<feff004e0061006d0065005f004c0069006e00650032005b0030005d>",
                label: "Subject's Name",
                name: "Subject's Name2"
              },
              {
                id:
                  "<feff004400610074006500460072006f006d005f004c0069006e00650032005b0030005d>",
                label: "From",
                name: "From2"
              },
              {
                id:
                  "<feff00440061007400650054006f005f004c0069006e00650032005b0030005d>",
                label: "To",
                name: "To2"
              }
            ],
            [
              {
                id:
                  "<feff004e0061006d0065005f004c0069006e00650033005b0030005d>",
                label: "Subject's Name",
                name: "Subject's Name3"
              },
              {
                id:
                  "<feff004400610074006500460072006f006d005f004c0069006e00650033005b0030005d>",
                label: "From",
                name: "From3"
              },
              {
                id:
                  "<feff00440061007400650054006f005f004c0069006e00650033005b0030005d>",
                label: "To",
                name: "To3"
              }
            ],
            [
              {
                id:
                  "<feff004e0061006d0065005f004c0069006e00650034005b0030005d>",
                label: "Subject's Name",
                name: "Subject's Name4"
              },
              {
                id:
                  "<feff004400610074006500460072006f006d005f004c0069006e00650034005b0030005d>",
                label: "From",
                name: "From4"
              },
              {
                id:
                  "<feff00440061007400650054006f005f004c0069006e00650034005b0030005d>",
                label: "To",
                name: "To4"
              }
            ],
            [
              {
                id:
                  "<feff004e0061006d0065005f004c0069006e00650035005b0030005d>",
                label: "Subject's Name",
                name: "Subject's Name5"
              },
              {
                id:
                  "<feff004400610074006500460072006f006d005f004c0069006e00650035005b0030005d>",
                label: "From",
                name: "From5"
              },
              {
                id:
                  "<feff00440061007400650054006f005f004c0069006e00650035005b0030005d>",
                label: "To",
                name: "To5"
              }
            ],
            [
              {
                id:
                  "<feff004e0061006d0065005f004c0069006e00650036005b0030005d>",
                label: "Subject's Name",
                name: "Subject's Name6"
              },
              {
                id:
                  "<feff004400610074006500460072006f006d005f004c0069006e00650036005b0030005d>",
                label: "From",
                name: "From6"
              },
              {
                id:
                  "<feff00440061007400650054006f005f004c0069006e00650036005b0030005d>",
                label: "To",
                name: "To6"
              }
            ]
          ]}
        />
      </fieldset>
      <h3>Classification sought</h3>
      <fieldset>
        <Input
          inputType="radio"
          label="H-1B Specialty Occupation"
          name="Classification"
          annot_field_key="<feff0053007500620048004c0069006e00650034005f0063006c006100730073005b0030005d>"
          doc="i-129"
        />
        <Input
          inputType="radio"
          label="H-1B1 Chile and Singapore"
          name="Classification"
          annot_field_key="<feff0053007500620048004c0069006e00650034005f0063006c006100730073005b0031005d>"
          doc="i-129"
        />
        <Input
          inputType="radio"
          label="H-1B2 Exceptional services relating to a cooperative research and development project administered by the U.S.
          Department of Defense (DOD)"
          name="Classification"
          annot_field_key="<feff0053007500620048004c0069006e00650034005f0063006c006100730073005b0032005d>"
          doc="i-129"
        />
        <Input
          inputType="radio"
          label="H-1B3 Fashion model of distinguished merit and ability"
          name="Classification"
          annot_field_key="<feff0053007500620048004c0069006e00650034005f0063006c006100730073005b0033005d>"
          doc="i-129"
        />
        <Input
          inputType="radio"
          label="H-2A Agricultural worker"
          name="Classification"
          annot_field_key="<feff0053007500620048004c0069006e00650034005f0063006c006100730073005b0034005d>"
          doc="i-129"
        />
        <Input
          inputType="radio"
          label="H-2B Non-agricultural worker"
          name="Classification"
          annot_field_key="<feff0053007500620048004c0069006e00650034005f0063006c006100730073005b0037005d>"
          doc="i-129"
        />
        <Input
          inputType="radio"
          label="H-3 Trainee"
          name="Classification"
          annot_field_key="<feff0053007500620048004c0069006e00650034005f0063006c006100730073005b0035005d>"
          doc="i-129"
        />
        <Input
          inputType="radio"
          label="H-3 Special education exchange visitor program"
          name="Classification"
          annot_field_key="<feff0053007500620048004c0069006e00650034005f0063006c006100730073005b0036005d>"
          doc="i-129"
        />
      </fieldset>
      <h3>
        Are you filing this petition on behalf of a beneficiary subject to the
        Guam-CNMI cap exemption under Public Law 110-229?
      </h3>
      <fieldset>
        <Input
          inputType="radio"
          label="Yes"
          name="Filing"
          annot_field_key="<feff0053007500700048004c0069006e00650035005f005900650073005b0031005d>"
          doc="i-129"
        />
        <Input
          inputType="radio"
          label="No"
          name="Filing"
          annot_field_key="<feff0053007500700048004c0069006e00650035005f004e006f005b0031005d>"
          doc="i-129"
        />
      </fieldset>
      <h3>
        Are you requesting a change of employer and was the beneficiary
        previously subject to the Guam-CNMI cap exemption under Public Law
        110-229?
      </h3>
      <fieldset>
        <Input
          inputType="radio"
          label="Yes"
          name="Requesting"
          annot_field_key="<feff0053007500700048004c0069006e00650035005f005900650073005b0030005d>"
          doc="i-129"
        />
        <Input
          inputType="radio"
          label="No"
          name="Requesting"
          annot_field_key="<feff0053007500700048004c0069006e00650035005f004e006f005b0030005d>"
          doc="i-129"
        />
      </fieldset>
      <h3>
        Does any beneficiary in this petition have ownership interest in the
        petitioning organization?
      </h3>
      <fieldset>
        <Input
          inputType="radio"
          label="Yes. If yes, please explain"
          name="Interest"
          annot_field_key="<feff004c0069006e006500370061005f0043006800650063006b005b0031005d>"
          doc="i-129"
        />
        <Input
          inputType="radio"
          label="No"
          name="Interest"
          annot_field_key="<feff004c0069006e006500370061005f0043006800650063006b005b0030005d>"
          doc="i-129"
        />
        <Input
          inputType="textarea"
          label="Explanation"
          name="Explanation"
          annot_field_key="<feff004c0069006e006500370062005f004500780070006c00610069006e005b0030005d>"
          doc="i-129"
        />
      </fieldset>
    </div>
    </WithValues>
  );
};

export default Section0;
