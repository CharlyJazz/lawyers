import React from "react";
import Input from "../../Input";
import StateSelect from "../../StateSelect";
import WithValues from '../../../WithValues'

const Section2 = () => {

  return (
    <WithValues doc={doc}>
      <div>
        <h2>
          Section 2. Complete This Section If Filing for H-2A or H-2B
          Classification
      </h2>
        <h3>Employment is:</h3>
        <fieldset>
          <Input
            inputType="radio"
            label="Seasonal"
            name="Employment"
            annot_field_key="<feff0061005f0073006500610073006f006e0061006c005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="Peak load"
            name="Employment"
            annot_field_key="<feff0062005f007000650061006b006c006f00610064005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="Intermittent"
            name="Employment"
            doc="i-129"
            annot_field_key="<feff0063005f0069006e007400650072006d0069007400740065006e0074005b0030005d>"
          />
          <Input
            inputType="radio"
            label="One-time occurrence"
            name="Employment"
            annot_field_key="<feff0064005f006f006e006500740069006d0065005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>Temporary need is</h3>
        <fieldset>
          <Input
            inputType="radio"
            label="Unpredictable"
            name="Temporary"
            annot_field_key="<feff0061005f0075006e007000720065006400690063007400610062006c0065005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="Periodic"
            name="Temporary"
            annot_field_key="<feff0062005f0070006500720069006f006400690063005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="Recurrent annually"
            name="Temporary"
            annot_field_key="<feff0063005f0072006500630075007200720065006e0074005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>
          Explain your temporary need for the workers' services (Attach a separate
          sheet if additional space is needed).
      </h3>
        <fieldset>
          <Input
            inputType="textarea"
            label="Explanation for need"
            name="Explanation for need"
            annot_field_key="<feff004c0069006e00650033005f004500780070006c0061006e006100740069006f006e005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>
          List the countries of citizenship for the H-2A or H-2B workers you plan
          to hire.
      </h3>
        <fieldset>
          <Input label="a." name="a." annot_field_key="<feff004c0069006e006500340061005f0043006f0075006e007400720079005b0030005d>" doc="i-129" />
          <Input label="b." name="b." annot_field_key="<feff004c0069006e006500340062005f0043006f0075006e007400720079005b0030005d>" doc="i-129" />
          <Input label="c." name="c." annot_field_key="<feff004c0069006e006500340063005f0043006f0075006e007400720079005b0030005d>" doc="i-129" />
          <Input label="d." name="d." annot_field_key="<feff004c0069006e006500340064005f0043006f0075006e007400720079005b0030005d>" doc="i-129" />
          <Input label="e." name="e." annot_field_key="<feff004c0069006e006500340065005f0043006f0075006e007400720079005b0030005d>" doc="i-129" />
          <Input label="f." name="f." annot_field_key="<feff004c0069006e006500340065005f0043006f0075006e007400720079005b0031005d>" doc="i-129" />
        </fieldset>
        <h3>
          You must provide all of the requested information for Item Numbers 5.a.
          - 6. for each H-2A or H-2B worker you plan to hire who is not from a
          country that has been designated as a participating country in
          accordance with 8 CFR 214.2(h)(5)(i)(F)(1) or 214.2(h)(6)(i)(E)(1). See
          www.uscis.gov for the list of participating countries. (Attach a
          separate sheet if additional space is needed.)
      </h3>
        <fieldset>
          <Input
            label="Family Name (Last Name)"
            name="Family Name (Last Name)"
            annot_field_key="<feff004c0069006e00650031005f00460061006d0069006c0079004e0061006d0065005b0033005d>"
            doc="i-129"
          />
          <Input
            label="Given Name (First Name)"
            name="Given Name (First Name)"
            annot_field_key="<feff004c0069006e00650031005f0047006900760065006e004e0061006d0065005b0033005d>"
            doc="i-129"
          />
          <Input
            label="Middle Name"
            name="Middle Name"
            annot_field_key="<feff004c0069006e00650031005f004d006900640064006c0065004e0061006d0065005b0033005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>Provide all other name(s) used</h3>
        <fieldset>
          <Input
            label="Family Name (Last Name) 1"
            name="Family Name (Last Name) 1"
            annot_field_key="<feff004c0069006e00650033005f00460061006d0069006c0079004e0061006d00650031005b0031005d>"
            doc="i-129"
          />
          <Input
            label="Given Name (First Name) 1"
            name="Given Name (First Name) 1"
            annot_field_key="<feff004c0069006e00650033005f0047006900760065006e004e0061006d00650031005b0031005d>"
            doc="i-129"
          />
          <Input
            label="Middle Name 1"
            name="Middle Name 1"
            annot_field_key="<feff004c0069006e00650033005f004d006900640064006c0065004e0061006d00650031005b0031005d>"
            doc="i-129"
          />
          <Input
            label="Family Name (Last Name) 2"
            name="Family Name (Last Name) 2"
            annot_field_key="<feff004c0069006e00650033005f00460061006d0069006c0079004e0061006d00650032005b0032005d>"
            doc="i-129"
          />
          <Input
            label="Given Name (First Name) 2"
            name="Given Name (First Name) 2"
            annot_field_key="<feff004c0069006e00650033005f0047006900760065006e004e0061006d00650032005b0032005d>"
            doc="i-129"
          />
          <Input
            label="Middle Name 2"
            name="Middle Name 2"
            annot_field_key="<feff004c0069006e00650033005f004d006900640064006c0065004e0061006d00650032005b0032005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>Date of Birth</h3>
        <fieldset>
          <Input
            inputType="date"
            label="DoB"
            name="DoB"
            annot_field_key="<feff004c0069006e00650036005f0044006100740065004f006600420069007200740068005b0031005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>Country of Birth</h3>
        <fieldset>
          <Input
            label="Country"
            name="Country"
            annot_field_key="<feff004c0069006e006500350064005f0043006f0075006e007400720079005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>Country of Citizenship or Nationality</h3>
        <fieldset>
          <Input
            label="Citizen Country"
            name="Citizen Country"
            annot_field_key="<feff004c0069006e006500350065005f0043006f0075006e007400720079005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>
          Have any of the workers listed in Item Number 5. above ever been
          admitted to the United States previously in H-2A/H-2B status?
      </h3>
        <fieldset>
          <Input
            inputType="radio"
            label="Yes. If yes, go to Part 9. of Form I-129 and write your explanation."
            name="Admitted"
            annot_field_key="<feff0053007500700048004c0069006e006500360061005b0031005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="No"
            name="Admitted"
            annot_field_key="<feff0053007500700048004c0069006e006500360061005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>Visa Classification (H-2A or H-2B):</h3>
        <fieldset>
          <Input
            label="Visa Classification"
            name="Visa Classification"
            annot_field_key="<feff00480053007500700070004c0069006e006500360061005f00560069007300610043006c0061007300730069006600690063006100740069006f006e005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h4>
          <p>
            <strong>NOTE:</strong> If any of the H-2A or H-2B workers you are
            requesting are nationals of a country that is not on the eligible
            countries list, you must also provide evidence showing: (1) that
            workers with the required skills are not available from a country
            currently on the eligible countries list*; (2) whether the
            beneficiaries have been admitted previously to the United States in
            H-2A or H-2B status; (3) that there is no potential for abuse, fraud,
            or other harm to the integrity of the H-2A or H-2B visa programs
            through the potential admission of the intended workers; and (4) any
            other factors that may serve the United States interest.
        </p>
          <p>
            * For H-2A petitions only: You must also show that workers with the
            required skills are not available from among United States workers
        </p>
        </h4>
        <h3>
          Did you or do you plan to use a staffing, recruiting, or similar
          placement service or agent to locate the H-2A/H-2B workers that you
          intend to hire by filing this petition?
      </h3>
        <fieldset>
          <Input
            inputType="radio"
            label="Yes"
            name="Use"
            annot_field_key="<feff0053007500700048004c0069006e006500370061005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="No"
            name="Use"
            annot_field_key="<feff0053007500700048004c0069006e006500370061005b0031005d>"
            doc="i-129"
          />
        </fieldset>
        <h4>
          If yes, list the name and address of service or agent used below. Please
          use Part 9. of Form I-129 if you need to include the name and address of
          more than one service or agent.
      </h4>
        <h3>Name</h3>
        <fieldset>
          <Input
            label="Name to use"
            name="Name to use"
            annot_field_key="<feff0053007500700048004c0069006e00650037005f004e0061006d0065005b0030005d>"
            doc="i-129"
          />
          <h3>Adress</h3>
          <Input
            label="Street Number and Name"
            name="Street Number and Name"
            annot_field_key="<feff0053006500630032004c0069006e006500370063005f005300740072006500650074004e0075006d006200650072004e0061006d0065005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="checkbox"
            label="Suite"
            name="Suite"
            annot_field_key="<feff0053006500630032004c0069006e006500370063005f0055006e00690074005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="checkbox"
            label="Apartment"
            name="Apartment"
            annot_field_key="<feff0053006500630032004c0069006e006500370063005f0055006e00690074005b0031005d>"
            doc="i-129"
          />
          <Input
            inputType="checkbox"
            label="Floor"
            name="Floor"
            annot_field_key="<feff0053006500630032004c0069006e006500370063005f0055006e00690074005b0032005d>"
            doc="i-129"
          />
          <Input
            label="Number"
            name="Number"
            annot_field_key="<feff0053006500630032004c0069006e006500370063005f0041007000740053007400650046006c0072004e0075006d006200650072005b0030005d>"
            doc="i-129"
          />
          <Input
            label="City or Town"
            name="City or Town"
            annot_field_key="<feff0053006500630032004c0069006e006500370063005f00430069007400790054006f0077006e005b0030005d>"
            doc="i-129"
          />
          <StateSelect
            annot_field_key="<feff0053006500630032004c0069006e006500370063005f00530074006100740065005b0030005d>"
            doc="i-129"
          />
          <Input
            label="ZIP Code"
            name="ZIP Code"
            annot_field_key="<feff0053006500630032004c0069006e006500370063005f005a006900700043006f00640065005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>
          Did any of the H-2A/H-2B workers that you are requesting pay you, or an
          agent, a job placement fee or other form of compensation (either direct
          or indirect) as a condition of the employment, or do they have an
          agreement to pay you or the service such fees at a later date? The
          phrase "fees or other compensation" includes, but is not limited to,
          petition fees, attorney fees, recruitment costs, and any other fees that
          are a condition of a beneficiary's employment that the employer is
          prohibited from passing to the H-2A or H-2B worker under law under U.S.
          Department of Labor rules. This phrase does not include reasonable
          travel expenses and certain government-mandated fees (such as passport
          fees) that are not prohibited from being passed to the H-2A or H-2B
          worker by statute, regulations, or any laws.
      </h3>
        <fieldset>
          <Input
            inputType="radio"
            label="Yes"
            name="Rules"
            annot_field_key="<feff0053007500700048004c0069006e006500380061005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="No"
            name="Rules"
            annot_field_key="<feff0053007500700048004c0069006e006500380061005b0031005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>
          If yes, list the types and amounts of fees that the worker(s) paid or
          will pay.
      </h3>
        <fieldset>
          <Input
            inputType="textarea"
            label="Fees"
            name="Fees"
            annot_field_key="<feff0053007500700048004c0069006e0065003800610031005f00540079007000650073005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>
          If the workers paid any fee or compensation, were they reimbursed?
      </h3>
        <fieldset>
          <Input
            inputType="radio"
            label="Yes"
            name="Compensation"
            annot_field_key="<feff0053007500700048004c0069006e006500380063005b0031005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="No"
            name="Compensation"
            annot_field_key="<feff0053007500700048004c0069006e006500380063005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>
          If the workers agreed to pay a fee that they have not yet been paid, has
          their agreement been terminated before the workers paid the fee? (Submit
          evidence of termination or reimbursement with this petition.)
      </h3>
        <fieldset>
          <Input
            inputType="radio"
            label="Yes"
            name="Agree"
            annot_field_key="<feff0053007500700048004c0069006e006500380064005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="No"
            name="Agree"
            annot_field_key="<feff0053007500700048004c0069006e006500380064005b0031005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>
          <p>
            Have you made reasonable inquiries to determine that to the best of
            your knowledge the recruiter, facilitator, or similar employment
            service that you used has not collected, and will not collect,
            directly or indirectly, any fees or other compensation from the H-2
            workers of this petition as a condition of the H-2 workers'
            employment?
        </p>
          <p>
            <strong>NOTE:</strong> If USCIS determines that you knew, or should
            have known, that the workers requested in connection with this
            petition paid any fees or other compensation at any time as a
            condition of employment, your petition may be denied or revoked.
        </p>
        </h3>
        <fieldset>
          <Input
            inputType="radio"
            label="Yes"
            name="Reasonable"
            annot_field_key=""
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="No"
            name="Reasonable"
            annot_field_key="<feff0053007500700048004c0069006e00650039005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>
          Have you ever had an H-2A or H-2B petition denied or revoked because an
          employee paid a job placement fee or other similar compensation as a
          condition of the job offer or employment?
      </h3>
        <fieldset>
          <Input
            inputType="radio"
            label="Yes"
            name="Had"
            annot_field_key=""
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="No"
            name="Had"
            annot_field_key=""
            doc="i-129"
          />
          <Input
            label="If yes, when?"
            name="If yes, when?"
            annot_field_key="<feff00480053007500700070004c0069006e00650031003000610031005f005700680065006e005b0030005d>"
            doc="i-129"
          />
          <Input
            label="Receipt Number"
            name="Receipt Number"
            annot_field_key="<feff00480053007500700070004c0069006e00650031003000610031005f0052006500630065006900700074004e0075006d006200650072005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>
          Were the workers reimbursed for such fees and compensation? (Submit
          evidence of reimbursement.) If you answered no because you were unable
          to locate the workers, include evidence of your efforts to locate the
          workers.
      </h3>
        <fieldset>
          <Input
            inputType="radio"
            label="Yes"
            name="Compensation"
            annot_field_key="<feff0053007500700048004c0069006e0065003100300062005f005900650073005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="No"
            name="Compensation"
            annot_field_key="<feff0053007500700048004c0069006e0065003100300062005f004e006f005b0030005d>"
            doc="i-129"
          />
          <h4>
            If yes, document the workers' periods of stay in the table on the
            first page of this supplement. Submit evidence of each entry and each
            exit, with the petition, as evidence of the interrupted stays.
        </h4>
        </fieldset>
        <h3>
          11. Have any of the workers you are requesting experienced an interrupted stay associated with their entry as
          an H-2A or H-2B? (See form instructions for more information on interrupted stays.)

          If yes, document the workers' periods of stay in the table on the first page of this supplement. Submit
          evidence of each entry and each exit, with the petition, as evidence of the interrupted stays.
      </h3>
        <fieldset>
          <Input
            label="YES"
            name="YES"
            annot_field_key="<feff0053007500700048004c0069006e006500310031005f005900650073005b0030005d>"
            type="checkbox"
            doc="i-129"
          />
          <Input
            label="NO"
            name="NO"
            annot_field_key="<feff0053007500700048004c0069006e006500310031005f004e006f005b0030005d>"
            type="checkbox"
            doc="i-129"
          />
        </fieldset>
        <h3>
          If you are an H-2A petitioner, are you a participant in the E-Verify
          program?
      </h3>
        <fieldset>
          <Input
            inputType="radio"
            label="Yes"
            name="E-Verify"
            annot_field_key="<feff0053007500700048004c0069006e006500310032005b0031005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="No"
            name="E-Verify"
            annot_field_key="<feff0053007500700048004c0069006e006500310032005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>If yes, provide the E-Verify Company ID or Client Company ID.</h3>
        <fieldset>
          <Input
            label="Company ID or Client Company ID"
            name="Company ID or Client Company ID"
            annot_field_key="<feff00480053007500700070004c0069006e0065003100320061005f0043006f006d00700061006e007900490044005b0030005d>"
            doc="i-129"
          />
        </fieldset>
      </div>
    </WithValues>
  );
};

export default Section2;
