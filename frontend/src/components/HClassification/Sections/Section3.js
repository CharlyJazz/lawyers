import React from "react";
import WithValues from '../../../WithValues'
import Input from "../../Input";

const Section3 = () => {

  return (
    <WithValues doc={doc}>
      <div>
        <h2>Section 3. Complete This Section If Filing for H-3 Classification</h2>
        <h3>
          If you answer yes to any of the following questions, attach a full
          explanation.
      </h3>
        <h3>
          Is the training you intend to provide, or similar training, available in
          the beneficiary's country?
      </h3>
        <fieldset>
          <Input
            inputType="radio"
            label="Yes"
            name="Training"
            annot_field_key="<feff00480053006500630033004c0069006e006500310061005f005900650073005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="No"
            name="Training"
            annot_field_key="<feff00480053006500630033004c0069006e006500310061005f004e006f005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>
          Will the training benefit the beneficiary in pursuing a career abroad?
      </h3>
        <fieldset>
          <Input
            inputType="radio"
            label="Yes"
            name="Benefit"
            annot_field_key="<feff00480053006500630033004c0069006e006500310062005f005900650073005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="No"
            name="Benefit"
            annot_field_key="<feff00480053006500630033004c0069006e006500310062005f004e006f005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>
          Does the training involve productive employment incidental to the
          training? If yes, explain the amount of compensation employment versus
          the classroom in Part 9. of Form I-129.
      </h3>
        <fieldset>
          <Input
            inputType="radio"
            label="Yes"
            name="Employment"
            annot_field_key="<feff00480053006500630033004c0069006e006500310063005f005900650073005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="No"
            name="Employment"
            annot_field_key="<feff00480053006500630033004c0069006e006500310063005f004e006f005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>Does the beneficiary already have skills related to the training?</h3>
        <fieldset>
          <Input
            inputType="radio"
            label="Yes"
            name="Skills"
            annot_field_key="<feff00480053006500630033004c0069006e006500310064005f005900650073005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="No"
            name="Skills"
            annot_field_key="<feff00480053006500630033004c0069006e006500310064005f004e006f005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>Is this training an effort to overcome a labor shortage?</h3>
        <fieldset>
          <Input
            inputType="radio"
            label="Yes"
            name="Effort"
            annot_field_key="<feff00480053006500630033004c0069006e006500310065005f005900650073005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="No"
            name="Effort"
            annot_field_key="<feff00480053006500630033004c0069006e006500310065005f004e006f005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>
          Do you intend to employ the beneficiary abroad at the end of this
          training?
      </h3>
        <fieldset>
          <Input
            inputType="radio"
            label="Yes"
            name="Abroad"
            annot_field_key="<feff00480053006500630033004c0069006e00650037005f0043006800650063006b005b0031005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="No"
            name="Abroad"
            annot_field_key="<feff00480053006500630033004c0069006e00650037005f0043006800650063006b005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>
          If you do not intend to employ the beneficiary abroad at the end of this
          training, explain why you wish to incur the cost of providing this
          training and your expected return from this training.
      </h3>
        <fieldset>
          <Input
            inputType="textarea"
            label="Explanation"
            name="Explanation"
            annot_field_key="<feff004c0069006e00650032005f004500780070006c0061006e006100740069006f006e005b0030005d>"
            doc="i-129"
          />
        </fieldset>
      </div>
    </WithValues>
  );
};

export default Section3;
