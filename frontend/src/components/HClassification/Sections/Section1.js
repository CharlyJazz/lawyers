import React from "react";
import WithValues from '../../../WithValues'
import Input from "../../Input";

const Section1 = () => {

  return (
    <WithValues doc={doc}>
      <div>
        <h2>
          Section 1. Complete This Section If Filing for H-1B Classification
      </h2>
        <h3>Describe the proposed duties.</h3>
        <fieldset>
          <Input
            inputType="textarea"
            label="Describe the proposed duties."
            name="Describe the proposed duties."
            annot_field_key="<feff004c0069006e00650031005f004400750074006900650073005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="textarea"
            label="Describe the beneficiary's present occupation and summary of prior work experience."
            name="Describe the beneficiary's present occupation and summary of prior work experience."
            annot_field_key="<feff004c0069006e00650032005f00530075006d006d006100720079006f00660057006f0072006b0045007800700065007200690065006e00630065005b0030005d>"
            doc="i-129"
          />
        </fieldset>
      </div>
    </WithValues>
  );
};

export default Section1;
