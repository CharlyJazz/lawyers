import React from "react";
import "./HClassification.css";
import Section0 from "./Sections/Section0";
import Section1 from "./Sections/Section1";
import Section2 from "./Sections/Section2";
import Section3 from "./Sections/Section3";
import { Link, Router } from "@reach/router";

const HClassification = () => {
  const doc = useDoc("i-129");

  return (
    <div>
      <h1>H Classification Supplement to Form I-129</h1>
      <div className="Layout-Form">
        <aside className="Layout-Form-Aside">
          <ul>
            <li>
              <Link to="/trade-agreement">H Classification Supplement</Link>
            </li>
            {[1, 2, 3].map(p => (
              <li key={p}>
                <Link to={`section-${p}`}>Section {p}</Link>
              </li>
            ))}
          </ul>
        </aside>
        <section className="Layout-Section">
          <Router>
            <Section0 path="/" />
            <Section1 path="section-1" />
            <Section2 path="section-2" />
            <Section3 path="section-3" />
          </Router>
        </section>
      </div>
    </div>
  );
};

export default HClassification;
