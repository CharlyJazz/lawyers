import React from "react";
import PropTypes from "prop-types";
import { URL_BASE } from "../constants";

const Input = ({ annot_field_key, doc, inputType, ...props }) => {
  const sendValueToBackend = () => {
    const { value, type, checked } = document.getElementById(props.name);
    let isCheckedInput = false;
    if (type === "checkbox" || type === "radio") {
      isCheckedInput = true;
    }
    fetch(URL_BASE + "/field", {
      method: "POST",
      body: JSON.stringify({
        doc,
        annot_field_key,
        value: isCheckedInput ? checked : value
      }),
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("jwt")}`
      }
    })
      .then(res => res.json())
      .then(res => console.log(res))
      .catch(err => console.log(err));
  };
  let input;
  switch (inputType) {
    case "input-table":
      input = (
        <input
          style={{
            display: "block",
            width: "100%",
            margin: "20px 0",
            height: 22,
            padding: "0 10px"
          }}
          {...props}
          id={props.name}
          placeholder={`Write the ${props.placeholder || props.name}`}
          onBlur={sendValueToBackend}
          data-annot={annot_field_key}
        />
      );
      break;
    case "textarea":
      input = (
        <label htmlFor={props.name}>
          {props.name || props.label}
          <textarea
            style={{
              display: "block",
              width: 200,
              margin: "20px 0",
              padding: "0 10px"
            }}
            {...props}
            id={props.name}
            placeholder={`Write the ${props.name}`}
            onBlur={sendValueToBackend}
            rows="10"
            data-annot={annot_field_key}
            cols="50"
          />
        </label>
      );
      break;
    case "date":
      input = (
        <label htmlFor={props.name}>
          {props.name || props.label}
          <input
            style={{
              display: "block",
              width: 200,
              margin: "20px 0",
              height: 22,
              padding: "0 10px"
            }}
            onBlur={sendValueToBackend}
            type="date"
            htmlFor={props.name}
            data-annot={annot_field_key}
          />
        </label>
      );

      break;
    case "radio":
      input = (
        <label
          style={{
            display: "flex",
            alignItems: "center"
          }}
          htmlFor={props.label}
        >
          {props.label}
          <input
            style={{
              height: "20px",
              width: "20px",
              marginLeft: "10px"
            }}
            type="radio"
            id={props.name}
            name={props.name}
            value="Yes"
            data-annot={annot_field_key}
            onChange={sendValueToBackend}
          />
        </label>
      );
      break;
    case "checkbox":
      input = (
        <label
          style={{
            display: "flex",
            alignItems: "center"
          }}
          htmlFor={props.name}
        >
          <span style={{ maxWidth: "80%" }}>{props.name || props.label}</span>
          <input
            style={{
              height: "20px",
              width: "20px",
              marginLeft: "10px"
            }}
            type="checkbox"
            id={props.name}
            value="Yes"
            onChange={sendValueToBackend}
            data-annot={annot_field_key}
          />
        </label>
      );
      break;

    default:
      input = (
        <label htmlFor={props.name}>
          {props.name || props.label}
          <input
            style={{
              display: "block",
              width: 200,
              margin: "20px 0",
              height: 22,
              padding: "0 10px"
            }}
            {...props}
            id={props.name}
            placeholder={`Write the ${props.placeholder || props.name}`}
            onBlur={sendValueToBackend}
            data-annot={annot_field_key}
          />
        </label>
      );
      break;
  }

  return <div style={{ marginTop: 20 }}>{input}</div>;
};

Input.defaultProps = {
  inputType: ""
};

Input.propTypes = {
  inputType: PropTypes.string
};

export default Input;
