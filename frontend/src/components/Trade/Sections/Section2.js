import React from "react";
import Input from "../../Input";

const Section2 = () => {
  return (
    <div>
      <h2>
        Section 2. Petitioner's Declaration, Signature, and Contact Information
        (Read the information on penalties in the instructions before completing
        this section.)
      </h2>
      <h4>
        <p>
          Copies of any documents submitted are exact photocopies of unaltered,
          original documents, and I understand that, as the petitioner, I may be
          required to submit original documents to U.S. Citizenship and
          Immigration Services (USCIS) at a later date.
        </p>
        <p>
          I authorize the release of any information from my records, or from
          the petitioning organization's records that USCIS needs to determine
          eligibility for the immigration benefit sought. I recognize the
          authority of USCIS to conduct audits of this petition using publicly
          available open source information. I also recognize that any
          supporting evidence submitted in support of this petition may be
          verified by USCIS through any means determined appropriate by USCIS,
          including but not limited to, on-site compliance reviews.
        </p>
        <p>
          I certify, under penalty of perjury, that I have reviewed this
          petition and that all of the information contained on the petition,
          including all responses to specific questions, and in the supporting
          documents, is complete, true, and correct.
        </p>
        <p>
          I am filing this petition on behalf of an organization and I certify
          that I am authorized to do so by the organization.
        </p>
      </h4>
      <h3>Name of Petitioner</h3>
      <fieldset>
        <Input
          label="Family Name (Last Name)"
          name="Family Name (Last Name)"
          annot_field_key="<feff00540041005300750070004c0069006e006500310061005f005000650074006900740069006f006e00650072004c006100730074004e0061006d0065005b0030005d>"
          doc="i-129"
        />
        <Input
          label="Given Name (First Name)"
          name="Given Name (First Name)"
          annot_field_key="<feff00540041005300750070004c0069006e006500310062005f005000650074006900740069006f006e0065007200460069007200730074004e0061006d0065005b0030005d>"
          doc="i-129"
        />
      </fieldset>
      <h3>Petitioner's Contact Information</h3>
      <fieldset>
        <Input
          label="Daytime Telephone Number"
          name="Daytime Telephone Number"
          annot_field_key="<feff00540041005300750070004c0069006e00650032005f00440061007900740069006d006500500068006f006e0065004e0075006d0062006500720031005b0030005d>"
          doc="i-129"
        />
        <Input
          label="Mobile Telephone Number"
          name="Mobile Telephone Number"
          annot_field_key="<feff00540041005300750070004c0069006e00650033005f004d006f00620069006c006500500068006f006e0065004e0075006d0062006500720031005b0030005d>"
          doc="i-129"
        />
        <Input
          label="Email Address (if any)"
          name="Email Address (if any)"
          annot_field_key="<feff00540041005300750070004c0069006e00650035005f0045006d00610069006c005b0030005d>"
          doc="i-129"
        />
      </fieldset>
    </div>
  );
};

export default Section2;
