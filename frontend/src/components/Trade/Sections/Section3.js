import React from "react";
import Input from "../../Input";
import StateSelect from "../../StateSelect";

const Section3 = () => {
  return (
    <div>
      <h2>
        Section 3. Declaration, Signature, and Contact Information of Person
        Preparing Form, If Other Than Petitioner
      </h2>
      <h4>Provide the following information concerning the preparer:</h4>
      <h3>Name of Petitioner</h3>
      <fieldset>
        <Input
          label="Family Name (Last Name)"
          name="Family Name (Last Name)"
          annot_field_key=  "<feff004c0069006e0065005f0050007200650070006100720065007200460061006d0069006c0079004e0061006d0065005b0031005d>"
          doc="i-129"
        />
        <Input
          label="Given Name (First Name)"
          name="Given Name (First Name)"
          annot_field_key="<feff004c0069006e0065005f005000720065007000610072006500720047006900760065006e004e0061006d0065005b0031005d>"
          doc="i-129"
        />
      </fieldset>
      <h3>Preparer's Business or Organization Name</h3>
      <fieldset>
        <Input
          label="(If applicable, provide the name of your accredited organization recognized by the Board of Immigration Appeals (BIA))."
          name="Acredited"
          annot_field_key="<feff004c0069006e0065005f0042007500730069006e006500730073004e0061006d0065005b0031005d>"
          doc="i-129"
        />
      </fieldset>
      <h3>Preparer's Mailing Address</h3>
      <fieldset>
        <Input
          label="Street Number and Name"
          name="Street Number and Name"
          annot_field_key="<feff004c0069006e006500370062005f005300740072006500650074004e0075006d006200650072004e0061006d0065005b0033005d>"
          doc="i-129"
        />
        <Input
          inputType="checkbox"
          label="Suite"
          name="Suite"
          annot_field_key="<feff0053006500630033004c0069006e00650033005f0055006e00690074005b0031005d>"
          doc="i-129"
        />
        <Input
          inputType="checkbox"
          label="Apartment"
          name="Apartment"
          annot_field_key="<feff0053006500630033004c0069006e00650033005f0055006e00690074005b0030005d>"
          doc="i-129"
        />
        <Input
          inputType="checkbox"
          label="Floor"
          name="Floor"
          annot_field_key="<feff0053006500630033004c0069006e00650033005f0055006e00690074005b0032005d>"
          doc="i-129"
        />
        <Input 
          label="Number" 
          name="Number" 
          annot_field_key="<feff0053006500630033004c0069006e00650033005f0041007000740053007400650046006c0072004e0075006d006200650072005b0030005d>"
          doc="i-129" 
        />
        <Input
          label="City or Town"
          name="City or Town"
          annot_field_key="<feff004c0069006e0065005f00430069007400790054006f0077006e005b0033005d>"
          doc="i-129"
        />
        <StateSelect
          annot_field_key="<feff00530033005f004c0069006e00650033005f00530074006100740065005b0030005d>"
          doc="i-129"
          id="City or Town"
        />
        <Input
          label="ZIP Code"
          name="ZIP Code"
          annot_field_key="<feff00530033005f004c0069006e00650033005f005a006900700043006f00640065005b0030005d>"
          doc="i-129"
        />
        <Input
          label="Province"
          name="Province"
          annot_field_key="<feff00530033005f004c0069006e00650033005f00500072006f00760069006e00630065005b0030005d>"
          doc="i-129"
        />
        <Input
          label="Postal Code"
          name="Postal Code"
          annot_field_key="<feff00530033005f004c0069006e00650033005f0050006f007300740061006c0043006f00640065005b0030005d>"
          doc="i-129"
        />
        <Input
          label="Country"
          name="Country"
          annot_field_key="<feff00530033005f004c0069006e00650033005f0043006f0075006e007400720079005b0030005d>"
          doc="i-129"
        />
      </fieldset>
      <h3>Preparer's Contact Information</h3>
      <fieldset>
        <Input
          label="Daytime Telephone Number"
          name="Daytime Telephone Number"
          annot_field_key="<feff00540041005300750070004c0069006e00650032005f00440061007900740069006d006500500068006f006e0065004e0075006d0062006500720031005b0031005d>"
          doc="i-129"
        />
        <Input
          label="Fax Number"
          name="Fax Number"
          annot_field_key="<feff005000740038004c0069006e00650034005f00440061007900740069006d006500500068006f006e0065004e0075006d0062006500720031005b0032005d>"
          doc="i-129"
        />
        <Input
          label="Email Address (if any)"
          name="Email Address (if any)"
          annot_field_key="<feff00540041005300750070004c0069006e00650035005f0045006d00610069006c005b0031005d>"
          doc="i-129"
        />
      </fieldset>
    </div>
  );
};

export default Section3;
