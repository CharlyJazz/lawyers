import React from "react";
import Input from "../../Input";

const Section1 = () => {
  return (
    <div>
      <h2>
        Section 1. Information About Requested Extension or Change (See
        instructions attached to this form.)
      </h2>
      <h3>This is a request for Free Trade status based on:</h3>
      <fieldset>
        <Input
          inputType="radio"
          label="Free Trade, Canada (TN1)"
          name="Request"
          annot_field_key="<feff0061005f00430061006e006100640061005b0030005d>"
          doc="i-129"
        />
        <Input
          inputType="radio"
          label="Free Trade, Mexico (TN2)"
          name="Request"
          annot_field_key="<feff0062005f004d0065007800690063006f005b0030005d>"
          doc="i-129"
        />
        <Input
          inputType="radio"
          label="Free Trade, Chile (H-1B1)"
          name="Request"
          annot_field_key="<feff0063005f004300680069006c0065005b0030005d>"
          doc="i-129"
        />
        <Input
          inputType="radio"
          label="Free Trade, Singapore (H-1B1)"
          name="Request"
          annot_field_key="<feff0064005f00530069006e006700610070006f00720065005b0030005d>"
          doc="i-129"
        />
        <Input
          inputType="radio"
          label="Free Trade, Other"
          name="Request"
          annot_field_key="<feff0065005f004f0074006800650072005b0030005d>"
          doc="i-129"
        />
        <Input
          inputType="radio"
          label="A sixth consecutive request for Free Trade, Chile or Singapore (H-1B1)"
          name="Request"
          annot_field_key="<feff0066005f004300680069006c0065004f007200530069006e006700610070006f00720065005b0030005d>"
          doc="i-129"
        />
      </fieldset>
    </div>
  );
};

export default Section1;
