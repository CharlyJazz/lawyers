import React from "react";

import Input from "../../Input";

const Section0 = () => {
  return (
    <div>
      <h3>Name of the Petitioner</h3>
      <fieldset>
        <Input
          label="Petitioner"
          name="Petitioner"
          annot_field_key="<feff00540041005300750070004c0069006e00650031005f004e0061006d0065006f0066005000650074006900740069006f006e00650072005b0030005d>"
          doc="i-129"
        />
      </fieldset>
      <h3>Name of the Beneficiary</h3>
      <fieldset>
        <Input
          label="Beneficiary"
          name="Beneficiary"
          annot_field_key="<feff00540041005300750070004c0069006e00650031005f004e0061006d0065006f006600420065006e00650066006900630069006100720079005b0030005d>"
          doc="i-129"
        />
      </fieldset>
      <h3>Employer is a:</h3>
      <fieldset>
        <Input
          inputType="radio"
          label="U.S. Employer"
          name="Employer"
          annot_field_key="<feff0065006d0070006c006f007900650072005b0031005d>"
          doc="i-129"
        />
        <Input
          inputType="radio"
          label="Foreign Employer"
          name="Employer"
          annot_field_key="<feff0065006d0070006c006f007900650072005b0030005d>"
          doc="i-129"
        />
      </fieldset>
      <h3>If Foreign Employer, Name the Foreign Country</h3>
      <fieldset>
        <Input 
          label="Country" 
          name="Country" 
          annot_field_key="<feff004c0069006e00650034005f0043006f0075006e007400720079005b0030005d>" 
          doc="i-129" 
        />
      </fieldset>
    </div>
  );
};

export default Section0;
