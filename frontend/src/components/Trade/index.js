import React from "react";
import "./Trade.css";
import Section0 from "./Sections/Section0";
import Section1 from "./Sections/Section1";
import Section2 from "./Sections/Section2";
import { useDoc } from "../../hooks";
import Section3 from "./Sections/Section3";
import { Link, Router } from "@reach/router";

const Trade = () => {
  const doc = useDoc("i-129");

  return (
    <div>
      <h1>Trade Agreement Supplement to Form I-129</h1>
      <div className="Layout-Form">
        <aside className="Layout-Form-Aside">
          <ul>
            <li>
              <Link to="/trade-agreement">Trade Agreement</Link>
            </li>
            {[1, 2, 3].map(p => (
              <li key={p}>
                <Link to={`section-${p}`}>Section {p}</Link>
              </li>
            ))}
          </ul>
        </aside>
        <section className="Layout-Section">
          <Router>
            <Section0 path="/" doc={doc} />
            <Section1 path="section-1" doc={doc} />
            <Section2 path="section-2" doc={doc} />
            <Section3 path="section-3" doc={doc} />
          </Router>
        </section>
      </div>
    </div>
  );
};

export default Trade;
