import React from "react";
import "./Q1Classification.css";
import Section0 from "./Sections/Section0";
import Section1 from "./Sections/Section1";
import { Link, Router } from "@reach/router";

const Q1Classifcation = () => {

  return (
    <div>
      <h1>Q-1 Classification Supplement to Form I-129</h1>
      <div className="Layout-Form">
        <aside className="Layout-Form-Aside">
          <ul>
            <li>
              <Link to="/q-1-classification">Q-1 Classification</Link>
            </li>
            {[1, 2].map(p => (
              <li key={p}>
                <Link to={`section-${p}`}>Section {p}</Link>
              </li>
            ))}
          </ul>
        </aside>
        <section className="Layout-Section">
          <Router>
            <Section0 path="/" />
            <Section1 path="section-1" />
          </Router>
        </section>
      </div>
    </div>
  );
};

export default Q1Classifcation;
