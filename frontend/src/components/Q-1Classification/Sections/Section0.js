import React from "react";
import WithValues from '../../../WithValues'
import Input from "../../Input";

const Section0 = () => {

  return (
    <WithValues doc={doc}>
      <div>
        <h3>1. Name of the Petitioner</h3>
        <fieldset>
          <Input
            label="1. Name of the Petitioner"
            name="1. Name of the Petitioner"
            annot_field_key="<feff004c0069006e00650031005f00460061006d0069006c0079004e0061006d0065005b0039005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>2. Name of the Beneficiary</h3>
        <fieldset>
          <Input
            label="2. Name of the Beneficiary"
            name="2. Name of the Beneficiary"
            annot_field_key="<feff0048005300750070004c0069006e00650032005f00460061006d0069006c0079004e0061006d0065005b0032005d>"
            doc="i-129"
          />
        </fieldset>
      </div>
    </WithValues>
  );
};

export default Section0;
