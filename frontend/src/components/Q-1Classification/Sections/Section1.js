import React from "react";
import Input from "../../Input";
import WithValues from '../../../WithValues'

const Section1 = () => {

  return (
    <WithValues doc={doc}>
      <div>
        <h2>
          Section 1. Complete if you are filing for a Q-1 International Cultural
          Exchange Alien
      </h2>
        <h3>
          I hereby certify that the participant(s) in the international cultural
          exchange program: a. Is at least 18 years of age, b. Is qualified to
          perform the service or labor or receive the type of training stated in
          the petition, c. Has the ability to communicate effectively about the
          cultural attributes of his or her country of nationality to the American
          public, and d. Has resided and been physically present outside the
          United States for the immediate prior year. (Applies only if the
          participant was previously admitted as a Q-1). I also certify that I
          will offer the alien(s) the same wages and working conditions comparable
          to those accorded local domestic workers similarly employed.
      </h3>
        <h3>1. Name of Petitioner</h3>
        <fieldset>
          <Input
            name="1. Family Name (Last Name)"
            label="1. Family Name (Last Name)"
            doc="i-129"
            annot_field_key="<feff004c0069006e00650031005f00460061006d0069006c0079004e0061006d0065005b00310030005d>"
          />
          <Input
            name="1. Given Name (First Name)"
            label="1. Given Name (First Name)"
            doc="i-129"
            annot_field_key="<feff004c0069006e00650031005f0047006900760065006e004e0061006d0065005b0035005d>"
          />
          <Input
            name="1. Middle Name"
            label="1. Middle Name"
            doc="i-129"
            annot_field_key="<feff004c0069006e00650031005f004d006900640064006c0065004e0061006d0065005b0035005d>"
          />
        </fieldset>
        <h3>3. Petitioner's Contact Information</h3>
        <fieldset>
          <Input
            name="3 Daytime Telephone Number"
            label="3 Daytime Telephone Number"
            doc="i-129"
            type="Number"
            annot_field_key="<feff005000740037004c0069006e00650033005f00440061007900740069006d006500500068006f006e0065004e0075006d0062006500720031005b0032005d>"
          />
          <Input
            name="3 Email Address (if any)"
            label="3 Email Address (if any)"
            doc="i-129"
            type="email"
            annot_field_key="<feff005000740037004c0069006e00650033005f0045006d00610069006c0041006400640072006500730073005b0032005d>"
          />
        </fieldset>
      </div>
    </WithValues>
  );
};

export default Section1;
