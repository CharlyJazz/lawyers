import React from "react";
import Input from "../../Input";
import WithValues from '../../../WithValues'
import StateSelect from "../../StateSelect";

const Section0 = () => {

  return (
    <WithValues doc={doc}>
    <div>
      <h3>
        Attach to Form I-129 when more than one person is included in the
        petition. (List each person separately. Do not include the person you
        named on the Form I-129.)
      </h3>
      <fieldset>
        <Input
          label="Family Name (Last Name)"
          name="Family Name (Last Name)"
          annot_field_key="<feff004c0069006e00650033005f00460061006d0069006c0079004e0061006d00650031005b0032005d>"
          doc="i-129"
        />
        <Input
          label="Given Name (First Name)"
          name="Given Name (First Name)"
          annot_field_key="<feff004c0069006e00650033005f0047006900760065006e004e0061006d00650031005b0032005d>"
          doc="i-129"
        />
        <Input
          label="Middle Name"
          name="Middle Name"
          annot_field_key="<feff004c0069006e00650033005f004d006900640064006c0065004e0061006d00650031005b0032005d>"
          doc="i-129"
        />
        <Input
          label="Date of birth (mm/dd/yyyy)"
          name="Date of birth (mm/dd/yyyy)"
          annot_field_key="<feff004c0069006e00650036005f0044006100740065004f006600420069007200740068005b0032005d>"
          doc="i-129"
          type="date"
        />
        <Input
          label="Gender MALE"
          name="Gender MALE"
          annot_field_key="<feff004c0069006e00650031005f00470065006e006400650072005b0030005d>"
          doc="i-129"
          type="checkbox"
        />
        <Input
          label="Gender FEMALE"
          name="Gender FEMALE"
          annot_field_key="<feff004c0069006e00650031005f00470065006e006400650072005b0031005d>"
          doc="i-129"
          type="checkbox"
        />
        <Input
          label="U.S. Social Security Number (if any)"
          name="U.S. Social Security Number (if any)"
          annot_field_key="<feff004c0069006e00650035005f00530053004e005b0031005d>"
          doc="i-129"
        />
        <Input
          label="A-Number (if any)"
          name="A-Number (if any)"
          annot_field_key="<feff004c0069006e00650031005f0041006c00690065006e004e0075006d006200650072005b0031005d>"
          doc="i-129"
        />
      </fieldset>
      <h3>
        All Other Names Used (include aliases, maiden name and names from
        previous Marriages)
      </h3>
      <fieldset>
        <Input
          label="Family Name (Last Name)"
          name="Family Name (Last Name)"
          annot_field_key="<feff004c0069006e00650033005f00460061006d0069006c0079004e0061006d00650031005b0033005d>"
          doc="i-129"
        />
        <Input
          label="Given Name (First Name)"
          name="Given Name (First Name)"
          annot_field_key="<feff004c0069006e00650033005f0047006900760065006e004e0061006d00650031005b0033005d>"
          doc="i-129"
        />
        <Input
          label="Middle Name"
          name="Middle Name"
          annot_field_key="<feff004c0069006e00650033005f004d006900640064006c0065004e0061006d00650031005b0033005d>"
          doc="i-129"
        />
      </fieldset>
      <h3>
        Address in the United States Where You Intend to Live (Complete Address)
      </h3>
      <fieldset>
        <Input
          name="Street Number and Name"
          label="Street Number and Name"
          doc="i-129"
          annot_field_key="<feff00500061007200740037004c0069006e00650041005f0045006d00700031005300740072006500650074004e0061006d0065005b0030005d>"
        />
        <Input
          name="Apt."
          label="Apt."
          doc="i-129"
          annot_field_key="<feff0041007400740031005f0055006e00690074005b0032005d>"
          type="checkbox"
        />
        <Input
          name="Ste."
          label="Ste."
          doc="i-129"
          type="checkbox"
          annot_field_key="<feff0041007400740031005f0055006e00690074005b0031005d>"
        />
        <Input
          name="Flr."
          label="Flr."
          doc="i-129"
          type="checkbox"
          annot_field_key="<feff0041007400740031005f0055006e00690074005b0030005d>"
        />
        <Input
          name="Number"
          label="Number"
          doc="i-129"
          annot_field_key="<feff0041007400740031005f0041007000740053007400650046006c0072004e0075006d006200650072005b0030005d>"
          type="Number"
        />
        <Input
          name="City or Town"
          label="City or Town"
          doc="i-129"
          annot_field_key="<feff00500061007200740037004c0069006e00650041005f0045006d00700043006900740079005b0030005d>"
        />
        <Input
          name="State"
          label="State"
          doc="i-129"
          annot_field_key="<feff00500061007200740037004c0069006e00650041005f0045006d0070003100530074006100740065005b0030005d>"
        />
        <Input
          name="ZIP Code"
          label="ZIP Code"
          doc="i-129"
          annot_field_key="<feff00500061007200740037004c0069006e00650041005f0045006d00700031005a006900700043006f00640065005b0030005d>"
        />
      </fieldset>
      <h3>Foreign Address (Complete Address)</h3>
      <fieldset>
        <Input
          name="Street Number and Name"
          label="Street Number and Name"
          doc="i-129"
          annot_field_key="<feff004c0069006e006500370062005f005300740072006500650074004e0075006d006200650072004e0061006d0065005b0034005d>"
        />
        <Input
          name="Apt."
          label="Apt."
          doc="i-129"
          annot_field_key="<feff0046006f0072005f0055006e00690074005b0031005d>"
          type="checkbox"
        />
        <Input
          name="Ste."
          label="Ste."
          doc="i-129"
          type="checkbox"
          annot_field_key="<feff0046006f0072005f0055006e00690074005b0030005d>"
        />
        <Input
          name="Flr."
          label="Flr."
          doc="i-129"
          type="checkbox"
          annot_field_key="<feff0046006f0072005f0055006e00690074005b0032005d>"
        />
        <Input
          name="Number"
          label="Number"
          doc="i-129"
          annot_field_key="<feff0046006f0072005f0041007000740053007400650046006c0072004e0075006d006200650072005b0030005d>"
          type="Number"
        />
        <Input
          name="City or Town"
          label="City or Town"
          doc="i-129"
          annot_field_key="<feff004c0069006e0065005f00430069007400790054006f0077006e005b0034005d>"
        />
        <StateSelect
          name="State"
          label="State"
          doc="i-129"
          annot_field_key="<feff00410031005f00530074006100740065005b0030005d>"
        />
        <Input
          name="ZIP Code"
          label="ZIP Code"
          doc="i-129"
          annot_field_key="<feff00410031005f005a006900700043006f00640065005b0030005d>"
        />
        <Input
          name="Province"
          label="Province"
          doc="i-129"
          annot_field_key="<feff00410031005f00500072006f00760069006e00630065005b0030005d>"
        />
        <Input
          name="Postal Code"
          label="Postal Code"
          doc="i-129"
          annot_field_key="<feff00410031005f0050006f007300740061006c0043006f00640065005b0030005d>"
        />
        <Input
          name="Country"
          label="Country"
          doc="i-129"
          annot_field_key="<feff00410031005f0043006f0075006e007400720079005b0030005d>"
        />
        <Input
          name="Country of Birth"
          label="Country of Birth"
          doc="i-129"
          annot_field_key="<feff004c0069006e0065005f0043006f0075006e007400720079005b0032005d>"
        />
        <Input
          name="Country of Citizenship or Nationality"
          label="Country of Citizenship or Nationality"
          doc="i-129"
          annot_field_key="<feff004c0069006e0065005f0043006f0075006e007400720079005b0033005d>"
        />
      </fieldset>
      <h3>IF IN THE UNITED STATES:</h3>
      <fieldset>
        <Input
          label="Date of Last Arrival
          (mm/dd/yyyy)"
          name="Date of Last Arrival
          (mm/dd/yyyy)"
          doc="i-129"
          annot_field_key="<feff004c0069006e0065003100310061005f0044006100740065006f0066004100720072006900760061006c005b0031005d>"
          type="date"
        />
        <Input
          label="I-94 Arrival-Departure Record
          Number"
          name="I-94 Arrival-Departure Record
          Number"
          doc="i-129"
          annot_field_key="<feff004c0069006e0065003100340061005f004100720072006900760061006c004400650070006100720074007500720065005b0030005d>"
        />
        <Input
          label="Passport or Travel Document
          Number"
          name="Passport or Travel Document
          Number"
          doc="i-129"
          annot_field_key="<feff004c0069006e0065003100340062005f00500061007300730070006f00720074005b0032005d>"
        />
        <Input
          label="Date Passport or Travel Document
          Issued (mm/dd/yyyy)"
          name="Date Passport or Travel Document
          Issued (mm/dd/yyyy)"
          doc="i-129"
          annot_field_key="<feff004c0069006e0065003100340065005f0045007800700044006100740065005b0031005d>"
          type="date"
        />
        <Input
          label="Date Passport or Travel Document
          Expires (mm/dd/yyyy)"
          name="Date Passport or Travel Document
          Expires (mm/dd/yyyy)"
          doc="i-129"
          annot_field_key="<feff004c0069006e0065003100340065005f0045007800700044006100740065005b0030005d>"
          type="date"
        />
        <Input
          label="Country of Issuance for Passport
          or Travel Document"
          name="Country of Issuance for Passport
          or Travel Document"
          doc="i-129"
          annot_field_key="<feff004c0069006e0065005f0043006f0075006e007400720079005b0034005d>"
        />
        <Input
          label="Current Nonimmigrant Status"
          name="Current Nonimmigrant Status"
          doc="i-129"
          annot_field_key="<feff004c0069006e006500310035005f00430075007200720065006e0074004e006f006e005b0030005d>"
        />
        <Input
          label="Date Status Expires or D/S
          (mm/dd/yyyy)"
          name="Date Status Expires or D/S
          (mm/dd/yyyy)"
          doc="i-129"
          annot_field_key="<feff004c0069006e006500310036005f00440061007400650053007400610074007500730045007800700069007200650073005b0030005d>"
          type="date"
        />
        <Input
          label="Student and Exchange Visitor Information System (SEVIS) Number
          (if any)"
          name="Student and Exchange Visitor Information System (SEVIS) Number
          (if any)"
          doc="i-129"
          annot_field_key="<feff004c0069006e0065003100340062005f00500061007300730070006f00720074005b0030005d>"
        />
        <Input
          label="Employment Authorization Document (EAD) Number
          (if any)"
          name="Employment Authorization Document (EAD) Number
          (if any)"
          doc="i-129"
          annot_field_key="<feff004c0069006e0065003100340062005f00500061007300730070006f00720074005b0031005d>"
        />
      </fieldset>
    </div>
    </WithValues>
  );
};

export default Section0;
