import React from "react";
import "./attachment1.css";
import Section0 from "./Sections/Section0";
import { Link, Router } from "@reach/router";

const Attachment1 = () => {

  return (
    <div>
      <h1>Attachment-1 Supplement I-129</h1>
      <div className="Layout-Form">
        <aside className="Layout-Form-Aside">
          <ul>
            <li>
              <Link to="/i-129">Supplements</Link>
            </li>
            {/* {[1, 2, 3, 4].map(p => (
              <li key={p}>
                <Link to={`section-${p}`}>Section {p}</Link>
              </li>
            ))} */}
          </ul>
        </aside>
        <section className="Layout-Section">
          <Router>
            <Section0 path="/" />
          </Router>
        </section>
      </div>
    </div>
  );
};

export default Attachment1;
