import React from "react";
import Input from "../../Input";
import WithValues from '../../../WithValues'

const Part2 = () => {
  
  return (
    <WithValues doc={doc}>
    <div>
      <h2>Part 2. Eligibility Information for Attorney  or Accredited Representative</h2>
      <fieldset>
        <h3>Selected applicable items.</h3>

        <Input
          name="1.a. I am attonery eligible to practice law in,
          and a member in a good standing of,
           the bar of the highest courst of the following states,
            possessions, territories, commonwealths,
            or the District of Columbia. If you need a extra space to complete this section,
             use the space provided in Part 6. Additional Information."
          inputType="checkbox"
          doc="g-28-2"
          annot_field_key="<feff0043006800650063006b0042006f00780031005b0030005d>"
        />
        <Input
          name=" 1.a. licesing authority"
          placeholder="licesing authority"
          doc="g-28-2"
          annot_field_key="<feff005000740032004c0069006e006500310061005f004c006900630065006e00730069006e00670041007500740068006f0072006900740079005b0030005d>"
        />
        <Input
          name="1.b. Bar Number (if applicable)"
          placeholder="Bar Number"
          doc="g-28-2"
          annot_field_key="<feff005000740032004c0069006e006500310062005f004200610072004e0075006d006200650072005b0030005d>"
        />
        <Input
          inputType="radio"
          name="1.c. I (selected only one box) am not"
          doc="g-28-2"
          annot_field_key="<feff0043006800650063006b0062006f0078003100640041006d004e006f0074005b0030005d>"
        />
        <Input
          name="1.c. am  subject to any order suspending, enjoining, restraining,
          disbarring, or otherwise restricting me in the practice of law.
          If you are subject to any orders, use the space
          provided in Part 6. Additional Information to provide
          an explanation."
          inputType="radio"
          doc="g-28-2"
          annot_field_key="<feff0043006800650063006b0062006f0078003100640041006d005b0030005d>"
        />
        <Input
          name="1.d Name of the La Firm or Organization (if applicable)"
          placeholder="Name of the laW Firm or Organization"
          doc="g-28-2"
          annot_field_key="<feff005000740032004c0069006e006500310064005f004e0061006d0065006f0066004600690072006d004f0072004f007200670061006e0069007a006100740069006f006e005b0030005d>"
        />
        <Input
          name="2.a.   I am an accredited representative of the following qualified nonprofit religious, charitable, social,
          service, or similar organization  established in the United states
          an recognized by the Department of Justice in accordance with 8 CRF part 1292."
          inputType="checkbox"
          doc="g-28-2"
          annot_field_key="<feff0043006800650063006b0042006f00780032005b0030005d>"
        />
        <Input
          name="2.b. Name of Recognized Organization"
          placeholder="Name of Recognized Organization"
          doc="g-28-2"
          annot_field_key="<feff004c0069006e006500320062005f004e0061006d0065006f0066004f007200670061006e0069007a006100740069006f006e005b0030005d>"
        />
        <Input
          name="2.c Date of Accredittion (mm/dd/yyyy)"
          placeholder="mm/dd/yyyy"
          type="date"
          doc="g-28-2"
          annot_field_key="<feff004c0069006e006500320063005f00440061007400650045007800700069007200650073005b0030005d>"
        />
        <Input
          name="3. I am associted with"
          inputType="checkbox"
          doc="g-28-2"
          annot_field_key="<feff0043006800650063006b0042006f00780033005b0030005d>"
        />
        <Input
          name="3. the attorney or accredited representative of record who previously
          filed Form G-28 in this case, and my appearance as an attorney or accredited representative
          for a limited purpose is at his or her request."
          placeholder="Name of association"
          doc="g-28-2"
          annot_field_key="<feff004c0069006e00650033005f004e0061006d0065006f0066004100740074006f0072006e00650079004f0072005200650070005b0030005d>"
        />
        <Input
          name="4.a. I am a law student or law graduate working under the
          direct supervision of the attorney or accredited
          representative of record on this form in accordance
          with the requirements in 8 CFR 292.1(a)(2)."
          inputType="checkbox"
          doc="g-28-2"
          annot_field_key="<feff0043006800650063006b0042006f00780034005b0030005d>"
        />
        <Input
          name="4.b Name of Law Student or Law graduate"
          placeholder="Name"
          doc="g-28-2"
          annot_field_key="<feff004c0069006e006500340062005f004c0061007700530074007500640065006e0074005b0030005d>"
        />
      </fieldset>
    </div>
    </WithValues>
  );
};

export default Part2;
