import React from "react";
import Input from "../../Input";
import WithValues from '../../../WithValues'
import StateSelect from '../../StateSelect';

const Part3 = () => {

  return (
    <WithValues doc={doc}>
    <div>
        <h2>
            Part 3. Notice of Appearance as Attorney or
            Accredited Representative
        </h2>
        <span>
            If you need extra space to complete this section, use the space
            provided in Part 6. Additional Information.
            This appearance relates to immigration matters before
            (select only one box):
        </span>
      <fieldset>
        <Input
          name="1.a. U.S. Citizenship and Immigration Services (USCIS)"
          inputType="checkbox"
          doc="g-28-2"
          annot_field_key="<feff004c0069006e006500310061005f00550053004300490053005b0030005d>"
        />
        <Input
          name="1.b. List the form numbers or specific matter in which appearance is entered."
          placeholder="List the form numbers"
          type="number"
          doc="g-28-2"
          annot_field_key="<feff004c0069006e006500320062005f004c006900730074004d00610074007400650072005b0030005d>"
        />
               <Input
          name="2.a. U.S. Immigration and Customs Enforcement (ICE)"
          inputType="checkbox"
          doc="g-28-2"
          annot_field_key="<feff004c0069006e006500320061005f004900430045005b0030005d>"
        />
        <Input
          name="2.b. List the specific matter in which appearance is entered"
          placeholder="List the form numbers"
          type="number"
          doc="g-28-2"
          annot_field_key="<feff004c0069006e006500320062005f004c006900730074004d00610074007400650072005b0030005d>"
        />
               <Input
          name="3.a. U.S. Customs and Border Protection (CBP)"
          inputType="checkbox"
          doc="g-28-2"
          annot_field_key="<feff004c0069006e006500330061005f004300420050005b0030005d>"
        />
        <Input
          name="3.b. List the form numbers or specific matter in which appearance is entered."
          placeholder="List the form numbers"
          annot_field_key="<feff004c0069006e006500330062005f004c00690073007400530070006500630069006600690063004d00610074007400650072005b0030005d>"
          type="number"
          doc="g-28-2"
        />
        <Input
          name="4. Receipt Number (if any)"
          type="number"
          placeholder="Receipt Number"
          doc="g-28-2"
          annot_field_key="<feff005000740033004c0069006e00650034005f0052006500630065006900700074004e0075006d006200650072005b0030005d>"
        />
        <Input
            name=" 5. I enter my appearance as an attorney or accredited
            representative at the request of the (select only one box) Applicant"
            inputType="radio"
            doc="g-28-2"
            annot_field_key="<feff004c0069006e00650034005f0043006800650063006b0062006f0078005b0031005d>"
            />
        <Input
            name="5. Petitioner"
            inputType="radio"
            doc="g-28-2"
            annot_field_key="<feff004c0069006e00650034005f0043006800650063006b0062006f0078005b0031005d>"
            />
        <Input
            name="5. Requestor"
            inputType="radio"
            doc="g-28-2"
            annot_field_key="<feff004c0069006e00650034005f0043006800650063006b0062006f0078005b0030005d>"
        />
        <Input
            name="5. Beneficiary/Derivate"
            inputType="radio"
            doc="g-28-2"
            annot_field_key="<feff004c0069006e00650034005f0043006800650063006b0062006f0078005b0034005d>"
        />
        <Input
            inputType="radio"
            name="5. Respondent (ICE, CBP)"
            doc="g-28-2"
            annot_field_key="<feff004c0069006e00650034005f0043006800650063006b0062006f0078005b0032005d>"
        />
        </fieldset>
        <h3>
            Information About Client (Applicant, Petitioner,
            Requestor, Beneficiary or Derivative, Respondent,
            or Authorized Signatory for an Entity)
        </h3>
        <fieldset>        
        <Input
          name="6.a. Family Name(Last Name)"
          placeholder="Last Name"
          doc="g-28-2"
          annot_field_key="<feff005000740033004c0069006e006500350061005f00460061006d0069006c0079004e0061006d0065005b0030005d>"
        />
         <Input
          name="6.b. Given Name (First Name)"
          placeholder="Given Name"
          doc="g-28-2"
          annot_field_key="<feff005000740033004c0069006e006500350062005f0047006900760065006e004e0061006d0065005b0030005d>"
        />
        <Input
          name="6.c. Middle Name"
          placeholder="Middle Name"
          doc="g-28-2"
          annot_field_key="<feff005000740033004c0069006e006500350063005f004d006900640064006c0065004e0061006d0065005b0030005d>"
        />
        <Input
            name="7.a. Name of Entity (if applicable)"
            placeholder="Name of Entity"
            doc="g-28-2"
            annot_field_key="<feff005000740033004c0069006e006500370061005f004e0061006d0065004f00660045006e0074006900740079005b0030005d>"
        />
        <Input
            name="7.b. Title of Authorized Signatory for Entity (if applicable)"
            placeholder="Title"
            doc="g-28-2"
            annot_field_key="<feff005000740033004c0069006e006500370062005f005400690074006c0065006f00660045006e0074006900740079005b0030005d>"
        />
        <Input
            name="8. Client's USCIS Online Account Number (if any)"
            placeholder="USCIS Online Account Number"
            doc="g-28-2"
            annot_field_key="<feff005000740033004c0069006e00650038005f00550053004300490053004f006e006c0069006e00650041006300630074004e0075006d006200650072005b0030005d>"
        />
        <Input
            name="9. Client's Alien Registration Number (A-Number) (if any)"
            placeholder="Client's Alien Registration Number"
            doc="g-28-2"
            annot_field_key="<feff005000740033004c0069006e00650039005f0041004e0075006d006200650072005b0030005d>"
        />
        </fieldset>
        <h3>
            Client's Contact Information
        </h3>
        <fieldset>
        <Input
            name="10. Daytime Telephone Number"
            placeholder="Telephone Number"
            type="tel"
            doc="g-28-2"    
            annot_field_key="<feff004c0069006e00650039005f00440061007900740069006d006500540065006c006500700068006f006e0065004e0075006d006200650072005b0030005d>"        
        />
        <Input
            name="11. Mobile Telephone Number (if any)"
            type="tel"
            placeholder="Mobile Telephone Number"
            doc="g-28-2"   
            annot_field_key="<feff004c0069006e006500310030005f004d006f00620069006c006500540065006c006500700068006f006e0065004e0075006d006200650072005b0030005d>"         
        />
        <Input
            name="12. Email Address (if any)"
            type="email"
            placeholder="Email Address"
            doc="g-28-2"
            annot_field_key="<feff004c0069006e006500310031005f0045004d00610069006c005b0030005d>"            
        />
        </fieldset>
        <h3>
            Mailing Address of Client
        </h3>
        <fieldset>
        <span>
            NOTE: Provide the client's mailing address. Do not provide
            the business mailing address of the attorney or accredited
            representative unless it serves as the safe mailing address on the
            application or petition being filed with this Form G-28.
        </span>
        <Input
          name="13.a. Street Number and Name"
          placeholder="Street Number and Name"
          doc="g-28-2"
          annot_field_key="<feff004c0069006e0065003100320061005f005300740072006500650074004e0075006d006200650072004e0061006d0065005b0030005d>"
        />
        <Input
          name="Apt.  13.b."
          inputType="radio"
          doc="g-28-2"
          annot_field_key="<feff004c0069006e0065003100320062005f0055006e00690074005b0032005d>"
        />
        <Input
          name="Ste."
          inputType="radio"
          doc="g-28-2"
          annot_field_key="<feff004c0069006e0065003100320062005f0055006e00690074005b0030005d>"
        />
        <Input
          name="Flr."
          inputType="radio"
          doc="g-28-2"
          annot_field_key="<feff004c0069006e0065003100320062005f0055006e00690074005b0031005d>"
        />
        <Input
          name="13.b. Enter Apartment, Suite or Floor Number."
          placeholder="Enter Apartment, Suite or Floor Number."
          doc="g-28-2"
          annot_field_key="<feff004c0069006e0065003100320062005f0041007000740053007400650046006c0072004e0075006d006200650072005b0030005d>"
        />
        <Input
            name="13.c. City or Town"
            placeholder="City or Town"
            doc="g-28-2"
            annot_field_key="<feff004c0069006e0065003100320063005f0043006900740079004f00720054006f0077006e005b0030005d>"
        />
        <StateSelect
            name="13.d State"
            placeholder="state"
            doc="g-28-2"
            annot_field_key="<feff004c0069006e0065003100320064005f00530074006100740065005b0030005d>"
        />
        <Input
            name="13.e ZIP Code"
            placeholder="ZIP Code"
            doc="g-28-2"
            annot_field_key="<feff004c0069006e0065003100320065005f005a006900700043006f00640065005b0030005d>"
        />
        <Input
            name="13.f. Province"
            placeholder="Province"
            doc="g-28-2"
            annot_field_key="<feff004c0069006e0065003100320066005f00500072006f00760069006e00630065005b0030005d>"
        />
        <Input
            name="13.g. Postal Code"
            placeholder="Postal Code"
            doc="g-28-2"
            annot_field_key="<feff004c0069006e0065003100320067005f0050006f007300740061006c0043006f00640065005b0030005d>"
        />
        <StateSelect
            name="13.h. Country"
            placeholder="Country"
            doc="g-28-2"
            annot_field_key="<feff004c0069006e0065003100320068005f0043006f0075006e007400720079005b0030005d>"
        />
      </fieldset>
    </div>
    </WithValues>
  );
};

export default Part3;
