import React from "react";
import Input from "../../Input";
import WithValues from '../../../WithValues'

const Part6 = () => {

    return (
        <WithValues doc={doc}>
        <div>
            <h3>
                Part 6. Additional Information
        </h3>
            <span>
                If you need extra space to provide any additional information
                within this form, use the space below. If you need more space
                than what is provided, you may make copies of this page to
                complete and file with this form or attach a separate sheet of
                paper. Type or print your name at the top of each sheet;
                indicate the Page Number, Part Number, and Item Number
                to which your answer refers; and sign and date each sheet.
        </span>
            <fieldset>
                <Input
                    name="1.a
                    Family Name
                    (Last Name)"
                    placeholder="Family Name"
                    doc="g-28-2"
                    annot_field_key="<feff005000740033004c0069006e006500350061005f00460061006d0069006c0079004e0061006d0065005b0031005d>"
                />
                <Input
                    name="1.b. Given Name
                    (First Name)"
                    placeholder="Given Name"
                    doc="g-28-2"
                    annot_field_key="<feff005000740033004c0069006e006500350062005f0047006900760065006e004e0061006d0065005b0031005d>"
                />
                <Input
                    name="1.c. Middle Name"
                    placeholder="Middle Name"
                    doc="g-28-2"
                    annot_field_key="<feff005000740033004c0069006e006500350063005f004d006900640064006c0065004e0061006d0065005b0031005d>"
                />
                <Input
                    name="2.a. Page Number"
                    placeholder="Page Number"
                    type="number"
                    doc="g-28-2"
                    annot_field_key="<feff005000740039004c0069006e006500330061005f0050006100670065004e0075006d006200650072005b0030005d>"
                />
                <Input
                    name="2.b. Part Number"
                    placeholder="Part Number"
                    type="number"
                    doc="g-28-2"
                    annot_field_key="<feff005000740039004c0069006e006500330062005f0050006100720074004e0075006d006200650072005b0030005d>"
                />
                <Input
                    name="2.c. Item Number"
                    placeholder="Item Number"
                    type="number"
                    doc="g-28-2"
                    annot_field_key="<feff005000740039004c0069006e006500330063005f004900740065006d004e0075006d006200650072005b0030005d>"
                />
                <textarea
                    name="2.d."
                    placeholder="2.d."
                    doc="g-28-2"
                    onAnimationEnd="<feff005000740039004c0069006e006500330064005f004100640064006900740069006f006e0061006c0049006e0066006f005b0030005d>"
                ></textarea>
                <Input
                    name="3.a. Page Number"
                    placeholder="Page Number"
                    type="number"
                    doc="g-28-2"
                    annot_field_key="<feff005000740039004c0069006e006500340061005f0050006100670065004e0075006d006200650072005b0030005d>"
                />
                <Input
                    name="3.b. Part Number"
                    placeholder="Part Number"
                    type="number"
                    doc="g-28-2"
                    annot_field_key="<feff005000740039004c0069006e006500340062005f0050006100720074004e0075006d006200650072005b0030005d>"
                />
                <Input
                    name="3.c. Item Number"
                    placeholder="Item Number"
                    type="number"
                    doc="g-28-2"
                    annot_field_key="<feff005000740039004c0069006e006500340063005f004900740065006d004e0075006d006200650072005b0030005d>"
                />
                <textarea
                    name="3.d."
                    placeholder="3.d."
                    doc="g-28-2"
                    annot_field_key="<feff005000740039004c0069006e006500340064005f004100640064006900740069006f006e0061006c0049006e0066006f005b0030005d>"
                ></textarea>
                    <Input
                    name="4.a. Page Number"
                    placeholder="Page Number"
                    type="number"
                    doc="g-28-2"
                    annot_field_key="<feff005000740039004c0069006e006500350061005f0050006100670065004e0075006d006200650072005b0030005d>"
                />
                <Input
                    name="4.b. Part Number"
                    placeholder="Part Number"
                    type="number"
                    doc="g-28-2"
                    annot_field_key="<feff005000740039004c0069006e006500350062005f0050006100720074004e0075006d006200650072005b0030005d>"
                />
                <Input
                    name="4.c. Item Number"
                    placeholder="Item Number"
                    type="number"
                    doc="g-28-2"
                    annot_field_key="<feff005000740039004c0069006e006500350063005f004900740065006d004e0075006d006200650072005b0030005d>"
                />
               <textarea
                    name="4.d."
                    placeholder="4.d."
                    doc="g-28-2"
                    annot_field_key="<feff005000740039004c0069006e006500350064005f004100640064006900740069006f006e0061006c0049006e0066006f005b0030005d>"
                ></textarea>
                    <Input
                    name="5.a. Page Number"
                    placeholder="Page Number"
                    type="number"
                    doc="g-28-2"
                    annot_field_key="<feff005000740039004c0069006e006500360061005f0050006100670065004e0075006d006200650072005b0031005d>"
                />
                <Input
                    name="5.b. Part Number"
                    placeholder="Part Number"
                    type="number"
                    doc="g-28-2"
                    annot_field_key="<feff005000740039004c0069006e006500360062005f0050006100720074004e0075006d006200650072005b0031005d>"
                />
                <Input
                    name="5.c. Item Number"
                    placeholder="Item Number"
                    type="number"
                    doc="g-28-2"
                    annot_field_key="<feff005000740039004c0069006e006500360063005f004900740065006d004e0075006d006200650072005b0031005d>"
                />
                <textarea
                    name="5.d."
                    placeholder="5.d."
                    doc="g-28-2"
                    annot_field_key="<feff005000740039004c0069006e006500360064005f004100640064006900740069006f006e0061006c0049006e0066006f005b0030005d>"
                ></textarea>
                    <Input
                    name="6.a. Page Number"
                    placeholder="Page Number"
                    type="number"
                    doc="g-28-2"
                    annot_field_key="<feff005000740039004c0069006e006500360061005f0050006100670065004e0075006d006200650072005b0030005d>"
                />
                <Input
                    name="6.b. Part Number"
                    placeholder="Part Number"
                    type="number"
                    doc="g-28-2"
                    annot_field_key="<feff005000740039004c0069006e006500360062005f0050006100720074004e0075006d006200650072005b0030005d>"
                />
                <Input
                    name="6.c. Item Number"
                    placeholder="Item Number"
                    type="number"
                    doc="g-28-2"
                    annot_field_key="<feff005000740039004c0069006e006500360063005f004900740065006d004e0075006d006200650072005b0030005d>"
                />
                <textarea
                    name="6.d."
                    placeholder="6.d."
                    doc="g-28-2"
                    annot_field_key="<feff005000740039004c0069006e006500360064005f004100640064006900740069006f006e0061006c0049006e0066006f005b0031005d>"
                ></textarea>
            </fieldset>
        </div>
        </WithValues>
    );
};

export default Part6;
