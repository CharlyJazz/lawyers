import React from "react";
import Input from "../../Input";

const Part5 = () => {
  return (
    <div>
      <h3>
        Part 5. Signature of Attorney or Accredited Representative
        </h3>
      <span>
        I have read and understand the regulations and conditions
        contained in 8 CFR 103.2 and 292 governing appearances and
        representation before DHS. I declare under penalty of perjury
        under the laws of the United States that the information I have
        provided on this form is true and correct.
        </span>
      <fieldset>
      </fieldset>
    </div>
  );
};

export default Part5;
