import React from "react";
import Input from "../../Input";
import StateSelect from "../../StateSelect";
import WithValues from '../../../WithValues'

const Part1 = () => {

  return (
    <WithValues doc={doc}>
      <div>
        <h2>Part 1. Information About Attorney or Accredited Representative</h2>
        {/* <h3>1. USCIS Oline Account Number (if any)</h3> */}
        <fieldset>
          <Input
            name="1. USCIS Oline Account Number (if any)"
            placeholder="USCIS Oline Account Number"
            doc="g-28-2"
            annot_field_key="<feff005000740031004c0069006e00650031005f00550053004300490053004f006e006c0069006e00650041006300630074004e0075006d006200650072005b0030005d>"
          />
        </fieldset>
        <h3>Name of Attorney or Accredited Representative</h3>
        <fieldset>
          <Input
            name="2.a. Family Name(Last Name)"
            placeholder="Family Name (Last Name)"
            doc="g-28-2"
            annot_field_key="<feff005000740031004c0069006e006500320061005f00460061006d0069006c0079004e0061006d0065005b0030005d>"
          />
          <Input
            name="2.b. Given Name (First Name)"
            placeholder="Given Name (Last Name)"
            doc="g-28-2"
            annot_field_key="<feff005000740031004c0069006e006500320062005f0047006900760065006e004e0061006d0065005b0030005d>"
          />
          <Input
            name="2.c. Middle Name"
            placeholder="Middle Name"
            doc="g-28-2"
            annot_field_key="<feff005000740031004c0069006e006500320063005f004d006900640064006c0065004e0061006d0065005b0030005d>"
          />
        </fieldset>
        <h3>Address of Attorney or Accredited Representative</h3>
        <fieldset>
          <Input
            name="3.a. Street Number and Name"
            placeholder="Street Number and Name"
            doc="g-28-2"
            annot_field_key="<feff004c0069006e006500330061005f005300740072006500650074004e0075006d006200650072005b0030005d>"
          />
          <Input
            name="3.b. Apt."
            inputType="radio"
            doc="g-28-2"
            annot_field_key="<feff004c0069006e006500330062005f0055006e00690074005b0032005d>"
          />
          <Input
            name="Ste"
            inputType="radio"
            placeholder="Street Number and Name"
            doc="g-28-2"
            annot_field_key="<feff004c0069006e006500330062005f0055006e00690074005b0030005d>"
          />
          <Input
            name="Flr"
            inputType="radio"
            placeholder="Street Number and Name"
            doc="g-28-2"
            annot_field_key="<feff004c0069006e006500330062005f0055006e00690074005b0030005d>"
          />
          <Input
            placeholder="3.b. Enter Apartment, Suite or Floor Number."
            doc="g-28-2"
            annot_field_key="<feff004c0069006e006500330062005f0041007000740053007400650046006c0072004e0075006d006200650072005b0030005d>"
          />
          <Input
            name="3.c. City or Town"
            placeholder="City or Town"
            doc="g-28-2"
            annot_field_key="<feff004c0069006e006500330063005f0043006900740079004f00720054006f0077006e005b0030005d>"
          />
          <StateSelect
            name="3.d. State"
            placeholder="State"
            doc="g-28-2"
            annot_field_key="<feff004c0069006e006500330064005f00530074006100740065005b0030005d>"
          />
          <Input
            name="3.e. ZIP Code (USPS ZIP Code Lookup)"
            placeholder="ZIP Code"
            doc="g-28-2"
            annot_field_key="<feff004c0069006e006500330065005f005a006900700043006f00640065005b0030005d>"
          />
          <Input
            name="3.f. Province"
            placeholder="Province"
            doc="g-28-2"
            annot_field_key="<feff004c0069006e006500330066005f00500072006f00760069006e00630065005b0030005d>"
          />
          <Input
            name="3.g. Postal Code"
            placeholder="Postal Code"
            doc="g-28-2"
            annot_field_key="<feff004c0069006e006500330067005f0050006f007300740061006c0043006f00640065005b0030005d>"
          />
          <StateSelect
            name="3.h. Country"
            placeholder="Country"
            doc="g-28-2"
            annot_field_key="<feff004c0069006e006500330068005f0043006f0075006e007400720079005b0030005d>"
          />
        </fieldset>
        <h3>Contact Information of Attoney or Accredited Representative</h3>
        <fieldset>
          <Input
            name="4. Daytime Telephone Number"
            type="tel"
            placeholder="Daytime Telephone Number"
            doc="g-28-2"
            annot_field_key="<feff004c0069006e00650034005f00440061007900740069006d006500540065006c006500700068006f006e0065004e0075006d006200650072005b0030005d>"
          />
          <Input
            name="5. Mobile Telephone Number (If any)"
            type="tel"
            placeholder="Mobile Telephone Number"
            doc="g-28-2"
            annot_field_key="<feff004c0069006e00650036005f0045004d00610069006c005b0030005d>"
          />
          <Input
            name="6. Email Address (if any)"
            type="email"
            placeholder="Email Address"
            doc="g-28-2"
            annot_field_key="<feff004c0069006e00650037005f004d006f00620069006c006500540065006c006500700068006f006e0065004e0075006d006200650072005b0030005d>"
          />

          <Input
            name="7. Fax Number (if any)"
            placeholder="Fax number"
            type="tel"
            doc="g-28-2"
            annot_field_key="<feff005000740031004900740065006d004e0075006d0062006500720037005f004600610078004e0075006d006200650072005b0030005d>"
          />
        </fieldset>
      </div>
    </WithValues>
  );
};

export default Part1;
