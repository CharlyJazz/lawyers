import React from "react";
import WithValues from '../../../WithValues'
import Input from "../../Input";

const Part4 = () => {
      
    return (
    <WithValues doc={doc}>
    <div>
        <h3>
        Part 4. Client's Consent to Representation and Signature
        </h3>
        <h3>
        Consent to Representation and Release of Information
        </h3>
        <span>
            I have requested the representation of and consented to being
            represented by the attorney or accredited representative named
            in Part 1. of this form. According to the Privacy Act of 1974
            and U.S. Department of Homeland Security (DHS) policy, I
            also consent to the disclosure to the named attorney or
            accredited representative of any records pertaining to me that
            appear in any system of records of USCIS, ICE, or CBP.
    </span>
    <h3>
    Part 4. Client's Consent to Representation and Signature (continued)
    </h3>
    <h3>
    Options Regarding Receipt of USCIS Notices and Documents
    </h3>
    <span>
        USCIS will send notices to both a represented party (the client)
        and his, her, or its attorney or accredited representative either
        through mail or electronic delivery. USCIS will send all secure
        identity documents and Travel Documents to the client's U.S.
        mailing address.
        If you want to have notices and/or secure identity documents
        sent to your attorney or accredited representative of record rather
        than to you, please select all applicable items below. You may
        change these elections through written notice to USCIS.
    </span>
    <fieldset>
        <Input
            name="1.a. I request that USCIS send original notices on an
            application or petition to the business address of my
            attorney or accredited representative as listed in this
            form."
            inputType="checkbox"
            doc="g-28-2"
            annot_field_key="<feff005000740034004c0069006e006500320061005f0043006800650063006b0042006f007800320061005b0030005d>"
        />
        <Input
            name="1.b. I request that USCIS send any secure identity
            document (Permanent Resident Card, Employment
            Authorization Document, or Travel Document) that I
            receive to the U.S. business address of my attorney or
            accredited representative (or to a designated military
            or diplomatic address in a foreign country (if
            permitted)). 
            NOTE: If your notice contains Form I-94,
            Arrival-Departure Record, USCIS will send the
            notice to the U.S. business address of your attorney
            or accredited representative. If you would rather
            have your Form I-94 sent directly to you, select
            Item Number 1.c."
            inputType="checkbox"
            doc="g-28-2"
            annot_field_key="<feff005000740034004c0069006e006500320062005f0043006800650063006b0042006f007800320062005b0030005d>"
        />
        <Input
            name="1.c. I request that USCIS send my notice containing Form
            I-94 to me at my U.S. mailing address."
            inputType="checkbox"
            doc="g-28-2"
            annot_field_key="<feff005000740034004c0069006e006500320063005f0043006800650063006b0042006f007800320063005b0030005d>"
        />
    </fieldset>
    <h3>
    Signature of Client or Authorized Signatory for an Entity
    </h3>
    <fieldset>
    </fieldset>
    </div>
    </WithValues>
  );
};

export default Part4;
