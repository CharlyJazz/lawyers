import React from "react";
import "./FormG282.css";
import Part1 from "./Parts/Part1";
import Part2 from "./Parts/Part2";
import Part3 from "./Parts/Part3";
import Part4 from "./Parts/Part4";
import Part5 from "./Parts/Part5";
import Part6 from "./Parts/Part6";
import { Link, Router } from "@reach/router";
import generatePDF from "../../generatePDF";

const FormG282 = () => {

  return (
    <div>
      <h1>G-28-2</h1>
      <div className="Layout-Form">
        <aside className="Layout-Form-Aside">
          <ul>
            {[1, 2, 3, 4, 5, 6].map(p => (
              <li key={p}>
                <Link to={`part-${p}`}>Part {p}</Link>
              </li>
            ))}
          </ul>
        </aside>
        <section className="Layout-Section">
          <Router>
            <Part1 path="part-1" />
            <Part2 path="part-2" />
            <Part3 path="part-3" />
            <Part4 path="part-4" />
            <Part5 path="part-5" />
            <Part6 path="part-6" />
          </Router>
        </section>
      </div>
      <button onClick={() => generatePDF("g-28-2")}>Generate PDF</button>
    </div>
  );
};

export default FormG282;
