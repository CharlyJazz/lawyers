import React from "react";
import Input from "../../Input";
import WithValues from '../../../WithValues'
import StateSelect from "../../StateSelect";

const Part4 = () => {

  return (
    <WithValues doc={doc}>
      <div>
        <h2>
          Part 4. Interpreter's Contact Information, Certification, and Signature
      </h2>
        <h3>Interpreter's Full Name</h3>
        <fieldset>
          <Input
            name="Interpreter's Family Name (Last Name)"
            doc="i-907"
          />
          <Input
            doc="i-907"
            name="Interpreter's Given Name (First Name)"
          />
          <Input
            doc="i-907"
            name="Interpreter's Business or Organization Name (if any)"
          />
        </fieldset>
        <h3>Interpreter's Mailing Address</h3>
        <fieldset>
          <Input
            name="Street Number and Name"
            annot_field_key="<feff00500034005f004c0069006e00650033005f005300740072006500650074004e0075006d006200650072004e0061006d0065005b0030005d>"
            doc="i-907" />
          <Input
            inputType="checkbox"
            label="Suite"
            name="Suite"
            annot_field_key="<feff00500034005f004c0069006e00650033005f0055006e00690074005b0030005d>"
            doc="i-907"
          />
          <Input
            inputType="checkbox"
            label="Apartment"
            name="Apartment"
            annot_field_key="<feff00500034005f004c0069006e00650033005f0055006e00690074005b0032005d>"
            doc="i-907"
          />
          <Input
            inputType="checkbox"
            label="Floor"
            name="Floor"
            annot_field_key="<feff00500034005f004c0069006e00650033005f0055006e00690074005b0031005d>"
            doc="i-907"
          />
          <Input
            label="Number"
            name="Number"
            annot_field_key="<feff00500034005f004c0069006e00650033005f0041007000740053007400650046006c0072004e0075006d006200650072005b0030005d>"
            doc="i-907" />
          <StateSelect
            name="state-part-2"
            doc="i-907"
            annot_field_key="<feff00500034005f004c0069006e00650033005f00530074006100740065005b0030005d>"
          />
          <Input
            label="City or Town"
            name="City or Town"
            annot_field_key="<feff00500034005f004c0069006e00650033005f00430069007400790054006f0077006e005b0030005d>"
            doc="i-907"
          />
          <Input
            label="ZIP Code"
            name="ZIP Code"
            annot_field_key="<feff00500034005f004c0069006e00650033005f005a006900700043006f00640065005b0030005d>"
            doc="i-907"
          />
          <Input
            label="Province"
            name="Province"
            annot_field_key="<feff00500034005f004c0069006e00650033005f00500072006f00760069006e00630065005b0030005d>"
            doc="i-907"
          />
          <Input
            label="Postal COde"
            name="Postal COde"
            annot_field_key="<feff00500034005f004c0069006e00650033005f0050006f007300740061006c0043006f00640065005b0030005d>"
            doc="i-907"
          />
          <Input
            label="Country"
            name="Country"
            annot_field_key="<feff00500034005f004c0069006e00650033005f0043006f0075006e007400720079005b0030005d>"
            doc="i-907"
          />
        </fieldset>
        <h3>Interpreter's Contact Information</h3>
        <fieldset>
          <Input
            doc="i-907"
            name="Interpreter's Daytime Telephone Number"
            annot_field_key="<feff00500034005f004c0069006e00650034005f00440061007900740069006d006500540065006c006500500068006f006e0065004e0075006d006200650072005b0030005d>"
          />
          <Input
            doc="i-907"
            annot_field_key="<feff00500034005f004c0069006e00650034005f00440061007900740069006d006500540065006c006500500068006f006e0065004e0075006d006200650072005b0031005d>"
            name="Interpreter's Mobile Telephone Number (if any)"
          />
          <Input
            doc="i-907"
            name="Interpreter's Email Address (if any)"
            type="email"
            annot_field_key="<feff00500034005f004c0069006e00650035005f0045006d00610069006c005b0030005d>"
          />
        </fieldset>
        <h3>Interpreter's Certification</h3>
        <fieldset>
          <p>I certify, under penalty of perjury, that:</p>
          <p>I am fluent in English and</p>
          <Input
            name="(Language)"
            doc="i-907"
            annot_field_key="<feff00500034005f004c0069006e00650036005f004c0061006e00670075006100670065005b0030005d>"
          />
          <p>which is the same language specified in Part 3.,</p>
          <p>
            Item B. in Item Number 1., and I have read to this requestor in the
            identified language every question and instruction on this request and
            his or her answer to every question. The requestor informed me that he
            or she understands every instruction, question, and answer on the
            request, including the Requestor's Declaration and Certification, and
            has verified the accuracy of every answer.
        </p>
        </fieldset>
      </div>
    </WithValues>
  );
};

export default Part4;
