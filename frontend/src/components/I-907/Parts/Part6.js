import React from "react";
import Input from "../../Input";
import WithValues from '../../../WithValues'

const Part6 = () => {

  return (
    <WithValues doc={doc}>
      <div>
        <h2>Part 6. Additional Information</h2>
        <p>
          If you need extra space to provide any additional information within
          this petition, use the space below. If you need more space than what is
          provided, you may make copies of this page to complete and file with
          this petition or attach a separate sheet of paper. Type or print your
          name and A-Number (if any) at the top of each sheet; indicate the Page
          Number, Part Number, and Item Number to which your answer refers; and
          sign and date each sheet.
      </p>
        <h3>Complete</h3>
        <fieldset>
          <Input
            name="Family Name (Last Name)"
            doc="i-907"
            annot_field_key="<feff005000740031004c0069006e00650033005f00460061006d0069006c0079004e0061006d0065005b0031005d>"
          />
          <Input
            doc="i-907"
            name="Given Name (First Name)"
            annot_field_key="<feff005000740031004c0069006e00650033005f0047006900760065006e004e0061006d0065005b0031005d>"
          />
          <Input
            doc="i-907"
            name="Middle Name"
            annot_field_key="<feff005000740031004c0069006e00650033005f004d006900640064006c0065004e0061006d0065005b0031005d>"
          />
        </fieldset>
        <h3>A-Number</h3>
        <fieldset>
          <Input
            doc="i-907"
            name="A-Number (if any)"
            annot_field_key="<feff005000740031004c0069006e00650031005f0041006c00690065006e0052006500670069007300740072006100740069006f006e004e0075006d006200650072005b0031005d>"
          />
        </fieldset>
        <br />
        <br />
        <fieldset>
          <Input
            name="3.A Page number"
            doc="i-907"
            annot_field_key="<feff005000740039004c0069006e006500330061005f0050006100670065004e0075006d006200650072005b0030005d>"
          />
          <Input
            name="3.B Part number"
            doc="i-907"
            annot_field_key="<feff005000740039004c0069006e006500330062005f0050006100720074004e0075006d006200650072005b0030005d>"
          />
          <Input
            name="3.C Item number"
            doc="i-907"
            annot_field_key="<feff005000740039004c0069006e006500330063005f004900740065006d004e0075006d006200650072005b0030005d>"
          />
          <Input
            name="3.D"
            doc="i-907"
            annot_field_key="<feff005000740039004c0069006e006500330064005f004100640064006900740069006f006e0061006c0049006e0066006f005b0030005d>"
          />
        </fieldset>
        <br />
        <br />
        <fieldset>
          <Input
            name="4.A Page number"
            doc="i-907"
            annot_field_key="<feff005000740039004c0069006e006500340061005f0050006100670065004e0075006d006200650072005b0030005d>"
          />
          <Input
            name="4.B Part number"
            doc="i-907"
            annot_field_key="<feff005000740039004c0069006e006500340062005f0050006100720074004e0075006d006200650072005b0030005d>"
          />
          <Input
            name="4.C Item number"
            doc="i-907"
            annot_field_key="<feff005000740039004c0069006e006500340063005f004900740065006d004e0075006d006200650072005b0030005d>"
          />
          <Input
            name="4.D"
            doc="i-907"
            annot_field_key="<feff005000740039004c0069006e006500340064005f004100640064006900740069006f006e0061006c0049006e0066006f005b0030005d>"
          />
        </fieldset>
        <br />
        <br />
        <fieldset>
          <Input
            name="5.A Page number"
            doc="i-907"
            annot_field_key="<feff005000740039004c0069006e006500340061005f0050006100670065004e0075006d006200650072005b0031005d>"
          />
          <Input
            name="5.B Part number"
            doc="i-907"
            annot_field_key="<feff005000740039004c0069006e006500340062005f0050006100720074004e0075006d006200650072005b0031005d>"
          />
          <Input
            name="5.C Item number"
            doc="i-907"
            annot_field_key="<feff005000740039004c0069006e006500340063005f004900740065006d004e0075006d006200650072005b0031005d>"
          />
          <Input
            name="5.D"
            doc="i-907"
            annot_field_key="<feff005000740039004c0069006e006500340064005f004100640064006900740069006f006e0061006c0049006e0066006f005b0031005d>"
          />
        </fieldset>
      </div>
    </WithValues>
  );
};

export default Part6;
