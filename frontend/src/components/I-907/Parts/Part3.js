import React from "react";
import Input from "../../Input";
import WithValues from '../../../WithValues'

const Part3 = () => {

  return (
    <WithValues doc={doc}>
    <div>
      <h2>
        Part 3. Requestor's Statement, Contact Information, Declaration,
        Certification, and Signature
      </h2>
      <small>
        NOTE: Read the Penalties section of the Form I-907 Instructions before
        completing this section. I understand that U.S. Citizenship and
        Immigration Services (USCIS) will refund the Premium Processing Service
        fee to the person listed in Part 1. of this request if USCIS does not
        take an action on the related case within 15 calendar days after the
        appropriate USCIS office physically receives this request. I understand
        that case actions include a referral for investigation of suspected
        fraud, misrepresentation, or the issuance of an approval notice, a
        request for evidence, a notice of intent to deny, or a denial notice.
      </small>
      <h3>Requestor's Statement</h3>
      <fieldset>
        <p>
          NOTE: Select the box for either Item A. or B. in Item Number 1. If
          applicable, select the box for Item Number 2.
        </p>
        <p>1. Requestor's Statement Regarding the Interpreter</p>
        <Input
          inputType="checkbox"
          name="A. I can read and understand English, and I have read and understand every question and instruction on this request and
          my answer to every question."
          annot_field_key="<feff00500033005f004c0069006e00650031005f0042005f004c0061006e00670075006100670065005b0030005d>"
          doc="i-907"
        />
        <Input
          inputType="checkbox"
          name="B. The interpreter named in Part 4. read to me every question and instruction on this request and my answer to every
            question in other language* which I am fluent, and I understood
            everything."
          annot_field_key="<feff00500033005f004c0069006e00650031005f0043006800650063006b0062006f0078005b0030005d>"
          doc="i-907"
        />
        <Input 
          name="The other language" 
          doc="i-907"
          annot_field_key="<feff00500033005f004c0069006e00650031005f0042005f004c0061006e00670075006100670065005b0030005d>"
        />
        <p>2. Requestor's Statement Regarding the Preparer</p>
        <Input 
          name="At my request, the preparer named in Part 5. Prepared this request for me based only upon information I provided or authorized." 
          type="checkbox"
          annot_field_key="<feff00500033005f004c0069006e00650032005f0043006800650063006b0062006f0078005b0030005d>"
          doc="i-907"
          />
          <Input
            name=" preparer named in part 5"
            annot_field_key="<feff00500033005f004c0069006e00650032005f004e0061006d0065006f00660052006500700072006500730065006e007400610074006900760065005b0030005d>"
            doc="i-907"          
          />
      </fieldset>
      <h3>Requestor's Contact Information</h3>
      <fieldset>
        <Input 
          name="Requestor's Daytime Telephone Number" 
          doc="i-907"
          annot_field_key="<feff00500033005f004c0069006e00650034005f00440061007900740069006d006500540065006c006500500068006f006e0065004e0075006d006200650072005b0030005d>"
          />
        <Input 
          doc="i-907"
          annot_field_key="<feff00500033005f004c0069006e00650035005f004d006f00620069006c006500540065006c006500500068006f006e0065004e0075006d006200650072005b0030005d>"
          name="Requestor's Mobile Telephone Number (if any)" 
          />
        <Input 
          doc="i-907"
          annot_field_key="<feff00500033005f004c0069006e00650035005f004d006f00620069006c006500540065006c006500500068006f006e0065004e0075006d006200650072005b0031005d>"
          name="Requestor's Fax Number (if any)" 
          />
        <Input 
          doc="i-907"
          annot_field_key="<feff00500033005f004c0069006e00650036005f0045006d00610069006c005b0030005d>"
          name="Requestor's Email Address (if any)"
          type="mail" 
          />
      </fieldset>
    </div>
  </WithValues>
  );
};

export default Part3;
