import React from "react";
import Input from "../../Input";
import WithValues from '../../../WithValues'
import StateSelect from "../../StateSelect";

const Part1 = () => {

  return (
    <WithValues doc={doc}>
    <div>
      <h2>Part 1. Information About the Person Filing This Request</h2>
      <h3>General Information and Company data</h3>
      <fieldset>
        <Input
          name="Alien Registration Number (A-Number) (if any)"
          annot_field_key="<feff005000740031004c0069006e00650031005f0041006c00690065006e0052006500670069007300740072006100740069006f006e004e0075006d006200650072005b0030005d>"
          doc="i-907"
        />
        <Input
          name="USCIS Online Account Number (if any)"
          annot_field_key="<feff005000740032004c0069006e00650032005f00550053004300490053004f006e006c0069006e0065004100630074004e0075006d006200650072005b0030005d>"
          doc="i-907"
        />
      </fieldset>
      <h3>Name</h3>
      <fieldset>
        <Input
          name="Family Name (Last Name)"
          annot_field_key="<feff005000740031004c0069006e00650033005f00460061006d0069006c0079004e0061006d0065005b0030005d>"
          doc="i-907"
        />
        <Input
          name="Given Name (First Name)"
          annot_field_key="<feff005000740031004c0069006e00650033005f0047006900760065006e004e0061006d0065005b0030005d>"
          doc="i-907"
        />
        <Input
          name="Middle Name"
          annot_field_key="<feff005000740031004c0069006e00650033005f004d006900640064006c0065004e0061006d0065005b0030005d>"
          doc="i-907"
        />
      </fieldset>
      <h3>
        Company or Organization Named in the Related Case (If filed on behalf of
        a company or organization)
      </h3>
      <fieldset>
        <Input
          name="Write the Name" 
          annot_field_key="<feff00500061007200740031005f004c0069006e00650036005f0043006f006d00700061006e0079006f0072004f007200670061006e0069007a006100740069006f006e004e0061006d0065005b0030005d>" 
          doc="i-907" />
      </fieldset>
      <h3>Mailing Addres</h3>
      <fieldset>
        <Input 
          name="In Care Of Name" 
          annot_field_key="<feff00500061007200740031005f004c0069006e00650035005f004d00610069006c0069006e00670041006400640072006500730073005f0049006e0043006100720065006f0066004e0061006d0065005b0030005d>" 
          doc="i-907" 
          />
        <Input 
          name="Street Number and Name" 
          annot_field_key="<feff00500061007200740031005f004c0069006e00650035005f004d00610069006c0069006e00670041006400640072006500730073005f005300740072006500650074004e0075006d006200650072004e0061006d0065005b0030005d>" 
          doc="i-907" />
        <Input
          inputType="checkbox"
          label="Suite"
          name="Suite"
          annot_field_key="<feff00500061007200740031005f004c0069006e00650035005f004d00610069006c0069006e00670041006400640072006500730073005f0055006e00690074005b0030005d>"
          doc="i-907"
        />
        <Input
          inputType="checkbox"
          label="Apartment"
          name="Apartment"
          annot_field_key="<feff00500061007200740031005f004c0069006e00650035005f004d00610069006c0069006e00670041006400640072006500730073005f0055006e00690074005b0032005d>"
          doc="i-907"
        />
        <Input
          inputType="checkbox"
          label="Floor"
          name="Floor"
          annot_field_key="<feff00500061007200740031005f004c0069006e00650035005f004d00610069006c0069006e00670041006400640072006500730073005f0055006e00690074005b0031005d>"
          doc="i-907"
        />
        <Input 
          label="Number" 
          name="Number" 
          annot_field_key="<feff00500061007200740031005f004c0069006e00650035005f004d00610069006c0069006e00670041006400640072006500730073005f0041007000740053007400650046006c0072004e0075006d006200650072005b0030005d>" 
          doc="i-907" 
          />
        <StateSelect
          name="5. state"
          id="state-part-1"
          annot_field_key="<feff00500061007200740031005f004c0069006e00650035005f004d00610069006c0069006e00670041006400640072006500730073005f00530074006100740065005b0030005d>" 
          />
        <Input
          label="City or Town"
          name="City or Town"
          annot_field_key="<feff00500061007200740031005f004c0069006e00650035005f004d00610069006c0069006e00670041006400640072006500730073005f00430069007400790054006f0077006e005b0030005d>"
          doc="i-907"
        />
        <Input
          label="ZIP Code"
          name="ZIP Code"
          annot_field_key="<feff005000740031004c0069006e00650035005f004d00610069006c0069006e00670041006400640072006500730073005f005a006900700043006f00640065005b0030005d>"
          doc="i-907"
        />
        <Input
          label="Province"
          name="Province"
          annot_field_key="<feff00500061007200740031005f004c0069006e00650035005f004d00610069006c0069006e00670041006400640072006500730073005f00500072006f00760069006e00630065005b0030005d>"
          doc="i-907"
        />
        <Input
          label="Postal Code"
          name="Postal Code"
          annot_field_key="<feff00500061007200740031005f004c0069006e00650035005f004d00610069006c0069006e00670041006400640072006500730073005f0050006f007300740061006c0043006f00640065005b0030005d>"
          doc="i-907"
        />
        <StateSelect 
          label="Country" 
          name="Country" 
          annot_field_key="<feff00500061007200740031005f004c0069006e00650035005f004d00610069006c0069006e00670041006400640072006500730073005f0043006f0075006e007400720079005b0030005d>" 
          doc="i-907" 
          />
      </fieldset>
      <h3>
        Is your current mailing address the same as your physical address?
      </h3>
      <fieldset>
        <Input
          inputType="checkbox"
          label="Yes"
          name="Yes"
          annot_field_key="<feff00500061007200740031004c0069006e00650035005f0043006800650063006b0062006f0078005b0030005d>"
          doc="i-907"
        />
        <Input
          inputType="checkbox"
          label="No"
          name="No"
          annot_field_key="<feff00500061007200740031004c0069006e00650035005f0043006800650063006b0062006f0078005b0031005d>"
          doc="i-907"
        />
      </fieldset>
      <h3>Physical Address</h3>
      <fieldset>
        <Input 
          name="Street Number and Name" 
          annot_field_key="<feff00500061007200740031005f004c0069006e00650034005f0050006800790073006900630061006c0041006400640072006500730073005f005300740072006500650074004e0075006d006200650072004e0061006d0065005b0030005d>"
          doc="i-907"
          />
        <Input
          inputType="checkbox"
          label="Suite"
          name="Suite"
          annot_field_key="<feff00500061007200740031005f004c0069006e00650034005f0050006800790073006900630061006c0041006400640072006500730073005f0055006e00690074005b0032005d>"
          doc="i-907"
        />
        <Input
          inputType="checkbox"
          label="Apartment"
          name="Apartment"
          annot_field_key="<feff00500061007200740031005f004c0069006e00650034005f0050006800790073006900630061006c0041006400640072006500730073005f0055006e00690074005b0030005d>"
          doc="i-907"
        />
        <Input
          inputType="checkbox"
          label="Floor"
          name="Floor"
          annot_field_key="<feff00500061007200740031005f004c0069006e00650034005f0050006800790073006900630061006c0041006400640072006500730073005f0055006e00690074005b0031005d>"
          doc="i-907"
        />
        <Input 
          label="Number" 
          name="Number" 
          annot_field_key="<feff00500061007200740031005f004c0069006e00650034005f0050006800790073006900630061006c0041006400640072006500730073005f005300740072006500650074004e0075006d006200650072004e0061006d0065005b0030005d>" 
          doc="i-907" 
          />
        <StateSelect 
          name="state-part-1" 
          annot_field_key="<feff00500061007200740031005f004c0069006e00650034005f0050006800790073006900630061006c0041006400640072006500730073005f00530074006100740065005b0030005d>"
          doc="i-907" 
        />
        <Input
          label="City or Town"
          name="City or Town"
          annot_field_key="<feff00500061007200740031005f004c0069006e00650034005f0050006800790073006900630061006c0041006400640072006500730073005f00430069007400790054006f0077006e005b0030005d>"
          doc="i-907"
        />
        <Input
          label="ZIP Code"
          name="ZIP Code"
          annot_field_key="<feff00500061007200740031005f004c0069006e00650034005f0050006800790073006900630061006c0041006400640072006500730073005f005a006900700043006f00640065005b0030005d>"
          doc="i-907"
        />
        <Input
          label="Province"
          name="Province"
          annot_field_key="<feff00500061007200740031005f004c0069006e00650034005f0050006800790073006900630061006c0041006400640072006500730073005f00500072006f00760069006e00630065005b0030005d>"
          doc="i-907"
        />
        <Input
          label="Postal Code"
          name="Postal Code"
          annot_field_key="<feff00500061007200740031005f004c0069006e00650034005f0050006800790073006900630061006c0041006400640072006500730073005f0050006f007300740061006c0043006f00640065005b0030005d>"
          doc="i-907"
        />
        <Input 
          label="Country" 
          name="Country" 
          annot_field_key="<feff00500061007200740031005f004c0069006e00650034005f0050006800790073006900630061006c0041006400640072006500730073005f0043006f0075006e007400720079005b0030005d>" 
          doc="i-907"
          />
      </fieldset>
      <h3>Request for Premium Processing Service (select only one box):</h3>
      <fieldset>
        <Input
          inputType="checkbox"
          name="I am the petitioner who is filing or has filed a petition eligible for Premium Processing Service."
          annot_field_key="<feff00500061007200740031005f004c0069006e006500310031005f0043006800650063006b0042006f0078005b0032005d>"
          doc="i-907"
        />
        <Input
          inputType="checkbox"
          name="I am the attorney or accredited representative for the petitioner who is filing or has filed a petition eligible for Premium
          Processing Service. (Complete and submit Form G-28, Notice of Entry of Appearance as Attorney or Accredited
          Representative, or Form G-28I, Notice of Entry of Appearance as Attorney In Matters Outside the Geographical Confines of
          the United States, if Form G-28 or Form G-28I has not been submitted with the petition.)"
          annot_field_key="<feff00500061007200740031005f004c0069006e006500310031005f0043006800650063006b0042006f0078005b0031005d>"
          doc="i-907"
        />
        <Input
          inputType="checkbox"
          name="I am the applicant who is filing or has filed an application eligible for Premium Processing Service."
          annot_field_key="<feff00500061007200740031005f004c0069006e006500310031005f0043006800650063006b0042006f0078005b0030005d>"
          doc="i-907"
        />
        <Input
          inputType="checkbox"
          name="I am the attorney or accredited representative for the applicant who is filing or has filed an application eligible for
          Premium Processing Service. (Complete and submit Form G-28 or Form G-28I, if Form G-28 or Form G-28I has not been
          submitted with the application.)"
          annot_field_key="<feff00500061007200740031005f004c0069006e006500310031005f0043006800650063006b0042006f0078005b0033005d>"
          doc="i-907"
        />
      </fieldset>
    </div>
    /</WithValues>
  );
};

export default Part1;
