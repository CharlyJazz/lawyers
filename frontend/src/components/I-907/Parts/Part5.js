import React from "react";
import Input from "../../Input";
import WithValues from '../../../WithValues'
import StateSelect from "../../StateSelect";

const Part5 = () => {

  return (
    <WithValues doc={doc}>
    <div>
      <h2>
        Part 5. Contact Information, Declaration, and Signature of the Person
        Preparing this Request, if Other Than the Requestor
      </h2>   
      <h3>Preparer's Full Name</h3>
      <fieldset>
        <Input 
          name="Preparer's Family Name (Last Name)" 
          doc="i-907" 
          annot_field_key="<feff00500035005f004c0069006e00650031005f0050007200650070006100720065007200460061006d0069006c0079004e0061006d0065005b0030005d>"
          />
        <Input 
          name="Preparer's Given Name (First Name)" 
          doc="i-907" 
          annot_field_key="<feff00500035005f004c0069006e00650031005f005000720065007000610072006500720047006900760065006e004e0061006d0065005b0030005d>"
          />
        <Input
          name="Preparer's Business or Organization Name (if any)"
          doc="i-907"
          annot_field_key="<feff00500035005f004c0069006e00650032005f004e0061006d0065006f00660042007500730069006e006500730073006f0072004f00720067004e0061006d0065005b0030005d>"
        />
      </fieldset>
      <h3>Preparer's Mailing Address</h3>
      <fieldset>
        <Input 
          name="Street Number and Name"  
          annot_field_key="<feff00500035005f004c0069006e00650033005f005300740072006500650074004e0075006d006200650072004e0061006d0065005b0030005d>"
          doc="i-907" 
          />
        <Input
          inputType="checkbox"
          label="Suite"
          name="Suite"
          annot_field_key="<feff00500035005f004c0069006e00650033005f0055006e00690074005b0030005d>"
          doc="i-907"
        />
        <Input
          inputType="checkbox"
          label="Apartment"
          name="Apartment"
          annot_field_key="<feff00500035005f004c0069006e00650033005f0055006e00690074005b0032005d>"
          doc="i-907"
        />
        <Input
          inputType="checkbox"
          label="Floor"
          name="Floor"
          annot_field_key="<feff00500035005f004c0069006e00650033005f0055006e00690074005b0031005d>"
          doc="i-907"
        />
        <Input 
          label="Number" 
          name="Number" 
          doc="i-907" 
          annot_field_key="<feff0050007400310030004c0069006e006500330062005f0041007000740053007400650046006c0072004e0075006d006200650072005b0030005d>"
          />
        <StateSelect 
          doc="i-907" 
          name="state-part-2"
          annot_field_key="<feff00500035005f004c0069006e00650033005f00530074006100740065005b0030005d>" 
        />
        <Input
          label="City or Town"
          name="City or Town"
          annot_field_key="<feff00500035005f004c0069006e00650033005f00430069007400790054006f0077006e005b0030005d>"
          doc="i-907"
        />
        <Input
          label="ZIP Code"
          name="ZIP Code"
          annot_field_key="<feff00500035005f004c0069006e00650033005f005a006900700043006f00640065005b0030005d>"
          doc="i-907"
        />
        <Input
          label="Province"
          name="Province"
          annot_field_key="<feff00500035005f004c0069006e00650033005f00500072006f00760069006e00630065005b0030005d>"
          doc="i-907"
        />
        <Input
          label="Postal COde"
          name="Postal COde"
          annot_field_key="<feff00500035005f004c0069006e00650033005f0050006f007300740061006c0043006f00640065005b0030005d>"
          doc="i-907"
        />
        <Input 
          label="Country" 
          name="Country" 
          annot_field_key="<feff00500035005f004c0069006e00650033005f0043006f0075006e007400720079005b0030005d>"
          doc="i-907" 
          />
      </fieldset>
      <h3>Preparer's Contact Information</h3>
      <fieldset>
        <Input 
          name="Preparer's Daytime Telephone Number" 
          type="number"
          doc="i-907" 
          annot_field_key="<feff00500035005f004c0069006e00650035005f004600610078004e0075006d006200650072005b0030005d>"
          />
        <Input 
          name="Preparer's Mobile Telephone Number (if any)" 
          doc="i-907" 
          type="number"
          annot_field_key="<feff00500035005f004c0069006e00650034005f00440061007900740069006d006500540065006c006500500068006f006e0065004e0075006d006200650072005b0030005d>"
          />
        <Input 
          name="Preparer's Email Address (if any)" 
          doc="i-907"
          type="mail" 
          annot_field_key="<feff00500035005f004c0069006e00650036005f0045006d00610069006c0041006400640072006500730073005b0030005d>"
          />
      </fieldset>
      <h3>Preparer's Statement</h3>
      <fieldset>
        <Input
          inputType="checkbox"
          name="I am not an attorney or accredited representative but have prepared this request on behalf of the requestor with the
          requestor's consent."
          annot_field_key="<feff00500035005f004c0069006e006500370061005f0043006800650063006b0062006f0078005b0030005d>"
          doc="i-907"
        />
        <Input
          inputType="checkbox"
          name="I am an attorney or accredited representative and my representation of the requestor in this case"
          annot_field_key="<feff00500035005f004c0069006e006500370062005f0043006800650063006b0062006f0078005b0030005d>"
          doc="i-907"
        />
        <Input
          inputType="checkbox"
          name="Extends"
          annot_field_key="<feff00500035005f004c0069006e006500370062005f0065007800740065006e00640073005b0030005d>"
          doc="i-907"
        />
        <Input
          inputType="checkbox"
          name="Does not extend beyond the preparation of this request."
          annot_field_key="<feff00500035005f004c0069006e006500370062005f006e006f00740065007800740065006e00640073005b0030005d>"
          doc="i-907"
        />
        <small>
          NOTE: If you are an attorney or accredited representative, you may
          need to submit a completed Form G-28 or Form G-28I with this request.
        </small>
      </fieldset>
    </div>
    </WithValues>
  );
};

export default Part5;
