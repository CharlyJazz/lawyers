import React from "react";
import PropTypes from "prop-types";
import { URL_BASE } from "../constants";

const StateSelect = ({ annot_field_key, doc, options, ...props }) => {
  const sendValueToBackend = () => {
    const value = document.getElementById(props.id).value;
    fetch(URL_BASE + "/field", {
      method: "POST",
      body: JSON.stringify({
        doc,
        annot_field_key,
        value: "AZ"
      }),
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("jwt")}`
      }
    })
      .then(res => res.json())
      .then(res => console.log(res))
      .catch(err => console.log(err));
  };
  return (
    <div>
      <label
        style={{
          display: "flex",
          alignItems: "center"
        }}
        htmlFor={props.id}
      >
        State
        <select
          style={{
            marginLeft: 15,
            padding: 5,
            background: "white",
            borderRadius: "3.2px"
          }}
          id={props.id}
          onBlur={sendValueToBackend}
        >
          {options.map(o => (
            <option key={o} value={o}>
              {o}
            </option>
          ))}
        </select>
      </label>
    </div>
  );
};

StateSelect.defaultProps = {
  options: [
    "AA",
    "AE",
    "AK",
    "AL",
    "AP",
    "AR",
    "AS",
    "AZ",
    "CA",
    "CO",
    "CT",
    "DC",
    "DE",
    "FL",
    "FM",
    "GA",
    "GU",
    "HI",
    "IA",
    "ID",
    "IL",
    "IN",
    "KS",
    "KY",
    "LA",
    "MA",
    "MD",
    "ME",
    "MH",
    "MI",
    "MN",
    "MO",
    "MP",
    "MS",
    "MT",
    "NC",
    "ND",
    "NE",
    "NH",
    "NJ",
    "NM",
    "NV",
    "NY",
    "OH",
    "OK",
    "OR",
    "PA",
    "PR",
    "PW",
    "RI",
    "SC",
    "SD",
    "TN",
    "TX",
    "UT",
    "VA",
    "VI",
    "VT",
    "WA",
    "WI",
    "WV",
    "WY"
  ]
};

StateSelect.propTypes = {
  options: PropTypes.array
};

export default StateSelect;
