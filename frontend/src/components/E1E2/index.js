import React from "react";
import "./E1E2.css";
import Section0 from "./Sections/Section0";
import Section1 from "./Sections/Section1";
import Section2 from "./Sections/Section2";
import Section3 from "./Sections/Section3";
import Section4 from "./Sections/Section4";
import { Link, Router } from "@reach/router";

const E1E2 = () => {

  return (
    <div>
      <h1>E-1/E-2 Classification Supplement</h1>
      <div className="Layout-Form">
        <aside className="Layout-Form-Aside">
          <ul>
            <li>
              <Link to="/e1-e2">E-1/E-2</Link>
            </li>
            {[1, 2, 3, 4].map(p => (
              <li key={p}>
                <Link to={`section-${p}`}>Section {p}</Link>
              </li>
            ))}
          </ul>
        </aside>
        <section className="Layout-Section">
          <Router>
            <Section0 path="/" doc={doc} />
            <Section1 path="section-1" />
            <Section2 path="section-2" />
            <Section3 path="section-3" />
            <Section4 path="section-4" />
          </Router>
        </section>
      </div>
    </div>
  );
};

export default E1E2;
