import React from "react";
import WithValues from '../../../WithValues'
import Input from "../../Input";

const Section0 = () => {

  return (
    <WithValues doc={doc}>
      <div>
        <h3>Name of the Petitioner</h3>
        <fieldset>
          <Input
            label="Petitioner"
            name="Petitioner"
            annot_field_key="<feff0053007500700045004c0069006e00650031005f004e0061006d0065006f00660045006d0070006c006f007900650072005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>Name of the Beneficiary</h3>
        <fieldset>
          <Input
            label="Family Name (Last Name)"
            name="Family Name (Last Name)"
            annot_field_key="<feff004c0069006e00650031005f00460061006d0069006c0079004e0061006d0065005b0032005d>"
            doc="i-129"
          />
          <Input
            label="Given Name (First Name)"
            name="Given Name (First Name)"
            annot_field_key="<feff004c0069006e00650031005f0047006900760065006e004e0061006d0065005b0032005d>"
            doc="i-129"
          />
          <Input
            label="Middle Name"
            name="Middle Name"
            annot_field_key="<feff004c0069006e00650031005f004d006900640064006c0065004e0061006d0065005b0032005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>Classification sought:</h3>
        <fieldset>
          <Input
            inputType="radio"
            label="E-1 Treaty Trader"
            name="Classification"
            annot_field_key="<feff00450031005f005400720065006100740079005400720061006400650072005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="E-2 Treaty Investor"
            name="Classification"
            annot_field_key="<feff00450032005f0054007200650061007400790049006e0076006500730074006f0072005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="E-2 CNMI Investor"
            name="Classification"
            annot_field_key="<feff00450032005f0043004e004d0049005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>Name of country signatory to treaty with the United States</h3>
        <fieldset>
          <Input
            label="Country"
            name="Country"
            annot_field_key="<feff004c0069006e0065005f0043006f0075006e007400720079005b0031005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>
          Are you seeking advice from USCIS to determine whether changes in the
          terms or conditions of E status for one or more employees are
          substantive?
      </h3>
        <fieldset>
          <Input
            inputType="radio"
            label="Yes"
            name="Status"
            annot_field_key="<feff0053007500700045004c0069006e00650035005f005900650073005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="No"
            name="Status"
            annot_field_key="<feff0053007500700045004c0069006e00650035005f004e006f005b0030005d>"
            doc="i-129"
          />
        </fieldset>
      </div>
    </WithValues>
  );
};

export default Section0;
