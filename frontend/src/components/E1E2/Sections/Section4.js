import React from "react";
import WithValues from '../../../WithValues'
import Input from "../../Input";

const Section4 = () => {

  return (
    <WithValues doc={doc}>
      <div>
        <h2>Section 4. Complete If Filing for an E-2 Treaty Investor</h2>
        <h3>Total Investment</h3>
        <fieldset>
          <Input
            label="Cash"
            name="Cash"
            annot_field_key="<feff00450053006500630034005f0043006100730068005b0030005d>"
            doc="i-129"
          />
          <Input
            label="Equipment"
            name="Equipment"
            annot_field_key="<feff00450053006500630034005f00450071007500690070006d0065006e0074005b0030005d>"
            doc="i-129"
          />
          <Input
            label="Other"
            name="Other"
            annot_field_key="<feff00450053006500630034005f00450071007500690070006d0065006e0074005b0031005d>"
            doc="i-129"
          />
          <Input
            label="Inventory"
            name="Inventory"
            annot_field_key="<feff00450053006500630034005f0049006e00760065006e0074006f00720079005b0030005d>"
            doc="i-129"
          />
          <Input
            label="Premises"
            name="Premises"
            annot_field_key="<feff00450053006500630034005f005000720065006d0069007300650073005b0030005d>"
            doc="i-129"
          />
          <Input
            label="Total"
            name="Total"
            annot_field_key="<feff00450053006500630034005f0054006f0074006c0061005b0030005d>"
            doc="i-129"
          />
        </fieldset>
      </div>
    </WithValues>
  );
};

export default Section4;
