import React from "react";
import InputTable from "../../InputTable";
import Input from "../../Input";
import WithValues from '../../../WithValues'

const Section2 = () => {

  return (
    <WithValues doc={doc}>
      <div>
        <h2>Section 2. Additional Information About the U.S. Employer</h2>
        <h3>How is the U.S. company related to the company abroad?</h3>
        <fieldset>
          <Input
            inputType="radio"
            label="Parent"
            name="Relation"
            annot_field_key="<feff00530065006300740032005f0050006100720065006e0074005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="Branch"
            name="Relation"
            annot_field_key="<feff00530065006300740032005f004200720061006e00630068005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="Subsidiary"
            name="Relation"
            annot_field_key="<feff00530065006300740032005f0053007500620073006900640069006100720079005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="Affiliate"
            name="Relation"
            annot_field_key="<feff00530065006300740032005f0041006600660069006c0069006100740065005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="radio"
            label="Joint Venture"
            name="Relation"
            annot_field_key="<feff00530065006300740032005f004a006f0069006e007400560065006e0074007500720065005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>Place of Incorporation or Establishment in the United States</h3>
        <fieldset>
          <Input
            label="Establishment"
            name="Establishment"
            annot_field_key="<feff004c0069006e006500320061005f0054007900700065006f00660042007500730069006e006500730073005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>Date of incorporation or establishment (mm/dd/yyyy)</h3>
        <fieldset>
          <Input
            inputType="date"
            label="Date of Incorporation"
            name="Date of Incorporation"
            annot_field_key="<feff004c0069006e006500320062005f004400610074006500450073007400610062006c00690073006800650064005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <InputTable
          columns={[
            "Name (First/MI/Last)",
            "Nationality",
            "Immigration Status",
            "Percent of Ownership"
          ]}
          rows={[
            [
              {
                id: "<feff004e0041004d0045005f004c0069006e00650031005b0030005d>",
                label: "First, Middle and Last Name",
                name: "First, Middle and Last Name"
              },
              {
                id:
                  "<feff004e006100740069006f006e0061006c006900740079005f006c0069006e00650031005b0030005d>",
                label: "Nationality",
                name: "Nationality"
              },
              {
                id:
                  "<feff0049006d006d006900670072006100740069006f006e005300740061007400750073005f004c0069006e00650031005b0030005d>",
                label: "Immigration Status",
                name: "Immigration Status"
              },
              {
                id:
                  "<feff00500065007200630065006e0074004f0077006e006500720073006800690070005f004c0069006e00650031005b0030005d>",
                label: "Percent of Ownership",
                name: "Percent of Ownership"
              }
            ],
            [
              {
                id: "<feff004e0041004d0045005f004c0069006e00650032005b0030005d>",
                label: "First, Middle and Last Name",
                name: "First, Middle and Last Name2"
              },
              {
                id:
                  "<feff004e006100740069006f006e0061006c006900740079005f006c0069006e00650032005b0030005d>",
                label: "Nationality",
                name: "Nationality2"
              },
              {
                id:
                  "<feff0049006d006d006900670072006100740069006f006e005300740061007400750073005f004c0069006e00650032005b0030005d>",
                label: "Immigration Status",
                name: "Immigration Status2"
              },
              {
                id:
                  "<feff00500065007200630065006e0074004f0077006e006500720073006800690070005f004c0069006e00650032005b0030005d>",
                label: "Percent of Ownership",
                name: "Percent of Ownership2"
              }
            ],
            [
              {
                id: "<feff004e0041004d0045005f004c0069006e00650033005b0030005d>",
                label: "First, Middle and Last Name",
                name: "First, Middle and Last Name3"
              },
              {
                id:
                  "<feff004e006100740069006f006e0061006c006900740079005f006c0069006e00650033005b0030005d>",
                label: "Nationality",
                name: "Nationality3"
              },
              {
                id:
                  "<feff0049006d006d006900670072006100740069006f006e005300740061007400750073005f004c0069006e00650033005b0030005d>",
                label: "Immigration Status",
                name: "Immigration Status3"
              },
              {
                id:
                  "<feff00500065007200630065006e0074004f0077006e006500720073006800690070005f004c0069006e00650033005b0030005d>",
                label: "Percent of Ownership",
                name: "Percent of Ownership3"
              }
            ],
            [
              {
                id: "<feff004e0041004d0045005f004c0069006e00650034005b0030005d>",
                label: "First, Middle and Last Name",
                name: "First, Middle and Last Name4"
              },
              {
                id:
                  "<feff004e006100740069006f006e0061006c006900740079005f006c0069006e00650034005b0030005d>",
                label: "Nationality",
                name: "Nationality4"
              },
              {
                id:
                  "<feff0049006d006d006900670072006100740069006f006e005300740061007400750073005f004c0069006e00650034005b0030005d>",
                label: "Immigration Status",
                name: "Immigration Status4"
              },
              {
                id:
                  "<feff00500065007200630065006e0074004f0077006e006500720073006800690070005f004c0069006e00650034005b0030005d>",
                label: "Percent of Ownership",
                name: "Percent of Ownership4"
              }
            ],
            [
              {
                id: "<feff004e0041004d00450035005b0030005d>",
                label: "First, Middle and Last Name",
                name: "First, Middle and Last Name5"
              },
              {
                id:
                  "<feff004e006100740069006f006e0061006c006900740079005f006c0069006e00650035005b0030005d>",
                label: "Nationality",
                name: "Nationality5"
              },
              {
                id:
                  "<feff0049006d006d006900670072006100740069006f006e005300740061007400750073005f004c0069006e00650035005b0030005d>",
                label: "Immigration Status",
                name: "Immigration Status5"
              },
              {
                id:
                  "<feff00500065007200630065006e0074004f0077006e006500720073006800690070005f004c0069006e00650035005b0030005d>",
                label: "Percent of Ownership",
                name: "Percent of Ownership5"
              }
            ],
            [
              {
                id: "<feff004e0041004d0045005f004c0069006e00650036005b0030005d>",
                label: "First, Middle and Last Name",
                name: "First, Middle and Last Name6"
              },
              {
                id:
                  "<feff004e006100740069006f006e0061006c006900740079005f006c0069006e00650036005b0030005d>",
                label: "Nationality",
                name: "Nationality6"
              },
              {
                id:
                  "<feff0049006d006d006900670072006100740069006f006e005300740061007400750073005f004c0069006e00650036005b0030005d>",
                label: "Immigration Status",
                name: "Immigration Status6"
              },
              {
                id:
                  "<feff00500065007200630065006e0074004f0077006e006500720073006800690070005f004c0069006e00650036005b0030005d>",
                label: "Percent of Ownership",
                name: "Percent of Ownership6"
              }
            ],
            [
              {
                id: "<feff004e0041004d0045005f004c0069006e00650037005b0030005d>",
                label: "First, Middle and Last Name",
                name: "First, Middle and Last Name7"
              },
              {
                id:
                  "<feff004e006100740069006f006e0061006c006900740079005f006c0069006e00650037005b0030005d>",
                label: "Nationality",
                name: "Nationality7"
              },
              {
                id:
                  "<feff0049006d006d006900670072006100740069006f006e005300740061007400750073005f004c0069006e00650037005b0030005d>",
                label: "Immigration Status",
                name: "Immigration Status7"
              },
              {
                id:
                  "<feff00500065007200630065006e0074004f0077006e006500720073006800690070005f004c0069006e00650037005b0030005d>",
                label: "Percent of Ownership",
                name: "Percent of Ownership7"
              }
            ]
          ]}
        />
        <h3>Assets</h3>
        <fieldset>
          <Input
            label="Assets"
            name="Assets"
            annot_field_key="<feff00500035004c0069006e0065005f005900650061007200450073007400610062006c00690073006800650064005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>Net Worth</h3>
        <fieldset>
          <Input
            label="Net Worth"
            name="Net Worth"
            annot_field_key="<feff004c0069006e006500320064005f00470072006f007300730041006e006e00750061006c0049006e0063006f006d0065005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>Net Annual Income</h3>
        <fieldset>
          <Input
            label="Net Annual Income"
            name="Net Annual Income"
            annot_field_key="<feff004c0069006e006500320065005f004e006500740041006e006e00750061006c0049006e0063006f006d0065005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>Staff in the United States</h3>
        <fieldset>
          <h4>
            How many executive and managerial employees does the petitioner have
            who are nationals of the treaty country in either E, L, or H
            nonimmigrant status?
        </h4>
          <Input
            label="Employees"
            name="Employees"
            annot_field_key="<feff0053007500700045004c0069006e006500370061005f0048006f0077004d0061006e0079005b0030005d>"
            doc="i-129"
          />
          <h4>
            How many persons with special qualifications does the petitioner
            employ who are in either E, L, or H nonimmigrant status?
        </h4>
          <Input
            label="Persons"
            name="Persons"
            annot_field_key="<feff0053007500700045004c0069006e006500370062005f0048006f0077004d0061006e0079005b0030005d>"
            doc="i-129"
          />
          <h4>
            Provide the total number of employees in executive and managerial
            positions in the United States.
        </h4>
          <Input
            label="Total employees"
            name="Total employees"
            annot_field_key="<feff0053007500700045004c0069006e006500370063005f0054006f00740061006c004e0075006d006200650072005b0030005d>"
            doc="i-129"
          />
          <h4>
            Provide the total number of positions in the United States that
            require persons with special qualifications.
        </h4>
          <Input
            label="Positions in United States"
            name="Positions in United States"
            annot_field_key="<feff0053007500700045004c0069006e006500370064005f0054006f00740061006c004e0075006d006200650072005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>
          If the petitioner is attempting to qualify the employee as an executive
          or manager, provide the total number of employees he or she will
          supervise. Or, if the petitioner is attempting to qualify the employee
          based on special qualifications, explain why the special qualifications
          are essential to the successful or efficient operation of the treaty
          enterprise.
      </h3>
        <fieldset>
          <Input
            inputType="textarea"
            label="Explanation"
            name="Explanation"
            annot_field_key="<feff004c0069006e00650033005f004a006f0062004400650073006300720069007000740069006f006e005b0030005d>"
            doc="i-129"
          />
        </fieldset>
      </div>
    </WithValues>
  );
};

export default Section2;
