import React from "react";
import Input from "../../Input";
import WithValues from '../../../WithValues'
import StateSelect from "../../StateSelect";

const Section1 = () => {

  return (
    <WithValues doc={doc}>
      <div>
        <h2>
          Section 1. Information About the Employer Outside the United States (if
          any)
      </h2>
        <h3>Employer's Name</h3>
        <fieldset>
          <Input
            label="Employer"
            name="Employer"
            annot_field_key="<feff004c0069006e00650033005f0043006f006d00700061006e0079006f0072004f00720067004e0061006d0065005b0031005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>Total Number of Employees</h3>
        <fieldset>
          <Input
            label="Number of Employees"
            name="Number of Employees"
            annot_field_key="<feff004c0069006e00650032005f00540074006c004e0075006d006200650072006f00660045006d0070006c006f0079006500650073005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>Employer's Address</h3>
        <fieldset>
          <Input
            label="Street Number and Name"
            name="Street Number and Name"
            annot_field_key="<feff004c0069006e006500370062005f005300740072006500650074004e0075006d006200650072004e0061006d0065005b0032005d>"
            doc="i-129"
          />
          <Input
            inputType="checkbox"
            label="Suite"
            name="Suite"
            annot_field_key="<feff0053006500630031004c0069006e00650033005f0055006e00690074005b0031005d>"
            doc="i-129"
          />
          <Input
            inputType="checkbox"
            label="Apartment"
            name="Apartment"
            annot_field_key="<feff0053006500630031004c0069006e00650033005f0055006e00690074005b0030005d>"
            doc="i-129"
          />
          <Input
            inputType="checkbox"
            label="Floor"
            name="Floor"
            annot_field_key="<feff0053006500630031004c0069006e00650033005f0055006e00690074005b0032005d>"
            doc="i-129"
          />
          <Input
            label="Number"
            name="Number"
            annot_field_key="<feff0053006500630031004c0069006e00650033005f0041007000740053007400650046006c0072004e0075006d006200650072005b0030005d>"
            doc="i-129"
          />
          <Input
            label="City or Town"
            name="City or Town"
            annot_field_key="<feff004c0069006e0065005f00430069007400790054006f0077006e005b0032005d>"
            doc="i-129"
          />
          <StateSelect
            annot_field_key="<feff00530031005f004c0069006e00650033005f00530074006100740065005b0030005d>"
            doc="i-129"
          />
          <Input
            label="ZIP Code"
            name="ZIP Code"
            annot_field_key="<feff00530031005f004c0069006e00650033005f005a006900700043006f00640065005b0030005d>"
            doc="i-129"
          />
          <Input
            label="Province"
            name="Province"
            annot_field_key="<feff00530031005f004c0069006e00650033005f00500072006f00760069006e00630065005b0030005d>"
            doc="i-129"
          />
          <Input
            label="Postal code"
            name="Postal code"
            annot_field_key="<feff00530031005f004c0069006e00650033005f0050006f007300740061006c0043006f00640065005b0030005d>"
            doc="i-129"
          />
          <Input
            label="country"
            name="country"
            annot_field_key="<feff00530031005f004c0069006e00650033005f0043006f0075006e007400720079005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>Principal Product, Merchandise or Service</h3>
        <fieldset>
          <Input
            inputType="textarea"
            label="Product"
            name="Product"
            annot_field_key="<feff004c0069006e00650034005f004400650073006300720069007000740069006f006e005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>Employee's Position - Title, duties and number of years employed</h3>
        <fieldset>
          <Input
            inputType="textarea"
            label="Position"
            name="Position"
            annot_field_key="<feff004c0069006e00650035005f0045006d0070006c006f0079006500650050006f0073006900740069006f006e004400650073006300720069007000740069006f006e005b0030005d>"
            doc="i-129"
          />
        </fieldset>
      </div>
    </WithValues>
  );
};

export default Section1;
