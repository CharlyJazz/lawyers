import React from "react";
import Input from "../../Input";
import WithValues from '../../../WithValues'

const Section3 = () => {

  return (
    <WithValues doc={doc}>
      <div>
        <h2>Section 3. Complete If Filing for an E-1 Treaty Trader</h2>
        <h3>Total Annual Gross Trade/Business of the U.S. company</h3>
        <fieldset>
          <Input
            label="Annual Gross"
            name="Annual Gross"
            annot_field_key="<feff00450053006500630033004c0069006e00650031005f00540074006c0041006e006e00750061006c00470072006f00730073005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>For Year Ending (yyyy)</h3>
        <fieldset>
          <Input
            label="Year"
            name="Year"
            annot_field_key="<feff00450053006500630033004c0069006e00650032005f00590065006100720045006e00640069006e0067005b0030005d>"
            doc="i-129"
          />
        </fieldset>
        <h3>
          Percent of total gross trade between the United States and the treaty
          trader country.
      </h3>
        <fieldset>
          <Input
            label="Total Gross"
            name="Total Gross"
            annot_field_key="<feff00450053006500630033004c0069006e00650033005f00500065007200630065006e0074004f006600540074006c00470072006f00730073005b0030005d>"
            doc="i-129"
          />
        </fieldset>
      </div>
    </WithValues>
  );
};

export default Section3;
