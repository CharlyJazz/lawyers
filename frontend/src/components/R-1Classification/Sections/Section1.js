import React from "react";
import InputTable from "../../InputTable";
import WithValues from '../../../WithValues'
import Input from "../../Input";

const Section1 = () => {

  return (
    <WithValues doc={doc}>
    <div>
      <h2>
        Section 1. Complete This Section If You Are Filing For An R-1 Religious
        Worker
      </h2>
      <h3>
        Employer Attestation Provide the following information about the
        petitioner:
      </h3>
      <h3>
        1.a. Number of members of the petitioner's religious organization?
      </h3>
      <fieldset>
        <Input
          name="1.a. Number of members of the petitioner's religious organization?"
          label="1.a. Number of members of the petitioner's religious organization?"
          doc="i-129"
          annot_field_key="<feff00540074006c004e0075006d0062006500720073006f00660057006f0072006b00650072005b0031005d>"
          type="Number"
        />
      </fieldset>
      <h3>
        1.b. Number of employees working at the same location where the
        beneficiary will be employed?
      </h3>
      <fieldset>
        <Input
          name="1.b. Number of employees working at the same location where the beneficiary will be employed?"
          label="1.b. Number of employees working at the same location where the beneficiary will be employed?"
          doc="i-129"
          annot_field_key="<feff00540074006c004e0075006d0062006500720073006f00660057006f0072006b00650072005b0032005d>"
          type="Number"
        />
      </fieldset>
      <h3>
        1.c. Number of aliens holding special immigrant or nonimmigrant
        religious worker status currently employed or employed within the past
        five years?
      </h3>
      <fieldset>
        <Input
          name="1.c. Number of aliens holding special immigrant or nonimmigrant religious worker status currently
          employed or employed within the past five years?"
          label="1.c. Number of aliens holding special immigrant or nonimmigrant religious worker status currently
          employed or employed within the past five years?"
          doc="i-129"
          annot_field_key="<feff00540074006c004e0075006d0062006500720073006f00660057006f0072006b00650072005b0033005d>"
          type="Number"
        />
      </fieldset>
      <h3>
        1.d. Number of special immigrant religious worker petition(s) (I-360)
        and nonimmigrant religious worker petition(s) (I-129) filed by the
        petitioner within the past five years?
      </h3>
      <fieldset>
        <Input
          name="1.d. Number of special immigrant religious worker petition(s) (I-360) and nonimmigrant religious
            worker petition(s) (I-129) filed by the petitioner within the past five years?"
          label="1.d. Number of special immigrant religious worker petition(s) (I-360) and nonimmigrant religious
            worker petition(s) (I-129) filed by the petitioner within the past five years?"
          doc="i-129"
          annot_field_key="<feff00540074006c004e0075006d0062006500720073006f00660057006f0072006b00650072005b0034005d>"
          type="Number"
        />
      </fieldset>
      <h3>
        2. Has the beneficiary or any of the beneficiary's dependent family
        members previously been admitted to the United States for a period of
        stay in the R visa classification in the last five years? If yes,
        complete the spaces below. List the beneficiary and any dependent family
        member’s prior periods of stay in the R visa classification in the
        United States in the last five years. Please be sure to list only those
        periods in which the beneficiary and/or family members were actually in
        the United States in an R classification. NOTE: Submit photocopies of
        Forms I-94 (Arrival-Departure Record), I-797 (Notice of Action), and/or
        other USCIS documents identifying these periods of stay in the R visa
        classification(s). If more space is needed, provide the information in
        Part 9. of Form I-129.
      </h3>
      <fieldset>
        <Input
          name="2. Yes"
          label="2. Yes"
          annot_field_key="<feff005200310053006500630031004c0069006e00650032005b0031005d>"
          doc="i-129"
          type="checkbox"
        />
        <Input
          name="2. NO"
          label="2. NO"
          annot_field_key="<feff005200310053006500630031004c0069006e00650032005b0030005d>"
          doc="i-129"
          type="checkbox"
        />
        <InputTable
          columns={[
            "Alien or Dependent Family Member's Name",
            "Period of Stay (mm/dd/yyyy) From",
            "Period of Stay (mm/dd/yyyy) To"
          ]}
          rows={[
            [
              {
                id:
                  "<feff00460061006d0069006c0079004d0065006d006200650072004e0061006d0065005f004c0069006e00650031005b0030005d>",
                label: "Name",
                name: "Name"
              },
              {
                id: "<feff00460072006f006d00440061007400650031005b0030005d>",
                label: "From",
                name: "From"
              },
              {
                id: "<feff0054006f00440061007400650031005b0030005d>",
                label: "To",
                name: "To"
              }
            ],
            [
              {
                id:
                  "<feff00460061006d0069006c0079004d0065006d006200650072004e0061006d00650032005b0030005d>",
                label: "Name",
                name: "Name2"
              },
              {
                id: "<feff00460072006f006d00440061007400650032005b0030005d>",
                label: "From",
                name: "From2"
              },
              {
                id: "<feff0054006f00440061007400650032005b0030005d>",
                label: "To",
                name: "To2"
              }
            ],
            [
              {
                id:
                  "<feff00460061006d0069006c0079004d0065006d006200650072004e0061006d00650033005b0030005d>",
                label: "Name",
                name: "Name3"
              },
              {
                id: "<feff00460072006f006d00440061007400650033005b0030005d>",
                label: "From",
                name: "From3"
              },
              {
                id: "<feff0054006f00440061007400650033005b0030005d>",
                label: "To",
                name: "To3"
              }
            ],
            [
              {
                id:
                  "<feff00460061006d0069006c0079004d0065006d006200650072004e0061006d00650034005b0030005d>",
                label: "Name",
                name: "Name4"
              },
              {
                id: "<feff00460072006f006d00440061007400650034005b0030005d>",
                label: "From",
                name: "From4"
              },
              {
                id: "<feff0054006f00440061007400650034005b0030005d>",
                label: "To",
                name: "To4"
              }
            ],
            [
              {
                id:
                  "<feff00460061006d0069006c0079004d0065006d006200650072004e0061006d00650035005b0030005d>",
                label: "Name",
                name: "Name5"
              },
              {
                id: "<feff00460072006f006d00440061007400650035005b0030005d>",
                label: "From",
                name: "From5"
              },
              {
                id: "<feff0054006f00440061007400650035005b0030005d>",
                label: "To",
                name: "To5"
              }
            ],
            [
              {
                id:
                  "<feff00460061006d0069006c0079004d0065006d006200650072004e0061006d00650036005b0030005d>",
                label: "Name",
                name: "Name6"
              },
              {
                id: "<feff00460072006f006d00440061007400650036005b0030005d>",
                label: "From",
                name: "From6"
              },
              {
                id: "<feff0054006f00440061007400650036005b0030005d>",
                label: "To",
                name: "To6"
              }
            ],
            [
              {
                id:
                  "<feff00460061006d0069006c0079004d0065006d006200650072004e0061006d00650037005b0030005d>",
                label: "Name",
                name: "Name7"
              },
              {
                id: "<feff00460072006f006d00440061007400650037005b0030005d>",
                label: "From",
                name: "From7"
              },
              {
                id: "<feff0054006f00440061007400650037005b0030005d>",
                label: "To",
                name: "To7"
              }
            ]
          ]}
        />
      </fieldset>
      <h3>
        3.Provide a summary of the type of responsibilities of those employees
        who work at the same location where the beneficiary will be employed. If
        additional space is needed, provide the information on additional
        sheet(s) of paper.
      </h3>
      <fieldset>
        <InputTable
          doc="i-129"
          annot_field_key=""
          columns={[
            "Position",
            "Summary of the Type of Responsibilities for That Position"
          ]}
          rows={[
            [
              {
                id: "<feff0050006f0073006900740069006f006e0031005b0030005d>",
                label: "Position",
                name: "Position"
              },
              {
                id: "<feff00530075006d006d0061007200790031005b0030005d>",
                label: "Summary",
                name: "Summary"
              }
            ],
            [
              {
                id: "<feff0050006f0073006900740069006f006e0032005b0030005d>",
                label: "Position",
                name: "Position2"
              },
              {
                id: "<feff00530075006d006d0061007200790032005b0030005d>",
                label: "Summary",
                name: "Summary2"
              }
            ],
            [
              {
                id: "<feff0050006f0073006900740069006f006e0033005b0030005d>",
                label: "Position",
                name: "Position3"
              },
              {
                id: "<feff00530075006d006d0061007200790033005b0030005d>",
                label: "Summary",
                name: "Summary3"
              }
            ],
            [
              {
                id: "<feff0050006f0073006900740069006f006e0034005b0030005d>",
                label: "Position",
                name: "Position4"
              },
              {
                id: "<feff00530075006d006d0061007200790034005b0030005d>",
                label: "Summary",
                name: "Summary4"
              }
            ],
            [
              {
                id: "<feff0050006f0073006900740069006f006e0035005b0030005d>",
                label: "Position",
                name: "Position5"
              },
              {
                id: "<feff00530075006d006d0061007200790035005b0030005d>",
                label: "Summary",
                name: "Summary5"
              }
            ],
            [
              {
                id: "<feff0050006f0073006900740069006f006e0036005b0030005d>",
                label: "Position",
                name: "Position6"
              },
              {
                id: "<feff00530075006d006d0061007200790036005b0030005d>",
                label: "Summary",
                name: "Summary6"
              }
            ]
          ]}
        />
      </fieldset>
      <h3>
        4.Summary of the Type of Responsibilities for That Positi onDescribe the
        relationship, if any, between the religious organization in the United
        States and the organization abroad of whi chthe beneficiary is a member.
      </h3>
      <fieldset>
        <Input
          name="4.Summary of the 
          Type of Responsibilities for That Positi onDescribe the relationship, 
          if any, between the religious organization in the United States and the organization abroad of whi  chthe beneficiary is a 
          member."
          label="4.Summary of the 
          Type of Responsibilities for That Positi onDescribe the relationship, 
          if any, between the religious organization in the United States and the organization abroad of whi  chthe beneficiary is a 
          member."
          doc="i-129"
          annot_field_key="<feff004c0069006e00650033005f004a006f0062004400650073006300720069007000740069006f006e005b0039005d>"
          inputType="textarea"
        />
      </fieldset>
      <h3>
        Provide the following information about the prospective employment:
      </h3>
      <h3>5.a. Title of position offered.</h3>
      <fieldset>
        <Input
          name="  5.a. Title of position offered."
          label="  5.a. Title of position offered."
          doc="i-129"
          annot_field_key="<feff004c0069006e00650033005f004a006f0062004400650073006300720069007000740069006f006e005b00310032005d>"
          inputType="textarea"
        />
      </fieldset>
      <h3>
        5.b. Detailed description of the beneficiary's proposed daily duties.
      </h3>
      <fieldset>
        <Input
          name="5.b. Detailed description of the beneficiary's proposed daily duties."
          label="5.b. Detailed description of the beneficiary's proposed daily duties."
          doc="i-129"
          annot_field_key="<feff004c0069006e00650033005f004a006f0062004400650073006300720069007000740069006f006e005b00310030005d>"
          inputType="textarea"
        />
      </fieldset>
      <h3>
        5.c. Description of the beneficiary's qualifications for position
        offered.
      </h3>
      <fieldset>
        <Input
          name="5.c. Description of the beneficiary's qualifications for position offered."
          label="5.c. Description of the beneficiary's qualifications for position offered."
          doc="i-129"
          annot_field_key="<feff004c0069006e00650033005f004a006f0062004400650073006300720069007000740069006f006e005b00310031005d>"
          inputType="textarea"
        />
      </fieldset>
      <h3>
        5.d. Description of the proposed salaried compensation or non-salaried
        compensation. If the beneficiary will be self-supporting, the petitioner
        must submit documentation establishing that the position the beneficiary
        will hold is part of an established program for temporary, uncompensated
        missionary work, which is part of a broader international program of
        missionary work sponsored by the denomination.
      </h3>
      <fieldset>
        <Input
          name="5.d. Description of the proposed salaried compensation or non-salaried compensation. If the beneficiary will be self-supporting, the
          petitioner must submit documentation establishing that the position the beneficiary will hold is part of an established program
          for temporary, uncompensated missionary work, which is part of a broader international program of missionary work sponsored
          by the denomination."
          label="5.d. Description of the proposed salaried compensation or non-salaried compensation. If the beneficiary will be self-supporting, the
          petitioner must submit documentation establishing that the position the beneficiary will hold is part of an established program
          for temporary, uncompensated missionary work, which is part of a broader international program of missionary work sponsored
          by the denomination."
          doc="i-129"
          annot_field_key="<feff004c0069006e00650033005f004a006f0062004400650073006300720069007000740069006f006e005b00310033005d>"
          inputType="textarea"
        />
      </fieldset>
      <h3>
        5.e. List of the address(es) or location(s) where the beneficiary will
        be working.
      </h3>
      <fieldset>
        <Input
          name="5.e. List of the address(es) or location(s) where the beneficiary will be working."
          label="5.e. List of the address(es) or location(s) where the beneficiary will be working."
          doc="i-129"
          annot_field_key="<feff004c0069006e00650033005f004a006f0062004400650073006300720069007000740069006f006e005b00310034005d>"
          inputType="textarea"
        />
      </fieldset>
      <h3>
        Petitioner Attestations Does the petitioner attest to all of the
        requirements described in Item Numbers 6. - 12. below?
      </h3>
      <h3>
        6. The petitioner is a bona fide non-profit religious organization or a
        bona fide organization that is affiliated with the religious
        denomination and is tax-exempt as described in section 501(c)(3) of the
        Internal Revenue Code of 1986, subsequent amendment, or equivalent
        sections of prior enactments of the Internal Revenue Code. If the
        petitioner is affiliated with the religious denomination, complete the
        Religious Denomination Certification included in this supplement.
      </h3>
      <fieldset>
        <Input
          name="6. Yes"
          label="6. Yes"
          doc="i-129"
          annot_field_key="<feff005200310053006500630031004c0069006e006500360061005b0031005d>"
          type="checkbox"
        />
        <Input
          name="6. No. If no, type or print your explanation below and if needed, go to Part 9. of Form I-129."
          label="6. No. If no, type or print your explanation below and if needed, go to Part 9. of Form I-129."
          doc="i-129"
          annot_field_key="<feff005200310053006500630031004c0069006e0065003600610031005f004500780070006c0061006e006100740069006f006e005b0030005d>"
          type="checkbox"
        />
        <Input
          name="6. no."
          label="6. no."
          doc="i-129"
          annot_field_key="<feff005200310053006500630031004c0069006e006500360061005b0030005d>"
          inputType="textarea"
        />
      </fieldset>
      <h3>
        7. The petitioner is willing and able to provide salaried or
        non-salaried compensation to the beneficiary. If the beneficiary will be
        self-supporting, the petitioner must submit documentation establishing
        that the position the beneficiary will hold is part of an established
        program for temporary, uncompensated missionary work, which is part of a
        broader international program of missionary work sponsored by the
        denomination.
      </h3>
      <fieldset>
        <Input
          name="7. Yes"
          label="7. Yes"
          doc="i-129"
          annot_field_key="<feff005200310053006500630031004c0069006e006500370061005b0030005d>"
          type="checkbox"
        />
        <Input
          name="7. No. If no, type or print your explanation below and if needed, go to Part 9. of Form I-129."
          label="7. No. If no, type or print your explanation below and if needed, go to Part 9. of Form I-129."
          doc="i-129"
          annot_field_key="<feff005200310053006500630031004c0069006e0065003700610031005f004500780070006c0061006e006100740069006f006e005b0030005d>"
          type="checkbox"
        />
        <Input
          name="7. no."
          label="7. no."
          doc="i-129"
          annot_field_key="<feff005200310053006500630031004c0069006e006500370061005b0031005d>"
          inputType="textarea"
        />
      </fieldset>
      <h3>
        8. If the beneficiary worked in the United States in an R-1 status
        during the 2 years immediately before the petition was filed, the
        beneficiary received verifiable salaried or non-salaried compensation,
        or provided uncompensated self-support.
      </h3>
      <fieldset>
        <Input
          name="8. Yes"
          label="8. Yes"
          doc="i-129"
          annot_field_key="<feff005200310053006500630031004c0069006e006500380061005b0030005d>"
          type="checkbox"
        />
        <Input
          name="8. No. If no, type or print your explanation below and if needed, go to Part 9. of Form I-129."
          label="8. No. If no, type or print your explanation below and if needed, go to Part 9. of Form I-129."
          doc="i-129"
          annot_field_key="<feff005200310053006500630031004c0069006e0065003800610031005f004500780070006c0061006e006100740069006f006e005b0030005d>"
          type="checkbox"
        />
        <Input
          name="8. no."
          label="8. no."
          doc="i-129"
          annot_field_key="<feff005200310053006500630031004c0069006e006500380061005b0031005d>"
          inputType="textarea"
        />
      </fieldset>
      <h3>
        9. If the position is not a religious vocation, the beneficiary will not
        engage in secular employment, and the petitioner will provide salaried
        or non-salaried compensation. If the position is a traditionally
        uncompensated and not a religious vocation, the beneficiary will not
        engage in secular employment, and the beneficiary will provide
        self-support.
      </h3>
      <fieldset>
        <Input
          name="9. Yes"
          label="9. Yes"
          doc="i-129"
          annot_field_key="<feff005200310053006500630031004c0069006e006500390061005b0030005d>"
          type="checkbox"
        />
        <Input
          name="9. No. If no, type or print your explanation below and if needed, go to Part 9. of Form I-129."
          label="9. No. If no, type or print your explanation below and if needed, go to Part 9. of Form I-129."
          doc="i-129"
          annot_field_key="<feff005200310053006500630031004c0069006e0065003900610031005f004500780070006c0061006e006100740069006f006e005b0030005d>"
          type="checkbox"
        />
        <Input
          name="9. no."
          label="9. no."
          doc="i-129"
          annot_field_key="<feff005200310053006500630031004c0069006e006500390061005b0031005d>"
          inputType="textarea"
        />
      </fieldset>
      <h3>
        10. The offered position requires at least 20 hours of work per week. If
        the offered position at the petitioning organization requires fewer than
        20 hours per week, the compensated service for another religious
        organization and the compensated service at the petitioning organization
        will total 20 hours per week. If the beneficiary will be
        self-supporting, the petitioner must submit documentation establishing
        that the position the beneficiary will hold is part of an established
        program for temporary, uncompensated missionary work, which is part of a
        broader international program of missionary work sponsored by the
        denomination.
      </h3>
      <fieldset>
        <Input
          name="10. Yes"
          label="10. Yes"
          doc="i-129"
          annot_field_key="<feff005200310053006500630031004c0069006e0065003100300061005b0030005d>"
          type="checkbox"
        />
        <Input
          name="10. No. If no, type or print your explanation below and if needed, go to Part 9. of Form I-129."
          label="10. No. If no, type or print your explanation below and if needed, go to Part 9. of Form I-129."
          doc="i-129"
          annot_field_key="<feff005200310053006500630031004c0069006e00650031003000610031005f004500780070006c0061006e006100740069006f006e005b0030005d>"
          type="checkbox"
        />
        <Input
          name="10. no."
          label="10. no."
          doc="i-129"
          annot_field_key="<feff005200310053006500630031004c0069006e0065003100300061005b0031005d>"
          inputType="textarea"
        />
      </fieldset>
      <h3>
        11. The beneficiary has been a member of the petitioner's denomination
        for at least two years immediately before Form I-129 was filed and is
        otherwise qualified to perform the duties of the offered position.
      </h3>
      <fieldset>
        <Input
          name="11. Yes"
          label="11. Yes"
          doc="i-129"
          annot_field_key="<feff005200310053006500630031004c0069006e0065003100310061005f005900650073005b0030005d>"
          type="checkbox"
        />
        <Input
          name="11. No. If no, type or print your explanation below and if needed, go to Part 9. of Form I-129."
          label="11. No. If no, type or print your explanation below and if needed, go to Part 9. of Form I-129."
          doc="i-129"
          annot_field_key="<feff005200310053006500630031004c0069006e0065003100310061005f004e006f005b0030005d>"
          type="checkbox"
        />
        <Input
          name="11. no."
          label="11. no."
          doc="i-129"
          annot_field_key="<feff005200310053006500630031004c0069006e00650031003100610031005f004500780070006c0061006e006100740069006f006e005b0030005d>"
          inputType="textarea"
        />
      </fieldset>
      <h3>
        12. The petitioner will notify USCIS within 14 days if an R-1 alien is
        working less than the required number of hours or has been released from
        or has otherwise terminated employment before the expiration of a period
        of authorized R-1 stay.
      </h3>
      <fieldset>
        <Input
          name="12. Yes"
          label="12. Yes"
          doc="i-129"
          annot_field_key="<feff005200310053006500630031004c0069006e0065003100320061005b0030005d>"
          type="checkbox"
        />
        <Input
          name="12. No. If no, type or print your explanation below and if needed, go to Part 9. of Form I-129."
          label="12. No. If no, type or print your explanation below and if needed, go to Part 9. of Form I-129."
          doc="i-129"
          annot_field_key="<feff005200310053006500630031004c0069006e0065003100310061005f004e006f005b0030005d>"
          type="checkbox"
        />
        <Input
          name="12. no."
          label="12. no."
          doc="i-129"
          inputType="textarea"
          annot_field_key="<feff005200310053006500630031004c0069006e0065003100320061005b0031005d>"
        />
      </fieldset>
      <h3>
        Attestation I certify, under penalty of perjury, that the contents of
        this attestation and the evidence submitted with it are true and
        correct.
      </h3>
      <fieldset>
        <Input
          name="Name of Petitioner"
          label="Name of Petitioner"
          doc="i-129"
          annot_field_key="<feff005000610072007400310034005f00500072006500700061007200650072005000720069006e007400650064004e0061006d0065005b0030005d>"
        />
        <Input
          name="Title"
          label="Title"
          doc="i-129"
          annot_field_key="<feff004c0069006e00650035005f004a006f0062005400690074006c0065005b0030005d>"
        />
        <Input
          name="Employer or Organization Name"
          label="Employer or Organization Name"
          doc="i-129"
          annot_field_key="<feff005000610072007400310034005f004600690072006d004e0061006d0065005b0030005d>"
        />
      </fieldset>
      <h3>
        Employer or Organization Address (do not use a post office or private
        mail box)
      </h3>
      <fieldset>
        <Input
          name="Street Number and Name"
          label="Street Number and Name"
          doc="i-129"
          annot_field_key="<feff005000610072007400310034005f005300740072006500650074004e0061006d0065005b0030005d>"
        />
        <Input
          name="Apt."
          label="Apt."
          doc="i-129"
          annot_field_key="<feff0053006500630031005f0055006e00690074005b0032005d>"
          type="checkbox"
        />
        <Input
          name="Ste."
          label="Ste."
          doc="i-129"
          type="checkbox"
          annot_field_key="<feff0053006500630031005f0055006e00690074005b0031005d>"
        />
        <Input
          name="Flr."
          label="Flr."
          doc="i-129"
          type="checkbox"
          annot_field_key="<feff0053006500630031005f0055006e00690074005b0030005d>"
        />
        <Input
          name="Number"
          label="Number"
          doc="i-129"
          annot_field_key="<feff0053006500630031005f0041007000740053007400650046006c0072004e0075006d006200650072005b0030005d>"
          type="Number"
        />
        <Input
          name="City or Town"
          label="City or Town"
          doc="i-129"
          annot_field_key="<feff005000610072007400310034005f0043006900740079005b0030005d>"
        />
        <Input
          name="State"
          label="State"
          doc="i-129"
          annot_field_key="<feff004c0069006e00650032005f00530074006100740065005b0030005d>"
        />
        <Input
          name="ZIP Code"
          label="ZIP Code"
          doc="i-129"
          annot_field_key="<feff005000610072007400310034005f005a006900700043006f00640065005b0030005d>"
        />
        <h3>Employer or Organization's Contact Information</h3>
        <fieldset>
          <Input
            name="Daytime Telephone Number"
            label="Daytime Telephone Number"
            doc="i-129"
            annot_field_key="<feff00500072006500700061007200650072005f00440061007900740069006d006500500068006f006e0065004e0075006d0062006500720031005b0030005d>"
            type="tel"
          />
          <Input
            name="Fax Number"
            label="Fax Number"
            doc="i-129"
            annot_field_key="<feff005000720065007000610072006500720073005f00460061007800500068006f006e0065004e0075006d0062006500720031005b0030005d>"
          />
          <Input
            name="Email Address (if any)"
            label="Email Address (if any)"
            doc="i-129"
            annot_field_key="<feff005000610072007400310034005f0045006d00610069006c0041006400640072006500730073005b0030005d>"
            type="email"
          />
        </fieldset>
      </fieldset>
    </div>
    </WithValues>
  );
};

export default Section1;
