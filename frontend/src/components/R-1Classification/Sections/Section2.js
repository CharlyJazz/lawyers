import React from "react";
import WithValues from '../../../WithValues'
import Input from "../../Input";

const Section2 = () => {

  return (
    <WithValues doc={doc}>
    <div>
      <h3>
        Section 2. This Section Is Required For Petitioners Affiliated With The
        Religious Denomination
      </h3>
      <h3>Religious Denomination Certification</h3>
      <fieldset>
        <Input
          name="I certify, under penalty of perjury, that: Name of Employing Organization"
          label="Name of Employing Organization"
          doc="i-129"
          annot_field_key="<feff0045006d0070006c006f00790069006e0067004f00720067004e0061006d0065005b0030005d>"
        />
        <Input
          name="is affiliated with:
                Name of Religious Denomination and that the attesting organization within the religious denomination is tax-exempt as described in section 501(c)(3) of the Internal
                Revenue Code of 1986 (codified at 26 U.S.C. 501(c)(3)), any subsequent amendment(s), subsequent amendment, or equivalent
                sections of prior enactments of the Internal Revenue Code. The contents of this certification are true and correct to the best of my
                knowledge."
          label="is affiliated with:
                Name of Religious Denomination and that the attesting organization within the religious denomination is tax-exempt as described in section 501(c)(3) of the Internal
                Revenue Code of 1986 (codified at 26 U.S.C. 501(c)(3)), any subsequent amendment(s), subsequent amendment, or equivalent
                sections of prior enactments of the Internal Revenue Code. The contents of this certification are true and correct to the best of my
                knowledge."
          doc="i-129"
          annot_field_key="<feff004e0061006d0065004f006600520065006c006900670069006f0075007300440065006e006f006d0069006e006100740069006f006e005b0030005d>"
        />
        <Input
          name="Name of Authorized Representative of Attesting Organization"
          label="Name of Authorized Representative of Attesting Organization"
          doc="i-129"
          annot_field_key=""
        />
        <Input
          name="Title"
          label="Title"
          doc="i-129"
          annot_field_key="<feff004c0069006e00650035005f004a006f0062005400690074006c0065005b0031005d>"
        />
      </fieldset>
      <h3>
        Attesting Organization Name and Address (do not use a post office or
        private mail box)
      </h3>
      <fieldset>
        <Input
          name="Attesting Organization Name"
          label="Attesting Organization Name"
          doc="i-129"
          annot_field_key="<feff005000610072007400310034005f004600690072006d004e0061006d0065005b0031005d>"
        />
        <Input
          name="Street Number and Name"
          label="Street Number and Name"
          doc="i-129"
          annot_field_key="<feff005000610072007400310034005f005300740072006500650074004e0061006d0065005b0031005d>"
        />
        <Input
          name="Apt."
          label="Apt."
          doc="i-129"
          annot_field_key="<feff004100740074006500730074005f0055006e00690074005b0032005d>"
          type="checkbox"
        />
        <Input
          name="Ste."
          label="Ste."
          doc="i-129"
          type="checkbox"
          annot_field_key="<feff004100740074006500730074005f0055006e00690074005b0031005d>"
        />
        <Input
          name="Flr."
          label="Flr."
          doc="i-129"
          annot_field_key="<feff004100740074006500730074005f0055006e00690074005b0030005d>"
          type="checkbox"
        />
        <Input
          name="Number"
          label="Number"
          doc="i-129"
          type="Number"
          annot_field_key="<feff004100740074006500730074005f0041007000740053007400650046006c0072004e0075006d006200650072005b0030005d>"
        />
        <Input
          name="City or Town"
          label="City or Town"
          doc="i-129"
          annot_field_key="<feff005000610072007400310034005f0043006900740079005b0031005d>"
        />
        <Input
          name="State"
          label="State"
          doc="i-129"
          annot_field_key="<feff004c0069006e00650032005f00530074006100740065005b0031005d>"
        />
        <Input
          name="ZIP Code"
          label="ZIP Code"
          doc="i-129"
          annot_field_key="<feff005000610072007400310034005f005a006900700043006f00640065005b0031005d>"
        />
      </fieldset>
      <h3>Attesting Organization's Contact Information</h3>
      <fieldset>
        <Input
          name="Daytime Telephone Number"
          label="Daytime Telephone Number"
          doc="i-129"
          annot_field_key="<feff00500072006500700061007200650072005f00440061007900740069006d006500500068006f006e0065004e0075006d0062006500720031005b0031005d>"
          type="tel"
        />
        <Input
          name="Fax Number"
          label="Fax Number"
          doc="i-129"
          annot_field_key="<feff005000720065007000610072006500720073005f00460061007800500068006f006e0065004e0075006d0062006500720031005b0031005d>"
        />
        <Input
          name="Email Address (if any)"
          label="Email Address (if any)"
          doc="i-129"
          annot_field_key="<feff005000610072007400310034005f0045006d00610069006c0041006400640072006500730073005b0031005d>"
          type="email"
        />
      </fieldset>
    </div>
    </WithValues>
  );
};

export default Section2;
