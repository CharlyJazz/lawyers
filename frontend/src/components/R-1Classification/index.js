import React from "react";
import "./R1Classification.css";
import Section0 from "./Sections/Section0";
import Section1 from "./Sections/Section1";
import Section2 from "./Sections/Section2";
import { Link, Router } from "@reach/router";

const R1Classifcation = () => {

  return (
    <div>
      <h1>R-1 Classification Supplement to Form I-129</h1>
      <div className="Layout-Form">
        <aside className="Layout-Form-Aside">
          <ul>
            <li>
              <Link to="/r-1-classification">R-1 Classification</Link>
            </li>
            {[1, 2, 3].map(p => (
              <li key={p}>
                <Link to={`section-${p}`}>Section {p}</Link>
              </li>
            ))}
          </ul>
        </aside>
        <section className="Layout-Section">
          <Router>
            <Section0 path="/" />
            <Section1 path="section-1" />
            <Section2 path="section-2" />
          </Router>
        </section>
      </div>
    </div>
  );
};

export default R1Classifcation;
