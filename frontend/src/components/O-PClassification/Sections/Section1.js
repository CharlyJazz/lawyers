import React from "react";
import Input from "../../Input";
import StateSelect from "../../StateSelect";
import WithValues from '../../../WithValues'

const Section1 = () => {

  return (
    <WithValues doc={doc}>
    <div>
      <h2>
        Section 1. Complete This Section if Filing for O or P Classification
      </h2>
      <h3>1. Name of the Petitioner</h3>
      <fieldset>
        <Input
          name="1.
          Name of the Petitioner"
          label="1.
          Name of the Petitioner"
          doc="i-129"
          annot_field_key="<feff004c0069006e00650031005f00460061006d0069006c0079004e0061006d0065005b0037005d>"
        />
      </fieldset>
      <h3>
        Name of the Beneficiary or if this petition includes multiple
        beneficiaries, the total number of beneficiaries included.
      </h3>
      <h3>2.a. Name of the Beneficiary</h3>
      <fieldset>
        <Input
          name="2.a. Name of the Beneficiary"
          label="2.a. Name of the Beneficiary"
          annot_field_key="<feff0048005300750070004c0069006e00650032005f00460061006d0069006c0079004e0061006d0065005b0031005d>"
          doc="i-129"
        />
        <Input
          name="2.b. Provide the total number of beneficiaries:"
          label="2.b. Provide the total number of beneficiaries:"
          doc="i-129"
          annot_field_key="<feff004c0069006e00650032005f00540074006c004e0075006d006200650072006f006600420065006e0065006600690063006900610072006900650073005b0031005d>"
        />
      </fieldset>
      <h3>3. Classification sought (select only one box)</h3>
      <fieldset>
        <Input
          name="3.a. O-1A Alien of extraordinary ability in sciences, education, business or athletics (not including the arts, motion picture or
            television industry)"
          label="3.a. O-1A Alien of extraordinary ability in sciences, education, business or athletics (not including the arts, motion picture or
            television industry)"
          doc="i-129"
          annot_field_key="<feff0061005f004f00310041005b0030005d>"
          type="chechbox"
        />
        <Input
          name="3.b. O-1B Alien of extraordinary ability in the arts or extraordinary achievement in the motion picture or television industry"
          label="3.b. O-1B Alien of extraordinary ability in the arts or extraordinary achievement in the motion picture or television industry"
          doc="i-129"
          annot_field_key="<feff0062005f004f00310042005b0030005d>"
          type="chechbox"
        />
        <Input
          name="3.c. O-2 Accompanying alien who is coming to the United States to assist in the performance of the O-1"
          label="3.c. O-2 Accompanying alien who is coming to the United States to assist in the performance of the O-1"
          doc="i-129"
          annot_field_key="<feff0063005f004f0032005b0030005d>"
          type="chechbox"
        />
        <Input
          name="3.d. P-1 Major League Sports"
          label="3.d. P-1 Major League Sports"
          doc="i-129"
          annot_field_key="<feff0064005f00500031005f006d0061006a006f0072006c00650061006700750065005b0030005d>"
          type="chechbox"
        />
        <Input
          name="3.e. P-1 Athlete or Athletic/Entertainment Group (includes minor league sports not affiliated with Major League Sports)"
          label="3.e. P-1 Athlete or Athletic/Entertainment Group (includes minor league sports not affiliated with Major League Sports)"
          doc="i-129"
          annot_field_key="<feff0065005f00500031005b0030005d>"
          type="chechbox"
        />
        <Input
          name="3.f. P-1S Essential Support Personnel for P-1"
          label="3.f. P-1S Essential Support Personnel for P-1"
          doc="i-129"
          annot_field_key="<feff0066005f005000310053005b0030005d>"
          type="chechbox"
        />
        <Input
          name="3.g. P-2 Artist or entertainer for reciprocal exchange program"
          label="3.g. P-2 Artist or entertainer for reciprocal exchange program"
          doc="i-129"
          annot_field_key="<feff0067005f00500032005b0030005d>"
          type="chechbox"
        />
        <Input
          name="3.h. P-2S Essential Support Personnel for P-2"
          label="3.h. P-2S Essential Support Personnel for P-2"
          doc="i-129"
          annot_field_key="<feff0068005f005000320053005b0030005d>"
          type="chechbox"
        />
        <Input
          name="3.i. P-3 Artist/Entertainer coming to the United States to perform, teach, or coach under a program that is culturally unique"
          label="3.i. P-3 Artist/Entertainer coming to the United States to perform, teach, or coach under a program that is culturally unique"
          doc="i-129"
          annot_field_key="<feff0069005f00500033005b0030005d>"
          type="chechbox"
        />
        <Input
          name="3.j. P-3S Essential Support Personnel for P-3"
          label="3.j. P-3S Essential Support Personnel for P-3"
          doc="i-129"
          annot_field_key="<feff006a005f005000330053005b0030005d>"
          type="chechbox"
        />
      </fieldset>
      <h3>4. Explain the nature of the event.</h3>
      <fieldset>
        <Input
          name="4. Explain the nature of the event."
          label="4. Explain the nature of the event."
          doc="i-129"
          annot_field_key="<feff004c0069006e00650033005f004a006f0062004400650073006300720069007000740069006f006e005b0038005d>"
          inputType="textarea"
        />
      </fieldset>
      <h3>5. Describe the duties to be performed.</h3>
      <fieldset>
        <Input
          name="5. Describe the duties to be performed."
          label="5. Describe the duties to be performed."
          doc="i-129"
          annot_field_key="<feff004c0069006e00650033005f004a006f0062004400650073006300720069007000740069006f006e005b0037005d>"
          inputType="textarea"
        />
      </fieldset>
      <h3>
        6. If filing for an O-2 or P support classification, list dates of the
        beneficiary's prior work experience under the principal O-1 or P alien.
      </h3>
      <fieldset>
        <Input
          name="6. If filing for an O-2 or P support classification, list dates 
          of the beneficiary's prior work experience under the principal O-1 or P alien."
          label="6. If filing for an O-2 or P support classification, list dates 
          of the beneficiary's prior work experience under the principal O-1 or P alien."
          doc="i-129"
          annot_field_key="<feff004c0069006e00650033005f004a006f0062004400650073006300720069007000740069006f006e005b0036005d>"
          inputType="textarea"
        />
      </fieldset>
      <h3>
        7.a. Does any beneficiary in this petition have ownership interest in
        the petitioning organization?
      </h3>
      <fieldset>
        <Input
          name="7.a Yes. If yes, please explain in Item Number 7.b."
          label="7.a Yes. If yes, please explain in Item Number 7.b."
          doc="i-129"
          annot_field_key="<feff004f0061006e006400500053007500700070004c0069006e00650037005b0030005d>"
          type="checkbox"
        />
        <Input
          name="7.a No."
          label="7.a No."
          doc="i-129"
          annot_field_key="<feff004f0061006e006400500053007500700070004c0069006e00650037005b0031005d>"
          type="checkbox"
        />
        <Input
          name="7.b. Explanation"
          label="7.b. Explanation"
          annot_field_key="<feff004c0069006e00650031005f004400750074006900650073005b0031005d>"
          doc="i-129"
          inputType="textarea"
        />
      </fieldset>
      <h3>8. Does an appropriate labor organization exist for the petition?</h3>
      <fieldset>
        <Input
          name="8 Yes"
          label="8 Yes"
          doc="i-129"
          annot_field_key="<feff004f0061006e006400500053007500700070004c0069006e00650037005f005900650073005b0030005d>"
          type="checkbox"
        />
        <Input
          name="8 No. If no, proceed to Part 9. and type or print your explanation."
          label="8 No. If no, proceed to Part 9. and type or print your explanation."
          doc="i-129"
          annot_field_key="<feff004f0061006e006400500053007500700070004c0069006e00650037005f004e006f005b0030005d>"
          type="checkbox"
        />
      </fieldset>
      <h3>
        9. Is the required consultation or written advisory opinion being
        submitted with this petition?
      </h3>
      <fieldset>
        <Input
          name="9 Yes"
          label="9 Yes"
          doc="i-129"
          annot_field_key="<feff004f0061006e006400500053007500700070004c0069006e00650038005b0031005d>"
          type="checkbox"
        />
        <Input
          name="9 No - copy of request attached"
          label="9 No - copy of request attached"
          doc="i-129"
          annot_field_key="<feff004f0061006e006400500053007500700070004c0069006e00650038005b0030005d>"
          type="checkbox"
        />
        <Input
          name="9 N/A"
          label="9 N/A"
          doc="i-129"
          annot_field_key="<feff004f0061006e006400500053007500700070004c0069006e00650038005b0032005d>"
          type="checkbox"
        />
      </fieldset>
      <h3>
        If no, provide the following information about the organization(s) to
        which you have sent a duplicate of this petition.
      </h3>
      <h3>O-1 Extraordinary Ability</h3>
      <fieldset>
        <Input
          name="10.a. Name of Recognized Peer/Peer Group or Labor Organization"
          label="10.a. Name of Recognized Peer/Peer Group or Labor Organization"
          doc="i-129"
          annot_field_key="<feff004c0053007500700070004c0069006e0065003100300061005f004e0061006d0065006f00660050006500650072005b0030005d>"
        />
      </fieldset>
      <h3>10.b. Physical Address</h3>
      <fieldset>
        <Input
          name="10.b. Street Number and Name"
          label="10.b. Street Number and Name"
          doc="i-129"
          annot_field_key="<feff00500061007200740037004c0069006e0065003100300062005f0045006d00700031005300740072006500650074004e0061006d0065005b0030005d>"
        />
        <Input
          name="10.b Apt."
          label="10.b Apt."
          doc="i-129"
          annot_field_key="<feff004c0069006e0065003100300062005f0055006e00690074005b0032005d>"
          type="checkbox"
        />
        <Input
          name="10.b Ste."
          label="10.b Ste."
          doc="i-129"
          annot_field_key="<feff004c0069006e0065003100300062005f0055006e00690074005b0031005d>"
          type="checkbox"
        />
        <Input
          name="10.b Flr."
          label="10.b Flr."
          doc="i-129"
          annot_field_key="<feff004c0069006e0065003100300062005f0055006e00690074005b0030005d>"
          type="checkbox"
        />
        <Input
          name="10.b. Number"
          label="10.b. Number"
          doc="i-129"
          annot_field_key="<feff004c0069006e0065003100300062005f0041007000740053007400650046006c0072004e0075006d006200650072005b0030005d>"
        />
        <Input
          name="10.b. City or Town"
          label="10.b. City or Town"
          doc="i-129"
          annot_field_key="<feff00500061007200740037004c0069006e0065003100300062005f0045006d00700043006900740079005b0030005d>"
        />
        <StateSelect
          name="10.b. State"
          label="10.b. State"
          doc="i-129"
          annot_field_key="<feff00500061007200740037004c0069006e0065003100300062005f0045006d0070003100530074006100740065005b0030005d>"
        />
        <Input
          name="10.b. ZIP Code"
          label="10.b. ZIP Code"
          doc="i-129"
          annot_field_key="<feff00500061007200740037004c0069006e0065003100300062005f0045006d00700031005a006900700043006f00640065005b0030005d>"
        />
      </fieldset>
      <h3>10.c. Date Sent (mm/dd/yyyy)</h3>
      <fieldset>
        <Input
          name="10.c. Date Sent (mm/dd/yyyy)"
          label="10.c. Date Sent (mm/dd/yyyy)"
          doc="i-129"
          type="date"
          annot_field_key="<feff00500061007200740037004c0069006e0065003100300063005f0045006d0070003100460072006f006d0044006100740065005b0030005d>"
        />
      </fieldset>
      <h3>10.d. Daytime Telephone Number</h3>
      <fieldset>
        <Input
          name="10.d. Daytime Telephone Number"
          label="10.d. Daytime Telephone Number"
          doc="i-129"
          type="Number"
          annot_field_key="<feff00500072006500700061007200650072005f00440061007900740069006d006500500068006f006e0065004e0075006d0062006500720031005f003100300064005b0030005d>"
        />
      </fieldset>
      <h3>O-1 Extraordinary achievement in motion pictures or television</h3>
      <fieldset>
        <Input
          name="11.a. Name of Labor Organization"
          label="11.a. Name of Labor Organization"
          doc="i-129"
          annot_field_key="<feff004c0053007500700070004c0069006e0065003100310061005f004e0061006d0065006f00660050006500650072005b0030005d>"
        />
      </fieldset>
      <h3>11.b. Complete Address</h3>
      <fieldset>
        <Input
          name="11.b. Street Number and Name"
          label="11.b. Street Number and Name"
          doc="i-129"
          annot_field_key="<feff00500061007200740037004c0069006e0065003100310062005f0045006d00700031005300740072006500650074004e0061006d0065005b0030005d>"
        />
        <Input
          name="11.b Apt."
          label="11.b Apt."
          doc="i-129"
          annot_field_key="<feff004c0069006e0065003100310062005f0055006e00690074005b0032005d>"
          type="checkbox"
        />
        <Input
          name="11.b Ste."
          label="11.b Ste."
          doc="i-129"
          annot_field_key="<feff004c0069006e0065003100310062005f0055006e00690074005b0031005d>"
          type="checkbox"
        />
        <Input
          name="11.b Flr."
          label="11.b Flr."
          doc="i-129"
          annot_field_key="<feff004c0069006e0065003100310062005f0055006e00690074005b0030005d>"
          type="checkbox"
        />
        <Input
          name="11.b. Number"
          label="11.b. Number"
          doc="i-129"
          annot_field_key="<feff004c0069006e0065003100310062005f0041007000740053007400650046006c0072004e0075006d006200650072005b0030005d>"
        />
        <Input
          name="11.b. City or Town"
          label="11.b. City or Town"
          doc="i-129"
          annot_field_key="<feff00500061007200740037004c0069006e0065003100310062005f0045006d00700043006900740079005b0030005d>"
        />
        <StateSelect
          name="11.b. State"
          label="11.b. State"
          doc="i-129"
          annot_field_key="<feff00500061007200740037004c0069006e0065003100310062005f0045006d0070003100530074006100740065005b0030005d>"
        />
        <Input
          name="11.b. ZIP Code"
          label="11.b. ZIP Code"
          doc="i-129"
          annot_field_key="<feff00500061007200740037004c0069006e0065003100310062005f0045006d00700031005a006900700043006f00640065005b0030005d>"
        />
      </fieldset>
      <h3>11.c. Date Sent (mm/dd/yyyy)</h3>
      <fieldset>
        <Input
          name="11.c. Date Sent (mm/dd/yyyy)"
          label="11.c. Date Sent (mm/dd/yyyy)"
          doc="i-129"
          type="date"
          annot_field_key="<feff00500061007200740037004c0069006e003100310063005f0045006d0070003100460072006f006d0044006100740065005b0030005d>"
        />
      </fieldset>
      <h3>11.d. Daytime Telephone Number</h3>
      <fieldset>
        <Input
          name="11.d. Daytime Telephone Number"
          label="11.d. Daytime Telephone Number"
          doc="i-129"
          type="Number"
          annot_field_key="<feff00500072006500700061007200650072005f00440061007900740069006d006500500068006f006e0065004e0075006d0062006500720031005f003100310064005b0030005d>"
        />
      </fieldset>
      <h3>12.a. Name of Management Organization</h3>
      <fieldset>
        <Input
          name="12.a. Name of Management Organization"
          label="12.a. Name of Management Organization"
          doc="i-129"
          annot_field_key="<feff004c0053007500700070004c0069006e0065003100320061005f004e0061006d0065006f00660050006500650072005b0030005d>"
        />
      </fieldset>
      <h3>12.b. Physical Address</h3>
      <fieldset>
        <Input
          name="12.b. Street Number and Name"
          label="12.b. Street Number and Name"
          doc="i-129"
          annot_field_key="<feff00500061007200740037004c0069006e0065003100320062005f0045006d00700031005300740072006500650074004e0061006d0065005b0030005d>"
        />
        <Input
          name="12.b Apt."
          label="12.b Apt."
          doc="i-129"
          annot_field_key="<feff004c0069006e0065003100320062005f0055006e00690074005b0032005d>"
          type="checkbox"
        />
        <Input
          name="12.b Ste."
          label="12.b Ste."
          doc="i-129"
          annot_field_key="<feff004c0069006e0065003100320062005f0055006e00690074005b0031005d>"
          type="checkbox"
        />
        <Input
          name="12.b Flr."
          label="12.b Flr."
          doc="i-129"
          annot_field_key="<feff004c0069006e0065003100320062005f0055006e00690074005b0030005d>"
          type="checkbox"
        />
        <Input
          name="12.b. Number"
          label="12.b. Number"
          doc="i-129"
          annot_field_key="<feff004c0069006e0065003100320062005f0041007000740053007400650046006c0072004e0075006d006200650072005b0030005d>"
        />
        <Input
          name="12.b. City or Town"
          label="12.b. City or Town"
          doc="i-129"
          annot_field_key="<feff00500061007200740037004c0069006e0065003100320062005f0045006d00700043006900740079005b0030005d>"
        />
        <StateSelect
          name="12.b. State"
          label="11.b. State"
          doc="i-129"
          annot_field_key="<feff00500061007200740037004c0069006e0065003100320062005f0045006d0070003100530074006100740065005b0030005d>"
        />
        <Input
          name="12.b. ZIP Code"
          label="12.b. ZIP Code"
          doc="i-129"
          annot_field_key="<feff00500061007200740037004c0069006e0065003100320062005f0045006d00700031005a006900700043006f00640065005b0030005d>"
        />
      </fieldset>
      <h3>12.c. Date Sent (mm/dd/yyyy)</h3>
      <fieldset>
        <Input
          name="12.c. Date Sent (mm/dd/yyyy)"
          label="12.c. Date Sent (mm/dd/yyyy)"
          doc="i-129"
          type="date"
          annot_field_key="<feff00500061007200740037004c0069006e0065003100320063005f0045006d0070003100460072006f006d0044006100740065005b0030005d>"
        />
      </fieldset>
      <h3>12.d. Daytime Telephone Number</h3>
      <fieldset>
        <Input
          name="12.d. Daytime Telephone Number"
          label="12.d. Daytime Telephone Number"
          doc="i-129"
          type="Number"
          annot_field_key="<feff00500072006500700061007200650072005f00440061007900740069006d006500500068006f006e0065004e0075006d0062006500720031005f003100320064005b0030005d>"
        />
      </fieldset>
      <h3>O-2 or P alien</h3>
      <fieldset>
        <Input
          label="13.a. Name of Labor Organization"
          name="13.a. Name of Labor Organization"
          doc="i-129"
          annot_field_key="<feff004c0053007500700070004c0069006e0065003100330061005f004e0061006d0065006f00660050006500650072005b0030005d>"
        />
      </fieldset>
      <h3>13.b. Complete Address</h3>
      <fieldset>
        <Input
          name="13.b. Street Number and Name"
          label="13.b. Street Number and Name"
          doc="i-129"
          annot_field_key="<feff00500061007200740037004c0069006e0065003100330062005f0045006d00700031005300740072006500650074004e0061006d0065005b0030005d>"
        />
        <Input
          name="13.b Apt."
          label="13.b Apt."
          doc="i-129"
          annot_field_key="<feff004c0069006e0065003100330062005f0055006e00690074005b0032005d>"
          type="checkbox"
        />
        <Input
          name="13.b Ste."
          label="13.b Ste."
          doc="i-129"
          annot_field_key="<feff004c0069006e0065003100330062005f0055006e00690074005b0031005d>"
          type="checkbox"
        />
        <Input
          name="13.b Flr."
          label="13.b Flr."
          doc="i-129"
          annot_field_key="<feff004c0069006e0065003100330062005f0055006e00690074005b0030005d>"
          type="checkbox"
        />
        <Input
          name="13.b. Number"
          label="13.b. Number"
          doc="i-129"
          annot_field_key="<feff004c0069006e0065003100330062005f0041007000740053007400650046006c0072004e0075006d006200650072005b0030005d>"
        />
        <Input
          name="13.b. City or Town"
          label="13.b. City or Town"
          doc="i-129"
          annot_field_key="<feff00500061007200740037004c0069006e0065003100330062005f0045006d00700043006900740079005b0030005d>"
        />
        <StateSelect
          name="13.b. State"
          label="13.b. State"
          doc="i-129"
          annot_field_key="<feff00500061007200740037004c0069006e0065003100330062005f0045006d0070003100530074006100740065005b0030005d>"
        />
        <Input
          name="13.b. ZIP Code"
          label="13.b. ZIP Code"
          doc="i-129"
          annot_field_key="<feff00500061007200740037004c0069006e0065003100330062005f0045006d00700031005a006900700043006f00640065005b0030005d>"
        />
      </fieldset>
      <h3>13.c. Date Sent (mm/dd/yyyy)</h3>
      <fieldset>
        <Input
          name="13.c. Date Sent (mm/dd/yyyy)"
          label="13.c. Date Sent (mm/dd/yyyy)"
          doc="i-129"
          type="date"
          annot_field_key="<feff00500061007200740037004c0069006e0065003100330063005f0045006d0070003100460072006f006d0044006100740065005b0030005d>"
        />
      </fieldset>
      <h3>13.d. Daytime Telephone Number</h3>
      <fieldset>
        <Input
          name="13.d. Daytime Telephone Number"
          label="13.d. Daytime Telephone Number"
          doc="i-129"
          type="Number"
          annot_field_key="<feff00500072006500700061007200650072005f00440061007900740069006d006500500068006f006e0065004e0075006d0062006500720031005f003100330064005b0030005d>"
        />
      </fieldset>
    </div>
    </WithValues>
  );
};

export default Section1;
