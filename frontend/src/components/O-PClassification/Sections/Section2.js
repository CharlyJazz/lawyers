import React from "react";
import WithValues from '../../../WithValues'
import Input from "../../Input";

const Section2 = () => {

  return (
    <WithValues doc={doc}>
      <div>
        <h3>
          Section 2. Statement by the Petitioner I certify that I, the petitioner,
          and the employer whose offer of employment formed the basis of status
          (if different from the petitioner) will be jointly and severally liable
          for the reasonable costs of return transportation of the beneficiary
          abroad if the beneficiary is dismissed from employment by the employer
          before the end of the period of authorized stay.
      </h3>
        <h3>1. Name of Petitioner</h3>
        <fieldset>
          <Input
            name="1. Family Name (Last Name)"
            label="1. Family Name (Last Name)"
            annot_field_key="<feff004c0069006e00650031005f00460061006d0069006c0079004e0061006d0065005b0038005d>"
            doc="i-129"
          />
          <Input
            name="1. Given Name (First Name)"
            label="1. Given Name (First Name)"
            annot_field_key="<feff004c0069006e00650031005f0047006900760065006e004e0061006d0065005b0034005d>"
            doc="i-129"
          />
          <Input
            name="1. Middle Name"
            label="1. Middle Name"
            annot_field_key="<feff004c0069006e00650031005f004d006900640064006c0065004e0061006d0065005b0034005d>"
            doc="i-129"
          />
        </fieldset>

        <h3>3. Petitioner's Contact Information</h3>
        <fieldset>
          <Input
            name="3. Daytime Telephone Number"
            label="3. Daytime Telephone Number"
            annot_field_key="<feff005000740037004c0069006e00650033005f00440061007900740069006d006500500068006f006e0065004e0075006d0062006500720031005b0031005d>"
            doc="i-129"
            type="tel"
          />
          <Input
            name="3. Email Address (if any)"
            label="3. Email Address (if any)"
            annot_field_key="<feff005000740037004c0069006e00650033005f0045006d00610069006c0041006400640072006500730073005b0031005d>"
            doc="i-129"
            type="email"
          />
        </fieldset>
      </div>
    </WithValues>
  );
};

export default Section2;
