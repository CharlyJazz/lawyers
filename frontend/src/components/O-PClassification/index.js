import React from "react";
import "./OPClassification.css";
import Section1 from "./Sections/Section1";
import Section2 from "./Sections/Section2";
import { Link, Router } from "@reach/router";

const OPClassification = () => {

  return (
    <div>
      <h1>O and P Classifications Supplement to Form I-129</h1>
      <div className="Layout-Form">
        <aside className="Layout-Form-Aside">
          <ul>
            <li>
              <Link to="/o-p-classification">Section 1</Link>
            </li>
            {[2].map(p => (
              <li key={p}>
                <Link to={`section-${p}`}>Section {p}</Link>
              </li>
            ))}
          </ul>
        </aside>
        <section className="Layout-Section">
          <Router>
            <Section1 path="/" />
            <Section2 path="section-2" />
          </Router>
        </section>
      </div>
    </div>
  );
};

export default OPClassification;
