import React from "react";
import Header from "../Header";
import I129 from "../I-129";
import I907 from "../I-907";
import G282 from "../G-28-2";
import E1E2 from "../E1E2";
import LClassification from "../LClassification";
import OPClassification from "../O-PClassification";
import Q1Classification from "../Q-1Classification";
import R1Classification from "../R-1Classification";
import Attachment1 from "../Attachment1";
import Trade from "../Trade";
import HClassification from "../HClassification";
import H1B from "../H1B";
import { Link, Router } from "@reach/router";
import "./Home.css";

const HomeRoute = () => (
  <div className="Home__pdf_wrapper">
    <Link to="i-129">
      <div>
        <h2>I-129</h2>
        <p>PDF</p>
      </div>
    </Link>
    <Link to="i-907">
      <div>
        <h2>I-907</h2>
        <p>PDF</p>
      </div>
    </Link>
    <Link to="g-28-2">
      <div>
        <h2>G-28-2</h2>
        <p>PDF</p>
      </div>
    </Link>
  </div>
);

const Home = () => {
  return (
    <div>
      <Header />
      <Router>
        <HomeRoute path="/" />
        <I129 path="i-129/*" />
        <I907 path="i-907/*" />
        <G282 path="g-28-2/*" />
        <E1E2 path="e1-e2/*" />
        <LClassification path="l-classification/*" />
        <OPClassification path="o-p-classification/*" />
        <Q1Classification path="q-1-classification/*" />
        <R1Classification path="r-1-classification/*" />
        <Attachment1 path="attachment-1" />
        <Trade path="trade-agreement/*" />
        <HClassification path="h-classification/*" />
        <H1B path="h1b-h1b1/*" />
      </Router>
    </div>
  );
};

export default Home;
