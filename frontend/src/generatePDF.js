import { URL_BASE } from "./constants";


export default document_type =>
  fetch(URL_BASE + "/document", {
    method: "POST",
    body: JSON.stringify({
      document_type
    }),
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("jwt")}`
    }
  })
    .then(res => res.json())
    .then(res => console.log(res))
    .catch(err => console.log(err));
