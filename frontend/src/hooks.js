import React from "react";
import { URL_BASE } from "./constants";

export const useDoc = doctype => {
  const [doc, setDoc] = React.useState({});

  const getFields = async () => {
    try {
      const { document } = await fetch(`${URL_BASE}/document/${doctype}`, {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("jwt")}`
        }
      })
        .then(res => res.json())
        .then(res => res)
        .catch(err => err);

      setDoc(
        (() => {
          const dict = {};
          for (let i = 0; i < document.fields.length; i++) {
            const field = document.fields[i];
            if (field.annot_field_key) {
              dict[field.annot_field_key] = field;
            }
          }
          return dict;
        })()
      );
    } catch (error) {
      console.log("No se seteo el doc quizas, no haya, o error en el server");
    }
  };

  React.useEffect(() => {
    getFields();
  }, []);

  return doc;
};
