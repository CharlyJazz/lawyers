import React from "react";
import Login from "./components/Login";
import { AppContext } from "./state";
import Home from "./components/Home";

const SwitchRootView = () => {
  const [state] = React.useContext(AppContext);
  return !state.user ? <Login /> : <Home path="/" />;
};
export default SwitchRootView;
