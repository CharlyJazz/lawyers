import { useDoc } from "./hooks";

export default ({ children }) => {
  const doc = useDoc("i-129");
  if (!(Object.entries(doc).length === 0 && doc.constructor === Object)) {
    [
      ...document.getElementsByTagName("input"),
      ...document.getElementsByTagName("textarea")
    ].forEach(input => {
      if (input.dataset.annot) {
        const tryGetField = doc[input.dataset.annot];

        if (tryGetField) {
          if (input.type === "checkbox" || input.type === "radio") {
            console.log(tryGetField)
            input.checked = tryGetField.value === '1' ? true : false;
          } else {
            input.value = tryGetField.value;
          }
        }
      }
    });
  }

  return children;
};
