from ..database import db, Model , Column
from sqlalchemy_utils import Timestamp

class Field(Model, Timestamp):
  __tablename__ = 'field'
  id              = Column(db.Integer, primary_key=True)
  id_user         = Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
  id_document     = Column(db.Integer, db.ForeignKey('document.id'), nullable=False)
  label_id        = Column(db.String(2000), nullable=False)
  value           = Column(db.Text(), nullable=False)
  annot_field_key = Column(db.Text(), nullable=False)