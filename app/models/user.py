from ..database import db, Model , Column, relationship
from ..extensions import marshmallow

from sqlalchemy_utils import PasswordType, EmailType, force_auto_coercion, Timestamp, aggregated, QueryChain
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import validates

import datetime, flask

force_auto_coercion()

class User(Model, Timestamp):
    __tablename__ = 'user'
    id       = Column(db.Integer, primary_key=True)
    username = Column(db.String(80), unique=True)
    email    = Column(EmailType, unique=True, nullable=False)
    password = Column(
        PasswordType(
            # The returned dictionary is forwarded to the CryptContext
            onload=lambda **kwargs: dict(
                schemes=flask.current_app.config['PASSWORD_SCHEMES'],
                **kwargs
            ),
        ),
        unique=False,
        nullable=False,
    )

    def __init__(self, username, email, password):
        self.username = username
        self.email = email
        self.password = password


    # Validations

    @validates('username')
    def validate_username(self, key, username):
        from ..controllers.interface.authentication.descriptions import SIGN_UP_DESCRIPTIONS as _
        if len(username) <= 2:
            raise ValueError(_['INVALID_USERNAME'])
        else:
            return username

    @validates('password')
    def validate_password(self, key, password):
        from ..controllers.interface.authentication.descriptions import SIGN_UP_DESCRIPTIONS as _
        if len(password) <= 5:
            raise ValueError(_['INVALID_PASSWORD'])
        else:
            return password

    @validates('email')
    def validate_email(self, key, address):
        from ..controllers.interface.authentication.descriptions import SIGN_UP_DESCRIPTIONS as _
        if len(address) < 3 or '@' not in address:
            raise ValueError(_['INVALID_EMAIL'])
        else:
            return address

    # Relationships

    notifications = relationship('Notification', backref='user', lazy='dynamic')
    documents = relationship('Document', backref='user', lazy='dynamic')
    fields = relationship('Field', backref='user', lazy='dynamic')

    # Methods

    @classmethod
    def authenticate(self, email, password):
        user = User.query.filter(User.email == email).scalar()
        if (user.password == password):
            return user
        else:
            return None