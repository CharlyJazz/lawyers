from ..database import db, Model , Column, relationship
from sqlalchemy_utils import Timestamp, ChoiceType

class Document(Model, Timestamp):
  TYPE_DOC = [
    (u'i-129', u'i-129'),
    (u'i-907', u'i-907'),
    (u'g-28-2', u'g-28-2')
  ]

  __tablename__ = 'document'
  id       = Column(db.Integer, primary_key=True)
  id_user  = Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
  type_doc = Column(ChoiceType(TYPE_DOC), nullable=False, default=u'i-129')

  fields = relationship('Field',  backref='document')