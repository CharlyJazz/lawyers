from flask import Flask
from .extensions import *
from .config import DevelopmentConfig, STATIC_FOLDER
from .models import *
from .commands import test

from sqlalchemy import create_engine
from sqlalchemy_utils import database_exists, create_database
from flask_migrate import Migrate, MigrateCommand
from flask_jwt_extended import JWTManager

def create_app(config=DevelopmentConfig):
    app = Flask(__name__, static_folder = STATIC_FOLDER)
    app.config.from_object(config)

    register_extensions(app)
    register_blueprints(app)
    register_commands(app)

    engine = create_engine(app.config['SQLALCHEMY_DATABASE_URI'])
    if not database_exists(engine.url):
        create_database(engine.url)

    with app.app_context():
        db.create_all()

    return app


def register_extensions(app):
    """Register Flask extensions."""
    db.init_app(app)
    marshmallow.init_app(app)
    migrate = Migrate(app, db) # this
    jwt = JWTManager(app)

def register_blueprints(app):
    """Register Flask blueprints."""
    from .controllers.web.web import app as application
    app.register_blueprint(application)
    from .controllers.interface.user.endpoints import app as user
    app.register_blueprint(user)
    from .controllers.interface.authentication.endpoints import app as auth
    app.register_blueprint(auth)
    from .controllers.interface.field.endpoints import app as field
    app.register_blueprint(field)
    from .controllers.interface.document.endpoints import app as document
    app.register_blueprint(document)

def register_commands(app):
    """Register Click commands."""
    app.cli.add_command(test)