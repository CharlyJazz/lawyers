from flask import Blueprint, request, jsonify
from flask_cors import CORS
from flask_jwt_extended import jwt_required, get_jwt_identity

from ....utils.get_data_or_400 import get_data_or_400
from ....models import User, Field, Document
from ....extensions import db
from ....config import APP_DIR
from ....schemas.document import DocumentSchema

import pdfrw
import os

app = Blueprint('document', __name__)
CORS(app)

ANNOT_KEY = '/Annots'
ANNOT_FIELD_KEY = '/T'
SUBTYPE_KEY = '/Subtype'
WIDGET_SUBTYPE_KEY = '/Widget'

@app.route('/api/document/<type_doc>')
@jwt_required
def get_fields_in_document(type_doc):
    if not request.is_json:
        return jsonify({"msg": "Missing JSON in request"}), 400
    user_id = get_jwt_identity()["id"]
    user = User.query.get(user_id)
    document = user.documents.filter(Document.type_doc==type_doc).first()
    if document is None:
        return jsonify({"message": "The user dont have this document assigned yet!"}), 400
    print(document.fields)
    schema = DocumentSchema()
    serialized = schema.dump(document)
    return jsonify({"document": serialized})

@app.route('/api/document', methods=['POST'])
@jwt_required
def generate_document():
    if not request.is_json:
        return jsonify({"msg": "Missing JSON in request"}), 400
    data = request.get_json()
    document_type = get_data_or_400(data, 'document_type', 'Doc type is needed')
    user_id = get_jwt_identity()["id"]
    user = User.query.get(user_id)
    document = user.documents.filter(Document.type_doc==document_type).first()
    if document is None:
        return jsonify({"message": "The user dont have this document assigned yet!"}), 400
    fields_related = user.fields.filter(Field.id_document==document.id).all()
    template_pdf = pdfrw.PdfReader(os.path.join(APP_DIR, '../documents/{}_expanded.pdf'.format(document_type)))
    for page in template_pdf.pages:
        annotations = page[ANNOT_KEY]
        for annotation in annotations:
            if annotation[SUBTYPE_KEY] == WIDGET_SUBTYPE_KEY and annotation[ANNOT_FIELD_KEY]:
                key = annotation[ANNOT_FIELD_KEY]
                for n in fields_related:
                    if n.annot_field_key == key:
                        annotation.update(pdfrw.PdfDict(V=n.value))
    path = os.path.join(APP_DIR, 'media/pdf/{}'.format(user_id))
    if not os.path.exists(path):
        os.mkdir(path)
    pdfrw.PdfWriter().write(os.path.join(path, '{}.pdf'.format(document_type)), template_pdf)
    return jsonify({"message": "Generate!"})
