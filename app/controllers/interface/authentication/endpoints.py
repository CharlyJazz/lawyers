from flask_jwt_extended import create_access_token
from flask_cors import CORS
from flask import Blueprint, request, jsonify, current_app
from sqlalchemy.exc import IntegrityError

from ....utils.get_data_or_400 import get_data_or_400

from ....models.user import User, db
from ....schemas.user import UserSchema

from .descriptions import SIGN_IN_DESCRIPTIONS, SIGN_UP_DESCRIPTIONS


app = Blueprint('auth', __name__)
CORS(app)

@app.route('/api/register', methods=['POST'])
def register():
    if not request.is_json:
        return jsonify({"msg": "Missing JSON in request"}), 400

    data = request.get_json()
    email = get_data_or_400(data, 'email', SIGN_UP_DESCRIPTIONS['MISS_EMAIL'])
    password = get_data_or_400(data, 'password', SIGN_UP_DESCRIPTIONS['MISS_PASSWORD'])
    username = get_data_or_400(data, 'username', SIGN_UP_DESCRIPTIONS['MISS_USERNAME'])
    try:
        new_user = User(username, email, password)
        db.session.add(new_user)
        db.session.commit()
        schema = UserSchema()
        serialized = schema.dump(new_user)
        return jsonify(serialized), 201
    except IntegrityError:
        return jsonify({"error": "Incorrect credentials"}), 401

@app.route('/api/login', methods=['POST'])
def login():
    if not request.is_json:
        return jsonify({"msg": "Missing JSON in request"}), 400

    data = request.get_json()
    email = get_data_or_400(data, 'email', SIGN_IN_DESCRIPTIONS['MISS_EMAIL'])
    password = get_data_or_400(data, 'password', SIGN_IN_DESCRIPTIONS['MISS_PASSWORD'])
    user = User.authenticate(email, password)
    if (user != None):
        schema = UserSchema()
        serialized = schema.dump(user)
        token = create_access_token(identity=serialized)
        return jsonify({"access_token": token, "user": serialized}), 200
    
    return jsonify({"error": "Incorrect credentials"}), 401