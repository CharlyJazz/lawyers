SIGN_IN_DESCRIPTIONS = {
  'MISS_PASSWORD': 'Missing password',
  'MISS_EMAIL': 'Missing email',
  'INVALID_PASSWORD': 'Invalid password',
  'INVALID_EMAIL': 'Missing email',
  'NOT_MATCH': 'Some not match',
  'SUCCESS': 'Successful authentication'
}

SIGN_UP_DESCRIPTIONS = {
  'INVALID_EMAIL': 'Invalid email',
  'INVALID_PASSWORD': 'Invalid password',
  'INVALID_USERNAME': 'Invalid username',
  'MISS_PASSWORD': 'Missing password',
  'MISS_EMAIL': 'Missing email',
  'MISS_USERNAME': 'Missing username',
}