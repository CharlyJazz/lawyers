from flask import Blueprint, request, jsonify
from flask_cors import CORS
from flask_jwt_extended import jwt_required, get_jwt_identity

from ....utils.get_data_or_400 import get_data_or_400
from ....models import User, Field, Document
from ....extensions import db


app = Blueprint('field', __name__)
CORS(app)


@app.route('/api/field', methods=['POST'])
@jwt_required
def update_field():
    if not request.is_json:
        return jsonify({"msg": "Missing JSON in request"}), 400
    data = request.get_json()
    field_doc_type = get_data_or_400(data, 'doc', 'Doc type is needed')
    annot_field_key = get_data_or_400(data, 'annot_field_key', 'Annot Field key is needed')
    value_field = get_data_or_400(data, 'value', 'New value of the field is needed')
    user_id = get_jwt_identity()["id"]
    user = User.query.get(user_id)
    check_if_exist_field = user.fields.filter(Field.annot_field_key==annot_field_key).first()
    check_if_exist_doc_type = user.documents.filter(Document.type_doc==field_doc_type).first()
    if check_if_exist_field is None and check_if_exist_doc_type is None:
        new_doc = Document(type_doc=field_doc_type, id_user=user_id)
        db.session.add(new_doc)
        db.session.commit()
        new_field = Field(
            id_user=user_id,
            id_document=new_doc.id,
            label_id='NOT NEEDED FOR NOW',
            value=value_field,
            annot_field_key=annot_field_key
        )
        db.session.add(new_field)
        db.session.commit()
        return jsonify({"message": "Created!"})
    elif check_if_exist_field is None:
        new_field = Field(
            id_user=user_id,
            id_document=check_if_exist_doc_type.id,
            label_id='NOT NEEDED FOR NOW',
            value=value_field,
            annot_field_key=annot_field_key
        )
        db.session.add(new_field)
        db.session.commit()
        return jsonify({"message": "Created!"})
    else:
        check_if_exist_field.value = value_field
        db.session.add(check_if_exist_field)
        db.session.commit()
        return jsonify({"message": "Update!"})
