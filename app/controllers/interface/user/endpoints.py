from flask import Blueprint, jsonify
from flask_jwt_extended import jwt_required, get_jwt_identity
from flask_cors import CORS


from .descriptions import GET_USER_DESCRIPTIONS, \
  GET_USER_NOTIFICATIONS_DESCRIPTIONS

app = Blueprint('user', __name__)
CORS(app)

@app.route('/api/current-user')
@jwt_required
def get_user():
  return jsonify(get_jwt_identity())
  