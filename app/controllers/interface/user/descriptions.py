GET_USER_DESCRIPTIONS = {
  'SUCCESS': 'User successfullly founded.',
  'NOT_FOUND': 'User not found.'
}

GET_USER_NOTIFICATIONS_DESCRIPTIONS = {
  'SUCCESS': 'Notifications successfully founded.',
  'NOT_FOUND': 'Notifications not found'
}