import os

from flask import Blueprint, send_from_directory, current_app, render_template, send_from_directory, request

APP_DIR = os.path.abspath(os.path.dirname(__file__))
STATIC_FOLDER = os.path.join(APP_DIR, '../../../frontend/build/static')
TEMPLATE_FOLDER = os.path.join(APP_DIR, '../../../frontend/build/')

app = Blueprint('web', __name__, static_folder=STATIC_FOLDER, template_folder=TEMPLATE_FOLDER)


@app.route('/robots.txt')
@app.route('/manifest.json')
@app.route('/favicon.ico')
@app.route('/service-worker.js')
def root_assets():
    return send_from_directory(TEMPLATE_FOLDER, request.path[1:])

@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def index(path):
    return render_template('index.html')