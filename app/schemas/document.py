from ..extensions import marshmallow

from marshmallow import fields

from .field import FieldSchema


class DocumentSchema(marshmallow.Schema):
    class Meta:
        fields = ('id', 'fields')
    
    id = fields.Int()
    fields = fields.Nested(FieldSchema, many=True)