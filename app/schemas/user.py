from ..extensions import marshmallow

from marshmallow import fields


class UserSchema(marshmallow.Schema):
    class Meta:
        fields = ('id', 'username', 'email', 'created', 'updated')
    
    id = fields.Int()