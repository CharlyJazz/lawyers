from ..extensions import marshmallow

from marshmallow import fields


class FieldSchema(marshmallow.Schema):
    class Meta:
        fields = (
            'id',
            'id_user',
            'id_document',
            'label_id',
            'value',
            'annot_field_key',
        )
    
    id = fields.Int()
    id_user = fields.Int()
    id_document = fields.Int()