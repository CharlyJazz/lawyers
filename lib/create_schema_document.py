import os
import pdfrw
import json

# Create JSON file with /TU and /T
PDF_NAME = 'g-28-2'
PDF_TEMPLATE_PATH = '{}_expanded.pdf'.format(PDF_NAME)
 
ANNOT_KEY = '/Annots'
ANNOT_FIELD_KEY = '/T'
ANNOT_VAL_KEY = '/V'
ANNOT_RECT_KEY = '/Rect'
SUBTYPE_KEY = '/Subtype'
WIDGET_SUBTYPE_KEY = '/Widget'
LABEL_ID = '/TU'

template_pdf = pdfrw.PdfReader(PDF_TEMPLATE_PATH)
data = {}

for page_index, page in enumerate(template_pdf.pages):
    annotations = page[ANNOT_KEY]
    page_data = []
    for field_index, annotation in enumerate(annotations):
        if annotation[SUBTYPE_KEY] == WIDGET_SUBTYPE_KEY:
            if annotation[ANNOT_FIELD_KEY]:
                key = annotation[ANNOT_FIELD_KEY][1:-1]
                page_data.append(
                    {
                        "ANNOT_FIELD_KEY": annotation[ANNOT_FIELD_KEY],
                        "LABEL_ID": annotation[LABEL_ID],
                        "VALUE": "{} {}".format(field_index, "Hey")
                    }
                )  
    data[page_index] = page_data

with open('{}.json'.format(PDF_NAME), 'w') as outfile:
    json.dump(data, outfile)


# Miscelanius
# annotation.update(pdfrw.PdfDict(V='Emma XS'))
# if key == 'feff00500061007200740031005f004c0069006e00650035005f004d00610069006c0069006e00670041006400640072006500730073005f0049006e0043006100720065006f0066004e0061006d0065005b0030005d':
#     annotation.update(pdfrw.PdfDict(V='aaaaaaaaalaverga'))
# print(key)
# https://unix.stackexchange.com/a/109177/379622
# qpdf --qdf --object-streams=disable  i-129.pdf i-129_expanded.pdf 
# pdfrw.PdfWriter().write(PDF_OUTPUT_PATH, template_pdf)