import os
import pdfrw
import json

# Create JSON file with /TU and /T
PDF_NAME = 'i-129'
PDF_TEMPLATE_PATH = '{}_expanded.pdf'.format(PDF_NAME)
PDF_OUTPUT_PATH = '{}_filled.pdf'.format(PDF_NAME)

ANNOT_KEY = '/Annots'
ANNOT_FIELD_KEY = '/T'
ANNOT_VAL_KEY = '/V'
ANNOT_RECT_KEY = '/Rect'
SUBTYPE_KEY = '/Subtype'
WIDGET_SUBTYPE_KEY = '/Widget'
LABEL_ID = '/TU'

template_pdf = pdfrw.PdfReader(PDF_TEMPLATE_PATH)

with open('i-129.json', 'r') as json_file:
    data = json.load(json_file)
    for page_index, page in enumerate(template_pdf.pages):
        annotations = page[ANNOT_KEY]
        page_in_json = data[str(page_index)]
        for field_index, annotation in enumerate(annotations):
            if annotation[SUBTYPE_KEY] == WIDGET_SUBTYPE_KEY:
                if annotation[ANNOT_FIELD_KEY]:
                    key = annotation[ANNOT_FIELD_KEY]
                    item_matched = next((x for x in page_in_json if "ANNOT_FIELD_KEY" in x and x["ANNOT_FIELD_KEY"] == key), None)
                    annotation.update(pdfrw.PdfDict(V=item_matched["VALUE"]))

pdfrw.PdfWriter().write(PDF_OUTPUT_PATH, template_pdf)

# Miscelanius
# 
# if key == 'feff00500061007200740031005f004c0069006e00650035005f004d00610069006c0069006e00670041006400640072006500730073005f0049006e0043006100720065006f0066004e0061006d0065005b0030005d':
#     annotation.update(pdfrw.PdfDict(V='aaaaaaaaalaverga'))
# print(key)
# https://unix.stackexchange.com/a/109177/379622
# qpdf --qdf --object-streams=disable  i-129.pdf i-129_expanded.pdf 
# pdfrw.PdfWriter().write(PDF_OUTPUT_PATH, template_pdf)